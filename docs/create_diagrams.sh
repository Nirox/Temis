#!/usr/bin/env bash

#
# Creates a small and large (complete) PDF class diagrams.
#

java -jar ../plantuml.jar -teps -nbthread auto -pipe < class_diagram.puml > class_diagram_small.eps

sed -i -e "3s/!define SMALL_DIAGRAM/'!define SMALL_DIAGRAM/" class_diagram.puml

java -jar ../plantuml.jar -teps -nbthread auto -pipe < class_diagram.puml > class_diagram.eps

sed -i -e "3s/'!define SMALL_DIAGRAM/!define SMALL_DIAGRAM/" class_diagram.puml

epstopdf --outfile class_diagram_small.pdf class_diagram_small.eps

epstopdf --outfile class_diagram.pdf       class_diagram.eps
