package ui;

import java.awt.Image;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import temis.Activity;
import temis.Apartment;
import temis.Article;
import temis.Charge;
import temis.ConnectionDB;
import temis.Debt;
import temis.Duty;
import temis.Event;
import temis.Group;
import temis.Item;
import temis.JournalPlace;
import temis.Place;
import temis.Purchase;
import temis.Task;
import temis.Trip;
import temis.User;
import temis.Utils;

public class TemisView extends javax.swing.JFrame {

    // view elements                   
    private javax.swing.JPanel ManageUser;
    private javax.swing.JButton jButtonApartmentBack1;
    private javax.swing.JButton jButtonApartmentDetailDelete;
    private javax.swing.JButton jButtonApartmentDetailView;
    private javax.swing.JButton jButtonArticleBack;
    private javax.swing.JButton jButtonChargeAcept;
    private javax.swing.JButton jButtonChargeBack;
    private javax.swing.JButton jButtonDebtBack;
    private javax.swing.JButton jButtonDebtDelete;
    private javax.swing.JButton jButtonDebtModify;
    private javax.swing.JButton jButtonDebtNew;
    private javax.swing.JButton jButtonDutyAcept;
    private javax.swing.JButton jButtonDutyBack;
    private javax.swing.JButton jButtonEventBack;
    private javax.swing.JButton jButtonEventBack1;
    private javax.swing.JButton jButtonEventDelete;
    private javax.swing.JButton jButtonEventView;
    private javax.swing.JButton jButtonGroupsDelete;
    private javax.swing.JButton jButtonGroupsModify;
    private javax.swing.JButton jButtonGroupsView;
    private javax.swing.JButton jButtonJorunalPlaceDelete;
    private javax.swing.JButton jButtonJournalPlaceBack;
    private javax.swing.JButton jButtonJournalPlaceModify;
    private javax.swing.JButton jButtonJournalPlaceNew;
    private javax.swing.JButton jButtonLanguage;
    private javax.swing.JButton jButtonLogin;
    private javax.swing.JButton jButtonManageUserBack;
    private javax.swing.JButton jButtonManageUserDelete;
    private javax.swing.JButton jButtonNewApartmentCancel;
    private javax.swing.JButton jButtonNewApartmentCreate;
    private javax.swing.JButton jButtonNewArticleCancel;
    private javax.swing.JButton jButtonNewArticleCreate;
    private javax.swing.JButton jButtonNewChargeCancel;
    private javax.swing.JButton jButtonNewChargeCreate;
    private javax.swing.JButton jButtonNewDebtCancel;
    private javax.swing.JButton jButtonNewDebtCreate;
    private javax.swing.JButton jButtonNewDutyCancel;
    private javax.swing.JButton jButtonNewDutyCreate;
    private javax.swing.JButton jButtonNewEventCancel;
    private javax.swing.JButton jButtonNewEventCreate1;
    private javax.swing.JButton jButtonNewJournalPlaceCancel;
    private javax.swing.JButton jButtonNewJournalPlaceCreate;
    private javax.swing.JButton jButtonNewPlaceCancel;
    private javax.swing.JButton jButtonNewPlaceCreate;
    private javax.swing.JButton jButtonNewPruchaseCancel;
    private javax.swing.JButton jButtonNewPruchaseCreate;
    private javax.swing.JButton jButtonNewTaskCancel;
    private javax.swing.JButton jButtonNewTaskCreate;
    private javax.swing.JButton jButtonNewTripCancel;
    private javax.swing.JButton jButtonNewTripCreate1;
    private javax.swing.JButton jButtonPlaceAcept;
    private javax.swing.JButton jButtonPlaceBack;
    private javax.swing.JButton jButtonPurchaseBack;
    private javax.swing.JButton jButtonPurchaseDelete;
    private javax.swing.JButton jButtonPurchaseModify;
    private javax.swing.JButton jButtonPurchaseNew;
    private javax.swing.JButton jButtonPurchasePay;
    private javax.swing.JButton jButtonRegister;
    private javax.swing.JButton jButtonRegisterCancel;
    private javax.swing.JButton jButtonRegisterRegister;
    private javax.swing.JButton jButtonTaskBack;
    private javax.swing.JButton jButtonTaskDelete;
    private javax.swing.JButton jButtonTaskModify;
    private javax.swing.JButton jButtonTaskNew;
    private javax.swing.JButton jButtonTripDelete;
    private javax.swing.JButton jButtonTripView;
    private javax.swing.JCheckBox jCheckBoxDutyFinished;
    private javax.swing.JCheckBox jCheckBoxPaid;
    private javax.swing.JCheckBox jCheckBoxPlaceVisited;
    private javax.swing.JComboBox<String> jComboBoxApartmentNewActivity;
    private javax.swing.JComboBox<String> jComboBoxCreate;
    private javax.swing.JComboBox<String> jComboBoxCreateUsers;
    private javax.swing.JComboBox<String> jComboBoxEventNew;
    private javax.swing.JComboBox<String> jComboBoxNewChargeUsers;
    private javax.swing.JComboBox<String> jComboBoxNewDutyUsers;
    private javax.swing.JComboBox<String> jComboBoxTripNewActivity;
    private javax.swing.JLabel jLabeTripTransport;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelApartment;
    private javax.swing.JLabel jLabelApartmentAddress;
    private javax.swing.JLabel jLabelApartmentGroupAddress;
    private javax.swing.JLabel jLabelApartmentGroupDescription;
    private javax.swing.JLabel jLabelApartmentGroupName;
    private javax.swing.JLabel jLabelApartmentGroupPot;
    private javax.swing.JLabel jLabelApartmentName;
    private javax.swing.JLabel jLabelApartmentPot;
    private javax.swing.JLabel jLabelArticle;
    private javax.swing.JLabel jLabelArticleMarket;
    private javax.swing.JLabel jLabelArticleMarketData;
    private javax.swing.JLabel jLabelArticlePriceData;
    private javax.swing.JLabel jLabelCharge;
    private javax.swing.JLabel jLabelChargeAmount;
    private javax.swing.JLabel jLabelChargeName;
    private javax.swing.JLabel jLabelDebt;
    private javax.swing.JLabel jLabelDebtDate;
    private javax.swing.JLabel jLabelDebtName;
    private javax.swing.JLabel jLabelDebtTotalCount;
    private javax.swing.JLabel jLabelDuty;
    private javax.swing.JLabel jLabelDutyDescription;
    private javax.swing.JLabel jLabelDutyEndDate;
    private javax.swing.JLabel jLabelDutyUser;
    private javax.swing.JLabel jLabelEvent;
    private javax.swing.JLabel jLabelEvent1;
    private javax.swing.JLabel jLabelEventDate;
    private javax.swing.JLabel jLabelEventDescription;
    private javax.swing.JLabel jLabelEventName;
    private javax.swing.JLabel jLabelEventPlace;
    private javax.swing.JLabel jLabelEventPot;
    private javax.swing.JLabel jLabelGroupsGroups;
    private javax.swing.JLabel jLabelGroupsHello;
    private javax.swing.JLabel jLabelJournalDebtDescription;
    private javax.swing.JLabel jLabelJournalPlace;
    private javax.swing.JLabel jLabelJournalPlaceDescription;
    private javax.swing.JLabel jLabelJournalPlaceEndDate;
    private javax.swing.JLabel jLabelJournalPlaceName;
    private javax.swing.JLabel jLabelJournalPlacePot;
    private javax.swing.JLabel jLabelLoggingError;
    private javax.swing.JLabel jLabelLogo;
    private javax.swing.JLabel jLabelManageUser;
    private javax.swing.JLabel jLabelNArticlePrice;
    private javax.swing.JLabel jLabelNewApartment;
    private javax.swing.JLabel jLabelNewApartmentAddress;
    private javax.swing.JLabel jLabelNewApartmentDescription;
    private javax.swing.JLabel jLabelNewApartmentError;
    private javax.swing.JLabel jLabelNewApartmentGroupName;
    private javax.swing.JLabel jLabelNewApartmentPot;
    private javax.swing.JLabel jLabelNewArticle;
    private javax.swing.JLabel jLabelNewArticleAmount;
    private javax.swing.JLabel jLabelNewArticleError;
    private javax.swing.JLabel jLabelNewArticleMarket;
    private javax.swing.JLabel jLabelNewArticleName;
    private javax.swing.JLabel jLabelNewArticlePrice;
    private javax.swing.JLabel jLabelNewCharge;
    private javax.swing.JLabel jLabelNewChargeAmount;
    private javax.swing.JLabel jLabelNewChargeError;
    private javax.swing.JLabel jLabelNewChargeName;
    private javax.swing.JLabel jLabelNewChargeUser;
    private javax.swing.JLabel jLabelNewChargeUser1;
    private javax.swing.JLabel jLabelNewDebt;
    private javax.swing.JLabel jLabelNewDebtDate;
    private javax.swing.JLabel jLabelNewDebtDescription;
    private javax.swing.JLabel jLabelNewDebtError;
    private javax.swing.JLabel jLabelNewDebtName;
    private javax.swing.JLabel jLabelNewDebtTotalCount;
    private javax.swing.JLabel jLabelNewDuty;
    private javax.swing.JLabel jLabelNewDutyDescription;
    private javax.swing.JLabel jLabelNewDutyEndDate;
    private javax.swing.JLabel jLabelNewDutyError;
    private javax.swing.JLabel jLabelNewDutyName;
    private javax.swing.JLabel jLabelNewDutyUser;
    private javax.swing.JLabel jLabelNewEventDate1;
    private javax.swing.JLabel jLabelNewEventDescription1;
    private javax.swing.JLabel jLabelNewEventError;
    private javax.swing.JLabel jLabelNewEventGroupName1;
    private javax.swing.JLabel jLabelNewEventPlace1;
    private javax.swing.JLabel jLabelNewEventPot1;
    private javax.swing.JLabel jLabelNewJournalPlace;
    private javax.swing.JLabel jLabelNewJournalPlaceDescription;
    private javax.swing.JLabel jLabelNewJournalPlaceEndDate;
    private javax.swing.JLabel jLabelNewJournalPlaceError;
    private javax.swing.JLabel jLabelNewJournalPlaceName;
    private javax.swing.JLabel jLabelNewPlace;
    private javax.swing.JLabel jLabelNewPlaceAddress;
    private javax.swing.JLabel jLabelNewPlaceDate;
    private javax.swing.JLabel jLabelNewPlaceDescription;
    private javax.swing.JLabel jLabelNewPlaceError;
    private javax.swing.JLabel jLabelNewPlaceJournalDescription;
    private javax.swing.JLabel jLabelNewPlaceName;
    private javax.swing.JLabel jLabelNewPlacePrice;
    private javax.swing.JLabel jLabelNewPruchaseDescription;
    private javax.swing.JLabel jLabelNewPruchaseError;
    private javax.swing.JLabel jLabelNewPruchaseName;
    private javax.swing.JLabel jLabelNewPurchase;
    private javax.swing.JLabel jLabelNewTask;
    private javax.swing.JLabel jLabelNewTaskDescription;
    private javax.swing.JLabel jLabelNewTaskEndDate;
    private javax.swing.JLabel jLabelNewTaskError;
    private javax.swing.JLabel jLabelNewTaskName;
    private javax.swing.JLabel jLabelNewTrip;
    private javax.swing.JLabel jLabelNewTripDate;
    private javax.swing.JLabel jLabelNewTripDescription;
    private javax.swing.JLabel jLabelNewTripError;
    private javax.swing.JLabel jLabelNewTripGroupName;
    private javax.swing.JLabel jLabelNewTripLodging;
    private javax.swing.JLabel jLabelNewTripPlace;
    private javax.swing.JLabel jLabelNewTripPot;
    private javax.swing.JLabel jLabelNewTripTransport;
    private javax.swing.JLabel jLabelPassword;
    private javax.swing.JLabel jLabelPlace;
    private javax.swing.JLabel jLabelPlaceAddress;
    private javax.swing.JLabel jLabelPlaceDate;
    private javax.swing.JLabel jLabelPlaceDescription;
    private javax.swing.JLabel jLabelPlaceJournalDescription;
    private javax.swing.JLabel jLabelPlaceName;
    private javax.swing.JLabel jLabelPlacePrice;
    private javax.swing.JLabel jLabelPruchaseDescription;
    private javax.swing.JLabel jLabelPruchaseName;
    private javax.swing.JLabel jLabelPruchasePot;
    private javax.swing.JLabel jLabelPruchasePot1;
    private javax.swing.JLabel jLabelPurchase;
    private javax.swing.JLabel jLabelRegisterError;
    private javax.swing.JLabel jLabelRegisterName;
    private javax.swing.JLabel jLabelRegisterPassword;
    private javax.swing.JLabel jLabelRegisterPassword1;
    private javax.swing.JLabel jLabelRegisterSurname;
    private javax.swing.JLabel jLabelRegisterUsername;
    private javax.swing.JLabel jLabelSignUp;
    private javax.swing.JLabel jLabelTask;
    private javax.swing.JLabel jLabelTaskDescription;
    private javax.swing.JLabel jLabelTaskEndDate;
    private javax.swing.JLabel jLabelTaskName;
    private javax.swing.JLabel jLabelTrip;
    private javax.swing.JLabel jLabelTripDate;
    private javax.swing.JLabel jLabelTripDescription;
    private javax.swing.JLabel jLabelTripLodging;
    private javax.swing.JLabel jLabelTripName;
    private javax.swing.JLabel jLabelTripPlace;
    private javax.swing.JLabel jLabelTripPot;
    private javax.swing.JLabel jLabelUsername;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuLogout;
    private javax.swing.JMenu jMenuProfile;
    private javax.swing.JPanel jPaneManagerUserList;
    private javax.swing.JPanel jPanelApartment;
    private javax.swing.JPanel jPanelApartmentDetailList;
    private javax.swing.JPanel jPanelArticle;
    private javax.swing.JPanel jPanelCharge;
    private javax.swing.JPanel jPanelDebt;
    private javax.swing.JPanel jPanelDebtList;
    private javax.swing.JPanel jPanelDuty;
    private javax.swing.JPanel jPanelEvent;
    private javax.swing.JPanel jPanelGroupList;
    private javax.swing.JPanel jPanelGroups;
    private javax.swing.JPanel jPanelJournalPlace;
    private javax.swing.JPanel jPanelJournalPlaceList;
    private javax.swing.JPanel jPanelLogging;
    private javax.swing.JPanel jPanelNewApartment;
    private javax.swing.JPanel jPanelNewArticle;
    private javax.swing.JPanel jPanelNewCharge;
    private javax.swing.JPanel jPanelNewDebt;
    private javax.swing.JPanel jPanelNewDuty;
    private javax.swing.JPanel jPanelNewEvent;
    private javax.swing.JPanel jPanelNewJournalPlace;
    private javax.swing.JPanel jPanelNewPlace;
    private javax.swing.JPanel jPanelNewPurchase;
    private javax.swing.JPanel jPanelNewTask;
    private javax.swing.JPanel jPanelNewTrip;
    private javax.swing.JPanel jPanelPlace;
    private javax.swing.JPanel jPanelPurchase;
    private javax.swing.JPanel jPanelPurchaseList;
    private javax.swing.JPanel jPanelRegister;
    private javax.swing.JPanel jPanelTask;
    private javax.swing.JPanel jPanelTaskList;
    private javax.swing.JPanel jPanelTrip;
    private javax.swing.JPasswordField jPasswordFieldRegister;
    private javax.swing.JPasswordField jPasswordFieldRegisterConfirm;
    private javax.swing.JPasswordField jPasswordPass;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPaneDebt;
    private javax.swing.JScrollPane jScrollPaneEvent;
    private javax.swing.JScrollPane jScrollPaneJournalPlace;
    private javax.swing.JScrollPane jScrollPaneNewApartment;
    private javax.swing.JScrollPane jScrollPaneNewDebt;
    private javax.swing.JScrollPane jScrollPaneNewDutyDescription;
    private javax.swing.JScrollPane jScrollPaneNewJournalPlace;
    private javax.swing.JScrollPane jScrollPaneNewPlaceDescription;
    private javax.swing.JScrollPane jScrollPaneNewPlaceJournalDescription;
    private javax.swing.JScrollPane jScrollPaneNewPruchase1;
    private javax.swing.JScrollPane jScrollPaneNewTrip;
    private javax.swing.JScrollPane jScrollPanelJournalPlace;
    private javax.swing.JScrollPane jScrollPanelPurchaseDescription;
    private javax.swing.JScrollPane jScrollPanelTask;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTableApartmentDetailTable;
    private javax.swing.JTable jTableDebt;
    private javax.swing.JTable jTableDuty;
    private javax.swing.JTable jTableManagerUserTable;
    private javax.swing.JTable jTablePlace;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JTextArea jTextAreaApartmentDetailDescription;
    private javax.swing.JTextArea jTextAreaDebtDescription;
    private javax.swing.JTextArea jTextAreaEventDescription;
    private javax.swing.JTextArea jTextAreaJournalPlace;
    private javax.swing.JTextArea jTextAreaNewApartmentDescription;
    private javax.swing.JTextArea jTextAreaNewDebtDescription;
    private javax.swing.JTextArea jTextAreaNewDuty;
    private javax.swing.JTextArea jTextAreaNewEventDescription1;
    private javax.swing.JTextArea jTextAreaNewJournalPlace;
    private javax.swing.JTextArea jTextAreaNewPruchaseDescription;
    private javax.swing.JTextArea jTextAreaNewTripDescription;
    private javax.swing.JTextArea jTextAreaPruchaseDescription;
    private javax.swing.JTextField jTextFieldApartmentDetailAddress;
    private javax.swing.JTextField jTextFieldApartmentDetailName1;
    private javax.swing.JTextField jTextFieldApartmentDetailPot;
    private javax.swing.JTextField jTextFieldChargeAmount;
    private javax.swing.JTextField jTextFieldChargeName;
    private javax.swing.JTextField jTextFieldChargeUser;
    private javax.swing.JTextField jTextFieldDebtDate;
    private javax.swing.JTextField jTextFieldDebtName;
    private javax.swing.JTextField jTextFieldDebtTotalCount;
    private javax.swing.JTextField jTextFieldDutyDescription;
    private javax.swing.JTextField jTextFieldDutyEndDate;
    private javax.swing.JTextField jTextFieldDutyUser;
    private javax.swing.JTextField jTextFieldEventDate;
    private javax.swing.JTextField jTextFieldEventName;
    private javax.swing.JTextField jTextFieldEventPlace;
    private javax.swing.JTextField jTextFieldEventPot;
    private javax.swing.JTextField jTextFieldJournalPlaceEndDate;
    private javax.swing.JTextField jTextFieldJournalPlaceName;
    private javax.swing.JTextField jTextFieldJournalPlacePot;
    private javax.swing.JTextField jTextFieldNewApartmentAddress;
    private javax.swing.JTextField jTextFieldNewApartmentGroupName;
    private javax.swing.JTextField jTextFieldNewApartmentPot;
    private javax.swing.JTextField jTextFieldNewArticleAmount;
    private javax.swing.JTextField jTextFieldNewArticleMarket;
    private javax.swing.JTextField jTextFieldNewArticleName;
    private javax.swing.JTextField jTextFieldNewArticlePrice;
    private javax.swing.JTextField jTextFieldNewChargeAmount;
    private javax.swing.JTextField jTextFieldNewChargeName;
    private javax.swing.JTextField jTextFieldNewDebtDate;
    private javax.swing.JTextField jTextFieldNewDebtName;
    private javax.swing.JTextField jTextFieldNewDebtTotalCount;
    private javax.swing.JTextField jTextFieldNewDutyEndDate;
    private javax.swing.JTextField jTextFieldNewDutyName;
    private javax.swing.JTextField jTextFieldNewEventDate1;
    private javax.swing.JTextField jTextFieldNewEventGroupName1;
    private javax.swing.JTextField jTextFieldNewEventPlace1;
    private javax.swing.JTextField jTextFieldNewEventPot1;
    private javax.swing.JTextField jTextFieldNewJournalPlaceEndDate;
    private javax.swing.JTextField jTextFieldNewJournalPlaceName;
    private javax.swing.JTextField jTextFieldNewPlaceAddress;
    private javax.swing.JTextField jTextFieldNewPlaceDate;
    private javax.swing.JTextField jTextFieldNewPlaceName;
    private javax.swing.JTextField jTextFieldNewPlacePrice;
    private javax.swing.JTextField jTextFieldNewPruchaseName;
    private javax.swing.JTextField jTextFieldNewTaskDescription;
    private javax.swing.JTextField jTextFieldNewTaskEndDate;
    private javax.swing.JTextField jTextFieldNewTaskName;
    private javax.swing.JTextField jTextFieldNewTaskName2;
    private javax.swing.JTextField jTextFieldNewTripDate;
    private javax.swing.JTextField jTextFieldNewTripGroupName;
    private javax.swing.JTextField jTextFieldNewTripLodging;
    private javax.swing.JTextField jTextFieldNewTripPlace;
    private javax.swing.JTextField jTextFieldNewTripPot;
    private javax.swing.JTextField jTextFieldNewTripTransport;
    private javax.swing.JTextField jTextFieldPlaceAddress;
    private javax.swing.JTextField jTextFieldPlaceDate;
    private javax.swing.JTextField jTextFieldPlaceDescription;
    private javax.swing.JTextField jTextFieldPlaceJournalDescription;
    private javax.swing.JTextField jTextFieldPlaceName;
    private javax.swing.JTextField jTextFieldPlacePrice;
    private javax.swing.JTextField jTextFieldPruchaseName;
    private javax.swing.JTextField jTextFieldPruchaseName1;
    private javax.swing.JTextField jTextFieldPruchaseTotal;
    private javax.swing.JTextField jTextFieldRegisterName;
    private javax.swing.JTextField jTextFieldRegisterSurname;
    private javax.swing.JTextField jTextFieldRegisterUsername;
    private javax.swing.JTextField jTextFieldTaskDescription;
    private javax.swing.JTextField jTextFieldTaskName;
    private javax.swing.JTextField jTextFieldTripDescription;
    private javax.swing.JTextField jTextFieldTripLodging;
    private javax.swing.JTextField jTextFieldTripName;
    private javax.swing.JTextField jTextFieldTripPlace;
    private javax.swing.JTextField jTextFieldTripPot;
    private javax.swing.JTextField jTextFieldTripTransport;
    private javax.swing.JTextField jTextFieldTripTransport1;
    private javax.swing.JTextField jTextFieldUsername;
    private javax.swing.JTable jTableGroup;
    private javax.swing.JScrollPane jScrollPane1;

    //messages
    protected static String messageLoginError = "Invalid user or password";
    protected static String messageWelcome = "Hello, ";
    protected static String messageRegisterErrorMatch = "Password not match";
    protected static String messageRegisterErrorTaken = "This username is already taken";
    protected static String messageErrorPot = "Fields numbers are incorrect";
    protected static String messageErrorGroupTaken = "Group is already in use";
    protected static String messageErrorActivityTaken = "Activity is already in use";
    protected static String messageErrorDate = "Date with incorrect format";
    protected static String messageErrorItemTaken = "Item is already in use";
    protected static String messageErrorPriceAmount = "Fields numbers are incorrect";
    protected static String messageErrorEverythingPaid = "Everything is paid";
    protected static String messageErrorNotPot = "There is not enough money in the pot";
    protected static String messageErrorEmpty = "Something obligatory field is empty";

    //other
    private GroupController controllerGroup;

    private List<Debt> debtList = new ArrayList<>();
    private List<Purchase> purchaseList = new ArrayList<>();
    private List<JournalPlace> journalPlaceList = new ArrayList<>();
    private List<Task> taskList = new ArrayList<>();

    private List<Article> articleList = new ArrayList<>();
    private List<Charge> chargeList = new ArrayList<>();
    private List<Duty> dutyList = new ArrayList<>();
    private List<Place> placeList = new ArrayList<>();

    private List<User> userList = new ArrayList<>();
    private List<User> notUserList = new ArrayList<>();

    private String[][] table;
    private String typeSelectedActivity;
    private int indexSelectedActivity;
    private int indexSelectedItem;
    private String typeSelectedItem;
    private int indexselectedUser;

    //MVC patron
    public TemisView() {
        controllerGroup = null;
    }

    public void setControllers(GroupController controllerGroup) {
        this.controllerGroup = controllerGroup;
        this.connect();
        initMenu();
        initLoginPanel();
        this.disconnect();
        this.setVisible(true);
    }

    public void moveToLogin() {
        this.connect();
        this.getContentPane().removeAll();
        this.initLoginPanel();
        this.setContentPane(this.jPanelLogging);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    public void moveToRegister(boolean modify, String name, String surname, String nickname) {
        this.connect();
        this.getContentPane().removeAll();
        this.initRegisterPanel();
        this.setContentPane(this.jPanelRegister);
        if (!modify) {
            this.jButtonRegisterRegister.setText("Register");
            this.jLabelSignUp.setText("Sing Up!");
            this.jTextFieldRegisterUsername.setEnabled(true);
        } else {
            this.jButtonRegisterRegister.setText("Acept");
            this.jLabelSignUp.setText("Profile");
            this.jTextFieldRegisterUsername.setEnabled(false);
            this.jTextFieldRegisterName.setText(name);
            this.jTextFieldRegisterSurname.setText(surname);
            this.jTextFieldRegisterUsername.setText(nickname);
        }
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    public void moveToGroup(String name) {
        this.connect();
        this.controllerGroup.setIndexSelectedGroup(-1);
        this.getContentPane().removeAll();
        this.controllerGroup.initGroupData();
        this.initGroupPanel();
        this.setContentPane(this.jPanelGroups);
        this.jLabelGroupsHello.setText(this.messageWelcome + name + "!");
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    public void moveToApartment() {
        this.connect();
        this.getContentPane().removeAll();
        this.initApartmentPanel();
        this.setContentPane(this.jPanelNewApartment);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    public void activeButtonLogoutMenu() {
        if (this.jMenuLogout.isEnabled()) {
            this.jMenuLogout.setEnabled(false);
            this.jMenuProfile.setEnabled(false);

            this.moveToLogin();
        }
    }

    public boolean jMenuProfileisActive() {
        return this.jMenuProfile.isEnabled();
    }

    public String getCreateSelectedItem() {
        return (String) this.jComboBoxCreate.getSelectedItem();
    }

    public int getSelectedGroupIndex() {
        return this.jTableGroup.getSelectedRow();
    }

    public void setRegisterError(String message) {
        this.jLabelRegisterError.setText(message);
    }

    public boolean isRegisterUsernameEnabled() {
        return this.jTextFieldRegisterUsername.isEnabled();
    }

    public Map<String, String> getUserRegisterData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", this.jTextFieldRegisterName.getText());
        map.put("surname", this.jTextFieldRegisterSurname.getText());
        map.put("username", this.jTextFieldRegisterUsername.getText());
        map.put("password", new String(this.jPasswordFieldRegister.getPassword()).trim());
        map.put("passwordConfirm", new String(this.jPasswordFieldRegisterConfirm.getPassword()).trim());
        return map;
    }

    public void changeLanguajeLogin(String languajeButton, String password, String username, String registerButton, String loginButton) {
        this.jButtonLanguage.setText(languajeButton);
        this.jLabelPassword.setText(password);
        this.jLabelUsername.setText(username);
        this.jButtonRegister.setText(registerButton);
        this.jButtonLogin.setText(loginButton);
    }

    public Map<String, String> getUserLoginData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("username", this.jTextFieldUsername.getText());
        map.put("password", new String(this.jPasswordPass.getPassword()));
        return map;
    }

    public void setNewApartmentError(String message) {
        this.jLabelNewApartmentError.setText(message);
    }

    public Map<String, String> getNewApartmentData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", this.jTextFieldNewApartmentGroupName.getText());
        map.put("address", this.jTextFieldNewApartmentAddress.getText());
        map.put("description", this.jTextAreaNewApartmentDescription.getText());
        map.put("pot", this.jTextFieldNewApartmentPot.getText());
        return map;
    }

    public void disableMenuButtons() {
        this.jMenuLogout.setEnabled(true);
        this.jMenuProfile.setEnabled(true);
    }

    public void setLoginError(String message) {
        this.jLabelLoggingError.setText(message);
    }

    private void initMenu() {
        jMenuBar = new javax.swing.JMenuBar();
        jMenuProfile = new javax.swing.JMenu();
        jMenuLogout = new javax.swing.JMenu();

        this.setResizable(false);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(620, 720));
        setMinimumSize(new java.awt.Dimension(620, 720));
        setPreferredSize(new java.awt.Dimension(620, 720));

        //jMenuBar.setBackground(new java.awt.Color(204, 102, 0));
        jMenuBar.setBorder(null);
        jMenuBar.setForeground(new java.awt.Color(153, 51, 0));
        jMenuBar.setFont(new java.awt.Font("Herculanum", 1, 18)); // NOI18N

        //jMenuProfile.setBackground(new java.awt.Color(204, 102, 0));
        jMenuProfile.setBorder(null);
        jMenuProfile.setForeground(new java.awt.Color(204, 102, 0));
        jMenuProfile.setText("Profile");

        jMenuProfile.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                controllerGroup.profileMenu();
            }
        }); //jMenuProfileMouseClicked(evt);

        jMenuBar.add(jMenuProfile);

        //jMenuLogout.setBackground(new java.awt.Color(204, 102, 0));
        jMenuLogout.setBorder(null);
        jMenuLogout.setForeground(new java.awt.Color(204, 102, 0));
        jMenuLogout.setText("Logout");
        jMenuLogout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                controllerGroup.logoutMenu();
            }
        }); //jMenuLogoutMouseClicked(evt);

        jMenuBar.add(jMenuLogout);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 747, Short.MAX_VALUE)
        );

        pack();
    }

    private void initLoginPanel() {
        jPanelLogging = new javax.swing.JPanel();
        jLabelLogo = new javax.swing.JLabel();
        jLabelUsername = new javax.swing.JLabel();
        jLabelPassword = new javax.swing.JLabel();
        jPasswordPass = new javax.swing.JPasswordField();
        jTextFieldUsername = new javax.swing.JTextField();
        jButtonRegister = new javax.swing.JButton();
        jButtonLogin = new javax.swing.JButton();
        jLabelLoggingError = new javax.swing.JLabel();
        jButtonLanguage = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(620, 720));
        setMinimumSize(new java.awt.Dimension(620, 720));
        setPreferredSize(new java.awt.Dimension(620, 720));

        jPanelLogging.setBackground(new java.awt.Color(255, 204, 102));
        jPanelLogging.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelLogging.setMinimumSize(new java.awt.Dimension(620, 720));
        jMenuLogout.setEnabled(false);
        jMenuProfile.setEnabled(false);

        ImageIcon imageIcon = new ImageIcon("./src/images/logo.png");

        jLabelLogo.setIcon(new ImageIcon(imageIcon.getImage().getScaledInstance(620, 300, Image.SCALE_DEFAULT)));

        jLabelUsername.setFont(new java.awt.Font("Herculanum", 0, 36));
        jLabelUsername.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelUsername.setForeground(new java.awt.Color(153, 102, 0));
        jLabelUsername.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelUsername.setText("Username");
        jLabelUsername.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelPassword.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelPassword.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPassword.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelPassword.setText("Password");
        jLabelPassword.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelPassword.setMaximumSize(new java.awt.Dimension(174, 47));
        jLabelPassword.setMinimumSize(new java.awt.Dimension(174, 47));
        jLabelPassword.setPreferredSize(new java.awt.Dimension(174, 47));

        jPasswordPass.setFont(new java.awt.Font("Source Sans Pro Black", 0, 18)); // NOI18N

        jTextFieldUsername.setFont(new java.awt.Font("Source Sans Pro Black", 0, 18)); // NOI18N

        jButtonRegister.setBackground(new java.awt.Color(255, 204, 153));
        jButtonRegister.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jButtonRegister.setText("Register");
        jButtonRegister.addActionListener(this.controllerGroup); //jButtonRegisterActionPerformed(evt);
        jButtonRegister.setActionCommand("jButtonRegister");

        jButtonLogin.setBackground(new java.awt.Color(255, 204, 153));
        jButtonLogin.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jButtonLogin.setText("Login");
        jButtonLogin.setMaximumSize(new java.awt.Dimension(127, 41));
        jButtonLogin.setMinimumSize(new java.awt.Dimension(127, 41));
        jButtonLogin.setPreferredSize(new java.awt.Dimension(127, 41));
        jButtonLogin.addActionListener(this.controllerGroup); //jButtonLoginActionPerformed(evt);
        jButtonLogin.setActionCommand("jButtonLogin");

        jLabelLoggingError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelLoggingError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelLoggingError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jButtonLanguage.setBackground(new java.awt.Color(255, 204, 153));
        jButtonLanguage.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jButtonLanguage.setText("Change languaje!");
        jButtonLanguage.addActionListener(this.controllerGroup);//jButtonLanguageActionPerformed(evt);
        jButtonLanguage.setActionCommand("jButtonLanguage");

        javax.swing.GroupLayout jPanelLoggingLayout = new javax.swing.GroupLayout(jPanelLogging);
        jPanelLogging.setLayout(jPanelLoggingLayout);
        jPanelLoggingLayout.setHorizontalGroup(
                jPanelLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabelLogo, javax.swing.GroupLayout.DEFAULT_SIZE, 1086, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelLoggingLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelLoggingLayout.createSequentialGroup()
                                                .addComponent(jButtonRegister)
                                                .addGap(72, 72, 72)
                                                .addComponent(jButtonLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(138, 138, 138))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelLoggingLayout.createSequentialGroup()
                                                .addGroup(jPanelLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jLabelLoggingError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelLoggingLayout.createSequentialGroup()
                                                                .addGroup(jPanelLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jLabelPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGroup(jPanelLoggingLayout.createSequentialGroup()
                                                                                .addGap(29, 29, 29)
                                                                                .addComponent(jLabelUsername)))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                                                                .addGroup(jPanelLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jPasswordPass, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(10, 10, 10)))
                                                .addGap(82, 82, 82))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelLoggingLayout.createSequentialGroup()
                                                .addComponent(jButtonLanguage)
                                                .addGap(181, 181, 181))))
        );
        jPanelLoggingLayout.setVerticalGroup(
                jPanelLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelLoggingLayout.createSequentialGroup()
                                .addComponent(jLabelLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 130, Short.MAX_VALUE)
                                .addComponent(jLabelLoggingError, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelUsername))
                                .addGap(12, 12, 12)
                                .addGroup(jPanelLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPasswordPass, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(45, 45, 45)
                                .addGroup(jPanelLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonRegister))
                                .addGap(18, 18, 18)
                                .addComponent(jButtonLanguage)
                                .addGap(57, 57, 57))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelLogging, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanelLogging, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        String path = TemisView.class.getPackage().getName() + ".messages";
        Locale locale = Locale.getDefault();
        ResourceBundle bundle = ResourceBundle.getBundle(path, locale);
        this.jButtonLanguage.setText(bundle.getString("cambialenguaje"));
        this.jLabelPassword.setText(bundle.getString("contrasena"));
        this.jLabelUsername.setText(bundle.getString("usuario"));
        this.jButtonRegister.setText(bundle.getString("registro"));
        this.jButtonLogin.setText(bundle.getString("entrar"));

        pack();
    }

    private void initRegisterPanel() {
        jPanelRegister = new javax.swing.JPanel();
        jLabelSignUp = new javax.swing.JLabel();
        jLabelRegisterName = new javax.swing.JLabel();
        jLabelRegisterSurname = new javax.swing.JLabel();
        jLabelRegisterUsername = new javax.swing.JLabel();
        jLabelRegisterPassword = new javax.swing.JLabel();
        jTextFieldRegisterName = new javax.swing.JTextField();
        jTextFieldRegisterSurname = new javax.swing.JTextField();
        jTextFieldRegisterUsername = new javax.swing.JTextField();
        jButtonRegisterCancel = new javax.swing.JButton();
        jButtonRegisterRegister = new javax.swing.JButton();
        jLabelRegisterError = new javax.swing.JLabel();
        jPasswordFieldRegisterConfirm = new javax.swing.JPasswordField();
        jLabelRegisterPassword1 = new javax.swing.JLabel();
        jPasswordFieldRegister = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelRegister.setBackground(new java.awt.Color(255, 204, 102));

        jLabelSignUp.setFont(new java.awt.Font("Showcard Gothic", 0, 55)); // NOI18N
        jLabelSignUp.setForeground(new java.awt.Color(153, 102, 0));
        jLabelSignUp.setText("Sign Up!");

        jLabelRegisterName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelRegisterName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelRegisterName.setText("Name");

        jLabelRegisterSurname.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelRegisterSurname.setForeground(new java.awt.Color(153, 102, 0));
        jLabelRegisterSurname.setText("Surname");

        jLabelRegisterUsername.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelRegisterUsername.setForeground(new java.awt.Color(153, 102, 0));
        jLabelRegisterUsername.setText("Username");

        jLabelRegisterPassword.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelRegisterPassword.setForeground(new java.awt.Color(153, 102, 0));
        jLabelRegisterPassword.setText("Password");

        jTextFieldRegisterName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldRegisterSurname.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldRegisterUsername.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonRegisterCancel.setBackground(new java.awt.Color(255, 204, 153));
        jButtonRegisterCancel.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jButtonRegisterCancel.setText("Cancel");
        jButtonRegisterCancel.setMaximumSize(new java.awt.Dimension(144, 33));
        jButtonRegisterCancel.setMinimumSize(new java.awt.Dimension(144, 33));
        jButtonRegisterCancel.addActionListener(this.controllerGroup); //jButtonRegisterCancelActionPerformed(evt);
        jButtonRegisterCancel.setActionCommand("jButtonRegisterCancel");

        jButtonRegisterRegister.setBackground(new java.awt.Color(255, 204, 153));
        jButtonRegisterRegister.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jButtonRegisterRegister.setText("Register");
        jButtonRegisterRegister.addActionListener(this.controllerGroup); //jButtonRegisterRegisterActionPerformed(evt);
        jButtonRegisterRegister.setActionCommand("jButtonRegisterRegister");

        jLabelRegisterError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelRegisterError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelRegisterError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jPasswordFieldRegisterConfirm.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jLabelRegisterPassword1.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelRegisterPassword1.setForeground(new java.awt.Color(153, 102, 0));
        jLabelRegisterPassword1.setText("Confirm Password");

        jPasswordFieldRegister.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanelRegisterLayout = new javax.swing.GroupLayout(jPanelRegister);
        jPanelRegister.setLayout(jPanelRegisterLayout);
        jPanelRegisterLayout.setHorizontalGroup(
                jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelRegisterLayout.createSequentialGroup()
                                .addGap(55, 55, 55)
                                .addGroup(jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelRegisterName)
                                        .addComponent(jLabelRegisterSurname)
                                        .addComponent(jLabelRegisterPassword)
                                        .addComponent(jLabelRegisterUsername)
                                        .addComponent(jLabelRegisterPassword1))
                                .addGap(62, 62, 62)
                                .addGroup(jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jPasswordFieldRegisterConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPasswordFieldRegister, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldRegisterUsername)
                                        .addComponent(jTextFieldRegisterSurname)
                                        .addComponent(jTextFieldRegisterName))
                                .addContainerGap(110, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelRegisterLayout.createSequentialGroup()
                                .addGap(108, 108, 108)
                                .addGroup(jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelRegisterError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(jPanelRegisterLayout.createSequentialGroup()
                                                .addComponent(jButtonRegisterCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jButtonRegisterRegister)))
                                .addGap(107, 107, 107))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelRegisterLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabelSignUp)
                                .addGap(193, 193, 193))
        );
        jPanelRegisterLayout.setVerticalGroup(
                jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelRegisterLayout.createSequentialGroup()
                                .addGap(43, 43, 43)
                                .addComponent(jLabelSignUp)
                                .addGroup(jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelRegisterLayout.createSequentialGroup()
                                                .addGap(94, 94, 94)
                                                .addComponent(jLabelRegisterName))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelRegisterLayout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jTextFieldRegisterName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelRegisterSurname)
                                        .addComponent(jTextFieldRegisterSurname, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelRegisterUsername)
                                        .addComponent(jTextFieldRegisterUsername, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelRegisterLayout.createSequentialGroup()
                                                .addGap(12, 12, 12)
                                                .addComponent(jLabelRegisterPassword))
                                        .addGroup(jPanelRegisterLayout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jPasswordFieldRegister, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(11, 11, 11)
                                .addGroup(jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jPasswordFieldRegisterConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelRegisterPassword1))
                                .addGap(37, 37, 37)
                                .addComponent(jLabelRegisterError, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(47, 47, 47)
                                .addGroup(jPanelRegisterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonRegisterCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonRegisterRegister))
                                .addContainerGap(215, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 599, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelRegister, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 764, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelRegister, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initGroupPanel() {
        jPanelGroups = new javax.swing.JPanel();
        jLabelGroupsHello = new javax.swing.JLabel();
        jLabelGroupsGroups = new javax.swing.JLabel();
        jButtonGroupsModify = new javax.swing.JButton();
        jButtonGroupsDelete = new javax.swing.JButton();
        jButtonGroupsView = new javax.swing.JButton();
        jComboBoxCreate = new javax.swing.JComboBox<>();
        jPanelGroupList = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableGroup = new javax.swing.JTable();

        this.jPanelGroupList.setBackground(new java.awt.Color(153, 102, 0));
        this.jScrollPane1.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableGroup.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableGroup.setGridColor(new java.awt.Color(153, 102, 0));

        if (this.controllerGroup.getTable() != null) {
            javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(this.controllerGroup.getTable(), this.controllerGroup.getHead()) {
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            this.jTableGroup.setModel(model);
            this.jScrollPane1.setViewportView(this.jTableGroup);
        }

        this.jTableGroup.setColumnSelectionAllowed(false);
        this.jTableGroup.setDragEnabled(false);
        this.jTableGroup.setRowSelectionAllowed(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelGroups.setBackground(new java.awt.Color(255, 204, 102));
        jPanelGroups.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelGroups.setMinimumSize(new java.awt.Dimension(620, 720));
        jPanelGroups.setName(""); // NOI18N

        jLabelGroupsHello.setFont(new java.awt.Font("Showcard Gothic", 1, 36)); // NOI18N
        jLabelGroupsHello.setForeground(new java.awt.Color(153, 102, 0));
        jLabelGroupsHello.setText("Hello, ");

        jLabelGroupsGroups.setFont(new java.awt.Font("Showcard Gothic", 1, 55)); // NOI18N
        jLabelGroupsGroups.setForeground(new java.awt.Color(153, 102, 0));
        jLabelGroupsGroups.setText("Groups");
        jLabelGroupsGroups.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jButtonGroupsModify.setBackground(new java.awt.Color(153, 102, 0));
        jButtonGroupsModify.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonGroupsModify.setForeground(new java.awt.Color(0, 0, 0));
        jButtonGroupsModify.setText("Users");
        jButtonGroupsModify.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonGroupsModify.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonGroupsModify.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonGroupsModify.addActionListener(this.controllerGroup); //jButtonGroupsModifyActionPerformed(evt);
        jButtonGroupsModify.setActionCommand("jButtonGroupsModify");

        jButtonGroupsDelete.setBackground(new java.awt.Color(153, 102, 0));
        jButtonGroupsDelete.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonGroupsDelete.setForeground(new java.awt.Color(0, 0, 0));
        jButtonGroupsDelete.setText("Delete");
        jButtonGroupsDelete.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonGroupsDelete.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonGroupsDelete.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonGroupsDelete.addActionListener(this.controllerGroup); //jButtonGroupsDeleteActionPerformed(evt);
        jButtonGroupsDelete.setActionCommand("jButtonGroupsDelete");

        jButtonGroupsView.setBackground(new java.awt.Color(153, 102, 0));
        jButtonGroupsView.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonGroupsView.setForeground(new java.awt.Color(0, 0, 0));
        jButtonGroupsView.setText("View");
        jButtonGroupsView.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonGroupsView.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonGroupsView.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonGroupsView.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonGroupsView.addActionListener(this.controllerGroup); //jButtonGroupsViewActionPerformed(evt);
        jButtonGroupsView.setActionCommand("jButtonGroupsView");

        jComboBoxCreate.setBackground(new java.awt.Color(153, 102, 0));
        jComboBoxCreate.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jComboBoxCreate.setForeground(new java.awt.Color(0, 0, 0));
        jComboBoxCreate.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"Apartment"}));
        jComboBoxCreate.setMaximumSize(new java.awt.Dimension(114, 29));
        jComboBoxCreate.setMinimumSize(new java.awt.Dimension(114, 29));
        jComboBoxCreate.setPreferredSize(new java.awt.Dimension(114, 29));
        jComboBoxCreate.addActionListener(this.controllerGroup); //jComboBoxCreateActionPerformed(evt);
        jComboBoxCreate.setActionCommand("jComboBoxCreate");

        javax.swing.GroupLayout jPanelGroupListLayout = new javax.swing.GroupLayout(jPanelGroupList);
        jPanelGroupList.setLayout(jPanelGroupListLayout);
        jPanelGroupListLayout.setHorizontalGroup(
                jPanelGroupListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1)
        );
        jPanelGroupListLayout.setVerticalGroup(
                jPanelGroupListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanelGroupsLayout = new javax.swing.GroupLayout(jPanelGroups);
        jPanelGroups.setLayout(jPanelGroupsLayout);
        jPanelGroupsLayout.setHorizontalGroup(
                jPanelGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelGroupsLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabelGroupsGroups)
                                .addGap(174, 174, 174))
                        .addGroup(jPanelGroupsLayout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addGroup(jPanelGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelGroupsLayout.createSequentialGroup()
                                                .addComponent(jLabelGroupsHello)
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelGroupsLayout.createSequentialGroup()
                                                .addGroup(jPanelGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jPanelGroupList, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelGroupsLayout.createSequentialGroup()
                                                                .addComponent(jButtonGroupsModify, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(31, 31, 31)
                                                                .addComponent(jButtonGroupsDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(28, 28, 28)
                                                                .addComponent(jButtonGroupsView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                                                                .addComponent(jComboBoxCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(37, 37, 37))))
        );
        jPanelGroupsLayout.setVerticalGroup(
                jPanelGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelGroupsLayout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(jLabelGroupsHello)
                                .addGap(23, 23, 23)
                                .addComponent(jLabelGroupsGroups)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelGroupsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonGroupsModify, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonGroupsDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonGroupsView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelGroupList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(18, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 621, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelGroups, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 1, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 764, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelGroups, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    //end MVC Patron
    private void initApartmentPanel() {
        jPanelNewApartment = new javax.swing.JPanel();
        jLabelNewApartment = new javax.swing.JLabel();
        jLabelNewApartmentGroupName = new javax.swing.JLabel();
        jLabelNewApartmentPot = new javax.swing.JLabel();
        jLabelNewApartmentDescription = new javax.swing.JLabel();
        jLabelNewApartmentAddress = new javax.swing.JLabel();
        jScrollPaneNewApartment = new javax.swing.JScrollPane();
        jTextAreaNewApartmentDescription = new javax.swing.JTextArea();
        jTextFieldNewApartmentGroupName = new javax.swing.JTextField();
        jTextFieldNewApartmentAddress = new javax.swing.JTextField();
        jTextFieldNewApartmentPot = new javax.swing.JTextField();
        jButtonNewApartmentCreate = new javax.swing.JButton();
        jButtonNewApartmentCancel = new javax.swing.JButton();
        jLabelNewApartmentError = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewApartment.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewApartment.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewApartment.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewApartment.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewApartment.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewApartment.setText("New Apartment");
        jLabelNewApartment.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewApartmentGroupName.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewApartmentGroupName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewApartmentGroupName.setText("Group name");
        jLabelNewApartmentGroupName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewApartmentPot.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewApartmentPot.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewApartmentPot.setText("Pot");
        jLabelNewApartmentPot.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewApartmentDescription.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewApartmentDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewApartmentDescription.setText("Description");
        jLabelNewApartmentDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewApartmentAddress.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewApartmentAddress.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewApartmentAddress.setText("Address");
        jLabelNewApartmentAddress.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextAreaNewApartmentDescription.setColumns(20);
        jTextAreaNewApartmentDescription.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextAreaNewApartmentDescription.setRows(5);
        jScrollPaneNewApartment.setViewportView(jTextAreaNewApartmentDescription);

        jTextFieldNewApartmentGroupName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18

        jTextFieldNewApartmentAddress.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewApartmentPot.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewApartmentCreate.setBackground(new java.awt.Color(255, 204, 153));
        jButtonNewApartmentCreate.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jButtonNewApartmentCreate.setText("Create");
        jButtonNewApartmentCreate.addActionListener(this.controllerGroup);
        jButtonNewApartmentCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewApartmentCreateActionPerformed(evt);
            }
        });

        jButtonNewApartmentCancel.setBackground(new java.awt.Color(255, 204, 153));
        jButtonNewApartmentCancel.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jButtonNewApartmentCancel.setText("Cancel");
        jButtonNewApartmentCancel.addActionListener(this.controllerGroup);
        jButtonNewApartmentCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewApartmentCancelActionPerformed(evt);
            }
        });

        jLabelNewApartmentError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewApartmentError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelNewApartmentError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanelNewApartmentLayout = new javax.swing.GroupLayout(jPanelNewApartment);
        jPanelNewApartment.setLayout(jPanelNewApartmentLayout);
        jPanelNewApartmentLayout.setHorizontalGroup(
                jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewApartmentLayout.createSequentialGroup()
                                .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelNewApartmentLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(jLabelNewApartmentError, javax.swing.GroupLayout.PREFERRED_SIZE, 438, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelNewApartmentLayout.createSequentialGroup()
                                                .addGap(102, 102, 102)
                                                .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewApartmentLayout.createSequentialGroup()
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jLabelNewApartmentDescription)
                                                                        .addGap(139, 139, 139))
                                                                .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(jScrollPaneNewApartment)
                                                                        .addGroup(jPanelNewApartmentLayout.createSequentialGroup()
                                                                                .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                        .addComponent(jLabelNewApartmentAddress)
                                                                                        .addComponent(jLabelNewApartmentPot)
                                                                                        .addComponent(jLabelNewApartmentGroupName))
                                                                                .addGap(55, 55, 55)
                                                                                .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                                        .addComponent(jTextFieldNewApartmentAddress, javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(jTextFieldNewApartmentPot, javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(jTextFieldNewApartmentGroupName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                                        .addGroup(jPanelNewApartmentLayout.createSequentialGroup()
                                                                .addComponent(jButtonNewApartmentCancel)
                                                                .addGap(120, 120, 120)
                                                                .addComponent(jButtonNewApartmentCreate)
                                                                .addGap(39, 39, 39))
                                                        .addComponent(jLabelNewApartment))))
                                .addContainerGap(80, Short.MAX_VALUE))
        );
        jPanelNewApartmentLayout.setVerticalGroup(
                jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewApartmentLayout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addComponent(jLabelNewApartment)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNewApartmentError, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldNewApartmentGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNewApartmentGroupName))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNewApartmentAddress)
                                        .addGroup(jPanelNewApartmentLayout.createSequentialGroup()
                                                .addGap(2, 2, 2)
                                                .addComponent(jTextFieldNewApartmentAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelNewApartmentPot)
                                        .addComponent(jTextFieldNewApartmentPot, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNewApartmentDescription)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPaneNewApartment, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(31, 31, 31)
                                .addGroup(jPanelNewApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewApartmentCancel)
                                        .addComponent(jButtonNewApartmentCreate))
                                .addGap(42, 42, 42))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 621, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewApartment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 764, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewApartment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initEventPanel() {
        jPanelNewEvent = new javax.swing.JPanel();
        jLabelEvent1 = new javax.swing.JLabel();
        jLabelNewEventGroupName1 = new javax.swing.JLabel();
        jLabelNewEventPot1 = new javax.swing.JLabel();
        jLabelNewEventDescription1 = new javax.swing.JLabel();
        jLabelNewEventPlace1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaNewEventDescription1 = new javax.swing.JTextArea();
        jTextFieldNewEventGroupName1 = new javax.swing.JTextField();
        jTextFieldNewEventPlace1 = new javax.swing.JTextField();
        jTextFieldNewEventPot1 = new javax.swing.JTextField();
        jButtonNewEventCreate1 = new javax.swing.JButton();
        jLabelNewEventDate1 = new javax.swing.JLabel();
        jTextFieldNewEventDate1 = new javax.swing.JTextField();
        jButtonNewEventCancel = new javax.swing.JButton();
        jLabelNewEventError = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewEvent.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewEvent.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewEvent.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelEvent1.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelEvent1.setForeground(new java.awt.Color(153, 102, 0));
        jLabelEvent1.setText("New Event");
        jLabelEvent1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewEventGroupName1.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewEventGroupName1.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewEventGroupName1.setText("Group name");
        jLabelNewEventGroupName1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewEventPot1.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewEventPot1.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewEventPot1.setText("Pot");
        jLabelNewEventPot1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewEventDescription1.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewEventDescription1.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewEventDescription1.setText("Description");
        jLabelNewEventDescription1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewEventPlace1.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewEventPlace1.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewEventPlace1.setText("Place");
        jLabelNewEventPlace1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextAreaNewEventDescription1.setColumns(20);
        jTextAreaNewEventDescription1.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextAreaNewEventDescription1.setRows(5);
        jScrollPane2.setViewportView(jTextAreaNewEventDescription1);

        jTextFieldNewEventGroupName1.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewEventPlace1.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewEventPot1.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewEventCreate1.setBackground(new java.awt.Color(255, 204, 153));
        jButtonNewEventCreate1.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jButtonNewEventCreate1.setText("Create");
        jButtonNewEventCreate1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewEventCreate1ActionPerformed(evt);
            }
        });

        jLabelNewEventDate1.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewEventDate1.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewEventDate1.setText("Date");
        jLabelNewEventDate1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldNewEventDate1.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewEventCancel.setBackground(new java.awt.Color(255, 204, 153));
        jButtonNewEventCancel.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jButtonNewEventCancel.setText("Cancel");
        jButtonNewEventCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewEventCancelActionPerformed(evt);
            }
        });

        jLabelNewEventError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewEventError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelNewEventError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanelNewEventLayout = new javax.swing.GroupLayout(jPanelNewEvent);
        jPanelNewEvent.setLayout(jPanelNewEventLayout);
        jPanelNewEventLayout.setHorizontalGroup(
                jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewEventLayout.createSequentialGroup()
                                .addContainerGap(106, Short.MAX_VALUE)
                                .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewEventLayout.createSequentialGroup()
                                                .addComponent(jLabelEvent1)
                                                .addGap(163, 163, 163))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewEventLayout.createSequentialGroup()
                                                .addComponent(jButtonNewEventCancel)
                                                .addGap(126, 126, 126)
                                                .addComponent(jButtonNewEventCreate1)
                                                .addGap(115, 115, 115))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewEventLayout.createSequentialGroup()
                                                .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewEventLayout.createSequentialGroup()
                                                                .addGap(142, 142, 142)
                                                                .addComponent(jLabelNewEventDescription1)
                                                                .addGap(143, 143, 143))
                                                        .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(jScrollPane2)
                                                                .addGroup(jPanelNewEventLayout.createSequentialGroup()
                                                                        .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                .addComponent(jLabelNewEventPot1)
                                                                                .addComponent(jLabelNewEventDate1)
                                                                                .addComponent(jLabelNewEventGroupName1)
                                                                                .addComponent(jLabelNewEventPlace1))
                                                                        .addGap(55, 55, 55)
                                                                        .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                                .addComponent(jTextFieldNewEventPlace1)
                                                                                .addComponent(jTextFieldNewEventGroupName1)
                                                                                .addComponent(jTextFieldNewEventDate1)
                                                                                .addComponent(jTextFieldNewEventPot1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addComponent(jLabelNewEventError, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 438, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(76, 76, 76))))
        );
        jPanelNewEventLayout.setVerticalGroup(
                jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewEventLayout.createSequentialGroup()
                                .addGap(43, 43, 43)
                                .addComponent(jLabelEvent1)
                                .addGap(26, 26, 26)
                                .addComponent(jLabelNewEventError, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewEventGroupName1)
                                        .addComponent(jTextFieldNewEventGroupName1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldNewEventPlace1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNewEventPlace1))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNewEventDate1)
                                        .addComponent(jTextFieldNewEventDate1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jTextFieldNewEventPot1)
                                        .addComponent(jLabelNewEventPot1))
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNewEventDescription1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewEventCreate1)
                                        .addComponent(jButtonNewEventCancel))
                                .addContainerGap(54, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 621, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 764, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initTripPanel() {
        jPanelNewTrip = new javax.swing.JPanel();
        jLabelNewTrip = new javax.swing.JLabel();
        jLabelNewTripGroupName = new javax.swing.JLabel();
        jLabelNewTripPot = new javax.swing.JLabel();
        jLabelNewTripDescription = new javax.swing.JLabel();
        jLabelNewTripPlace = new javax.swing.JLabel();
        jScrollPaneNewTrip = new javax.swing.JScrollPane();
        jTextAreaNewTripDescription = new javax.swing.JTextArea();
        jTextFieldNewTripGroupName = new javax.swing.JTextField();
        jTextFieldNewTripPlace = new javax.swing.JTextField();
        jTextFieldNewTripLodging = new javax.swing.JTextField();
        jButtonNewTripCancel = new javax.swing.JButton();
        jLabelNewTripDate = new javax.swing.JLabel();
        jLabelNewTripLodging = new javax.swing.JLabel();
        jLabelNewTripTransport = new javax.swing.JLabel();
        jTextFieldNewTripDate = new javax.swing.JTextField();
        jTextFieldNewTripTransport = new javax.swing.JTextField();
        jTextFieldNewTripPot = new javax.swing.JTextField();
        jButtonNewTripCreate1 = new javax.swing.JButton();
        jLabelNewTripError = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewTrip.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewTrip.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewTrip.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewTrip.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewTrip.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTrip.setText("New Trip");
        jLabelNewTrip.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewTripGroupName.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewTripGroupName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTripGroupName.setText("Group name");
        jLabelNewTripGroupName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewTripPot.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewTripPot.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTripPot.setText("Pot");
        jLabelNewTripPot.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewTripDescription.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewTripDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTripDescription.setText("Description");
        jLabelNewTripDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewTripPlace.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewTripPlace.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTripPlace.setText("Place");
        jLabelNewTripPlace.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextAreaNewTripDescription.setColumns(20);
        jTextAreaNewTripDescription.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextAreaNewTripDescription.setRows(5);
        jScrollPaneNewTrip.setViewportView(jTextAreaNewTripDescription);

        jTextFieldNewTripGroupName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewTripPlace.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewTripLodging.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewTripCancel.setBackground(new java.awt.Color(255, 204, 153));
        jButtonNewTripCancel.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jButtonNewTripCancel.setText("Cancel");
        jButtonNewTripCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewTripCancelActionPerformed(evt);
            }
        });

        jLabelNewTripDate.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewTripDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTripDate.setText("Date");
        jLabelNewTripDate.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewTripLodging.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewTripLodging.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTripLodging.setText("Lodging");
        jLabelNewTripLodging.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewTripTransport.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewTripTransport.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTripTransport.setText("Transport");
        jLabelNewTripTransport.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldNewTripDate.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewTripTransport.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewTripPot.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewTripCreate1.setBackground(new java.awt.Color(255, 204, 153));
        jButtonNewTripCreate1.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jButtonNewTripCreate1.setText("Create");
        jButtonNewTripCreate1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewTripCreate1ActionPerformed(evt);
            }
        });

        jLabelNewTripError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewTripError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelNewTripError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanelNewTripLayout = new javax.swing.GroupLayout(jPanelNewTrip);
        jPanelNewTrip.setLayout(jPanelNewTripLayout);
        jPanelNewTripLayout.setHorizontalGroup(
                jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewTripLayout.createSequentialGroup()
                                .addGap(79, 79, 79)
                                .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewTripLayout.createSequentialGroup()
                                                .addComponent(jLabelNewTripDescription)
                                                .addGap(131, 131, 131))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(jPanelNewTripLayout.createSequentialGroup()
                                                        .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addComponent(jLabelNewTripGroupName)
                                                                .addComponent(jLabelNewTripPlace)
                                                                .addComponent(jLabelNewTripDate)
                                                                .addComponent(jLabelNewTripLodging)
                                                                .addComponent(jLabelNewTripTransport)
                                                                .addComponent(jLabelNewTripPot))
                                                        .addGap(68, 68, 68)
                                                        .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jTextFieldNewTripGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(jTextFieldNewTripDate, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(jTextFieldNewTripPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(jTextFieldNewTripLodging, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(jTextFieldNewTripTransport, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(jTextFieldNewTripPot, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelNewTripLayout.createSequentialGroup()
                                                        .addGap(33, 33, 33)
                                                        .addComponent(jScrollPaneNewTrip, javax.swing.GroupLayout.PREFERRED_SIZE, 418, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jLabelNewTripError, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addContainerGap(90, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewTripLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewTripLayout.createSequentialGroup()
                                                .addComponent(jButtonNewTripCancel)
                                                .addGap(121, 121, 121)
                                                .addComponent(jButtonNewTripCreate1)
                                                .addGap(90, 90, 90))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewTripLayout.createSequentialGroup()
                                                .addComponent(jLabelNewTrip)
                                                .addGap(180, 180, 180))))
        );
        jPanelNewTripLayout.setVerticalGroup(
                jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewTripLayout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addComponent(jLabelNewTrip)
                                .addGap(21, 21, 21)
                                .addComponent(jLabelNewTripError, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldNewTripGroupName, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNewTripGroupName))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNewTripPlace)
                                        .addComponent(jTextFieldNewTripPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextFieldNewTripDate, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNewTripDate))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNewTripLodging)
                                        .addComponent(jTextFieldNewTripLodging, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNewTripTransport)
                                        .addComponent(jTextFieldNewTripTransport, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelNewTripPot)
                                        .addComponent(jTextFieldNewTripPot, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelNewTripDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPaneNewTrip, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewTripLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewTripCancel)
                                        .addComponent(jButtonNewTripCreate1))
                                .addContainerGap(47, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 621, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewTrip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 764, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewTrip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initApartmentDetailPanel() {
        jPanelApartment = new javax.swing.JPanel();
        jLabelApartment = new javax.swing.JLabel();
        jLabelApartmentGroupName = new javax.swing.JLabel();
        jLabelApartmentGroupPot = new javax.swing.JLabel();
        jLabelApartmentGroupDescription = new javax.swing.JLabel();
        jLabelApartmentGroupAddress = new javax.swing.JLabel();
        jLabelApartmentName = new javax.swing.JLabel();
        jLabelApartmentAddress = new javax.swing.JLabel();
        jLabelApartmentPot = new javax.swing.JLabel();
        jComboBoxApartmentNewActivity = new javax.swing.JComboBox<>();
        jTextFieldApartmentDetailAddress = new javax.swing.JTextField();
        jTextFieldApartmentDetailName1 = new javax.swing.JTextField();
        jTextFieldApartmentDetailPot = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextAreaApartmentDetailDescription = new javax.swing.JTextArea();
        jPanelApartmentDetailList = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableApartmentDetailTable = new javax.swing.JTable();
        jButtonApartmentDetailView = new javax.swing.JButton();
        jButtonApartmentDetailDelete = new javax.swing.JButton();
        jButtonApartmentBack1 = new javax.swing.JButton();

        String[] head = {"Type", "Name"};
        this.table = null;

        if (this.debtList.size() > 0 || this.journalPlaceList.size() > 0 || this.purchaseList.size() > 0 || taskList.size() > 0) {
            table = new String[this.debtList.size() + this.journalPlaceList.size() + this.purchaseList.size() + taskList.size()][head.length];
        }

        this.jPanelApartmentDetailList.setBackground(new java.awt.Color(153, 102, 0));
        this.jScrollPane4.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableApartmentDetailTable.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableApartmentDetailTable.setGridColor(new java.awt.Color(153, 102, 0));

        int i = 0;
        for (Debt activity : this.debtList) {
            table[i][0] = "Debt";
            table[i][1] = activity.getName();
            i++;
        }
        for (JournalPlace activity : this.journalPlaceList) {
            table[i][0] = "Journal Place";
            table[i][1] = activity.getName();
            i++;
        }

        for (Purchase activity : this.purchaseList) {
            table[i][0] = "Purchase";
            table[i][1] = activity.getName();
            i++;
        }

        for (Task activity : this.taskList) {
            table[i][0] = "Task";
            table[i][1] = activity.getName();
            i++;
        }

        if (this.debtList.size() > 0 || this.journalPlaceList.size() > 0 || this.purchaseList.size() > 0 || taskList.size() > 0) {
            javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(table, head) {
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            this.jTableApartmentDetailTable.setModel(model);
        }
        this.jTableApartmentDetailTable.setColumnSelectionAllowed(false);
        this.jTableApartmentDetailTable.setDragEnabled(false);
        this.jTableApartmentDetailTable.setRowSelectionAllowed(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelApartment.setBackground(new java.awt.Color(255, 204, 102));
        jPanelApartment.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelApartment.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelApartment.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelApartment.setForeground(new java.awt.Color(153, 102, 0));
        jLabelApartment.setText("Apartment");
        jLabelApartment.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelApartmentGroupName.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelApartmentGroupName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelApartmentGroupName.setText("name");
        jLabelApartmentGroupName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelApartmentGroupPot.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelApartmentGroupPot.setForeground(new java.awt.Color(153, 102, 0));
        jLabelApartmentGroupPot.setText("Pot");
        jLabelApartmentGroupPot.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelApartmentGroupDescription.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelApartmentGroupDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelApartmentGroupDescription.setText("Description");
        jLabelApartmentGroupDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelApartmentGroupAddress.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelApartmentGroupAddress.setForeground(new java.awt.Color(153, 102, 0));
        jLabelApartmentGroupAddress.setText("Address");
        jLabelApartmentGroupAddress.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelApartmentName.setFont(new java.awt.Font("Showcard Gothic", 0, 14)); // NOI18N

        jLabelApartmentAddress.setFont(new java.awt.Font("Showcard Gothic", 0, 14)); // NOI18N

        jLabelApartmentPot.setFont(new java.awt.Font("Showcard Gothic", 0, 14)); // NOI18N

        jComboBoxApartmentNewActivity.setBackground(new java.awt.Color(153, 102, 0));
        jComboBoxApartmentNewActivity.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jComboBoxApartmentNewActivity.setForeground(new java.awt.Color(0, 0, 0));
        jComboBoxApartmentNewActivity.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"Debt", "Journal Place", "Purchase", "Task"}));
        jComboBoxApartmentNewActivity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxApartmentNewActivityActionPerformed(evt);
            }
        });

        jTextFieldApartmentDetailAddress.setEditable(false);
        jTextFieldApartmentDetailAddress.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldApartmentDetailAddress.setEnabled(false);

        jTextFieldApartmentDetailName1.setEditable(false);
        jTextFieldApartmentDetailName1.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldApartmentDetailName1.setEnabled(false);

        jTextFieldApartmentDetailPot.setEditable(false);
        jTextFieldApartmentDetailPot.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldApartmentDetailPot.setEnabled(false);

        jTextAreaApartmentDetailDescription.setEditable(false);
        jTextAreaApartmentDetailDescription.setColumns(10);
        jTextAreaApartmentDetailDescription.setRows(5);
        jTextAreaApartmentDetailDescription.setEnabled(false);
        jScrollPane3.setViewportView(jTextAreaApartmentDetailDescription);

        jScrollPane4.setViewportView(jTableApartmentDetailTable);

        javax.swing.GroupLayout jPanelApartmentDetailListLayout = new javax.swing.GroupLayout(jPanelApartmentDetailList);
        jPanelApartmentDetailList.setLayout(jPanelApartmentDetailListLayout);
        jPanelApartmentDetailListLayout.setHorizontalGroup(
                jPanelApartmentDetailListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 513, Short.MAX_VALUE)
        );
        jPanelApartmentDetailListLayout.setVerticalGroup(
                jPanelApartmentDetailListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelApartmentDetailListLayout.createSequentialGroup()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );

        jButtonApartmentDetailView.setBackground(new java.awt.Color(153, 102, 0));
        jButtonApartmentDetailView.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonApartmentDetailView.setForeground(new java.awt.Color(0, 0, 0));
        jButtonApartmentDetailView.setText("View");
        jButtonApartmentDetailView.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonApartmentDetailView.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonApartmentDetailView.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonApartmentDetailView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonApartmentDetailViewActionPerformed(evt);
            }
        });

        jButtonApartmentDetailDelete.setBackground(new java.awt.Color(153, 102, 0));
        jButtonApartmentDetailDelete.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonApartmentDetailDelete.setForeground(new java.awt.Color(0, 0, 0));
        jButtonApartmentDetailDelete.setText("Delete");
        jButtonApartmentDetailDelete.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonApartmentDetailDelete.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonApartmentDetailDelete.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonApartmentDetailDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonApartmentDetailDeleteActionPerformed(evt);
            }
        });

        jButtonApartmentBack1.setBackground(new java.awt.Color(153, 102, 0));
        jButtonApartmentBack1.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonApartmentBack1.setForeground(new java.awt.Color(0, 0, 0));
        jButtonApartmentBack1.setText("Back");
        jButtonApartmentBack1.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonApartmentBack1.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonApartmentBack1.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonApartmentBack1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonApartmentBack1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelApartmentLayout = new javax.swing.GroupLayout(jPanelApartment);
        jPanelApartment.setLayout(jPanelApartmentLayout);
        jPanelApartmentLayout.setHorizontalGroup(
                jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                                .addGap(115, 115, 115)
                                                                .addComponent(jLabelApartmentAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 475, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelApartmentLayout.createSequentialGroup()
                                                                                .addComponent(jLabelApartmentGroupName)
                                                                                .addGap(39, 39, 39)
                                                                                .addComponent(jTextFieldApartmentDetailName1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                                                .addComponent(jLabelApartmentGroupDescription)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                                                .addComponent(jLabelApartmentGroupPot)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(jTextFieldApartmentDetailPot, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addGap(100, 100, 100)
                                                                                .addComponent(jLabelApartmentName, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addComponent(jComboBoxApartmentNewActivity, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(32, 32, 32)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabelApartmentPot, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jButtonApartmentDetailView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jButtonApartmentDetailDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                                .addComponent(jLabelApartmentGroupAddress)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jTextFieldApartmentDetailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jPanelApartmentDetailList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(0, 0, Short.MAX_VALUE))))
                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                .addGap(158, 158, 158)
                                                .addComponent(jLabelApartment))
                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                .addGap(237, 237, 237)
                                                .addComponent(jButtonApartmentBack1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelApartmentLayout.setVerticalGroup(
                jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                .addComponent(jLabelApartment)
                                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                                .addGap(19, 19, 19)
                                                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                        .addComponent(jLabelApartmentGroupName)
                                                                        .addComponent(jLabelApartmentName)
                                                                        .addComponent(jLabelApartmentPot)))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelApartmentLayout.createSequentialGroup()
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jTextFieldApartmentDetailName1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jLabelApartmentGroupPot, javax.swing.GroupLayout.Alignment.TRAILING)))))
                                        .addComponent(jTextFieldApartmentDetailPot, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelApartmentGroupAddress)
                                        .addComponent(jTextFieldApartmentDetailAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(4, 4, 4)
                                .addComponent(jLabelApartmentAddress)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelApartmentGroupDescription)
                                        .addComponent(jComboBoxApartmentNewActivity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelApartmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelApartmentLayout.createSequentialGroup()
                                                .addGap(15, 15, 15)
                                                .addComponent(jButtonApartmentDetailView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonApartmentDetailDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelApartmentDetailList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonApartmentBack1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelApartment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelApartment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initnewPurchasePanel() {
        jPanelNewPurchase = new javax.swing.JPanel();
        jLabelNewPurchase = new javax.swing.JLabel();
        jLabelNewPruchaseName = new javax.swing.JLabel();
        jLabelNewPruchaseDescription = new javax.swing.JLabel();
        jScrollPaneNewPruchase1 = new javax.swing.JScrollPane();
        jTextAreaNewPruchaseDescription = new javax.swing.JTextArea();
        jTextFieldNewPruchaseName = new javax.swing.JTextField();
        jButtonNewPruchaseCreate = new javax.swing.JButton();
        jButtonNewPruchaseCancel = new javax.swing.JButton();
        jLabelNewPruchaseError = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewPurchase.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewPurchase.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewPurchase.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewPurchase.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewPurchase.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPurchase.setText("New Purchase");
        jLabelNewPurchase.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewPruchaseName.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewPruchaseName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPruchaseName.setText("Name");
        jLabelNewPruchaseName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewPruchaseDescription.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewPruchaseDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPruchaseDescription.setText("Description");
        jLabelNewPruchaseDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextAreaNewPruchaseDescription.setColumns(20);
        jTextAreaNewPruchaseDescription.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextAreaNewPruchaseDescription.setRows(5);
        jScrollPaneNewPruchase1.setViewportView(jTextAreaNewPruchaseDescription);

        jTextFieldNewPruchaseName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewPruchaseCreate.setBackground(new java.awt.Color(255, 204, 153));
        jButtonNewPruchaseCreate.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jButtonNewPruchaseCreate.setText("Create");
        jButtonNewPruchaseCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewPruchaseCreateActionPerformed(evt);
            }
        });

        jButtonNewPruchaseCancel.setBackground(new java.awt.Color(255, 204, 153));
        jButtonNewPruchaseCancel.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jButtonNewPruchaseCancel.setText("Cancel");
        jButtonNewPruchaseCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewPruchaseCancelActionPerformed(evt);
            }
        });

        jLabelNewPruchaseError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewPruchaseError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelNewPruchaseError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanelNewPurchaseLayout = new javax.swing.GroupLayout(jPanelNewPurchase);
        jPanelNewPurchase.setLayout(jPanelNewPurchaseLayout);
        jPanelNewPurchaseLayout.setHorizontalGroup(
                jPanelNewPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewPurchaseLayout.createSequentialGroup()
                                .addGroup(jPanelNewPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelNewPurchaseLayout.createSequentialGroup()
                                                .addGroup(jPanelNewPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelNewPurchaseLayout.createSequentialGroup()
                                                                .addGap(102, 102, 102)
                                                                .addGroup(jPanelNewPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelNewPurchaseLayout.createSequentialGroup()
                                                                                .addGap(32, 32, 32)
                                                                                .addComponent(jLabelNewPruchaseName)
                                                                                .addGap(55, 55, 55)
                                                                                .addComponent(jTextFieldNewPruchaseName, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addComponent(jLabelNewPurchase, javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelNewPurchaseLayout.createSequentialGroup()
                                                                                .addGap(35, 35, 35)
                                                                                .addComponent(jButtonNewPruchaseCancel)
                                                                                .addGap(104, 104, 104)
                                                                                .addComponent(jButtonNewPruchaseCreate))))
                                                        .addGroup(jPanelNewPurchaseLayout.createSequentialGroup()
                                                                .addGap(114, 114, 114)
                                                                .addComponent(jScrollPaneNewPruchase1, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanelNewPurchaseLayout.createSequentialGroup()
                                                                .addGap(219, 219, 219)
                                                                .addComponent(jLabelNewPruchaseDescription)))
                                                .addGap(0, 117, Short.MAX_VALUE))
                                        .addComponent(jLabelNewPruchaseError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        jPanelNewPurchaseLayout.setVerticalGroup(
                jPanelNewPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewPurchaseLayout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addComponent(jLabelNewPurchase)
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNewPruchaseError, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addGroup(jPanelNewPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldNewPruchaseName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNewPruchaseName))
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNewPruchaseDescription)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPaneNewPruchase1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(36, 36, 36)
                                .addGroup(jPanelNewPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewPruchaseCancel)
                                        .addComponent(jButtonNewPruchaseCreate))
                                .addContainerGap(188, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initnewDebtPanel() {
        jPanelNewDebt = new javax.swing.JPanel();
        jLabelNewDebt = new javax.swing.JLabel();
        jLabelNewDebtDate = new javax.swing.JLabel();
        jTextFieldNewDebtDate = new javax.swing.JTextField();
        jButtonNewDebtCancel = new javax.swing.JButton();
        jButtonNewDebtCreate = new javax.swing.JButton();
        jLabelNewDebtError = new javax.swing.JLabel();
        jLabelNewDebtTotalCount = new javax.swing.JLabel();
        jTextFieldNewDebtTotalCount = new javax.swing.JTextField();
        jLabelNewDebtName = new javax.swing.JLabel();
        jTextFieldNewDebtName = new javax.swing.JTextField();
        jLabelNewDebtDescription = new javax.swing.JLabel();
        jScrollPaneNewDebt = new javax.swing.JScrollPane();
        jTextAreaNewDebtDescription = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(620, 720));
        setMinimumSize(new java.awt.Dimension(620, 720));
        setPreferredSize(new java.awt.Dimension(620, 720));

        jPanelNewDebt.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewDebt.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewDebt.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewDebt.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewDebt.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDebt.setText("New debt");
        jLabelNewDebt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelNewDebt.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelNewDebt.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelNewDebt.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelNewDebtDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewDebtDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDebtDate.setText("date");

        jTextFieldNewDebtDate.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewDebtCancel.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewDebtCancel.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewDebtCancel.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewDebtCancel.setText("Cancel");
        jButtonNewDebtCancel.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewDebtCancel.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewDebtCancel.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewDebtCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewDebtCancelActionPerformed(evt);
            }
        });

        jButtonNewDebtCreate.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewDebtCreate.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewDebtCreate.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewDebtCreate.setText("Create");
        jButtonNewDebtCreate.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewDebtCreate.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewDebtCreate.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewDebtCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewDebtCreateActionPerformed(evt);
            }
        });

        jLabelNewDebtError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewDebtError.setForeground(new java.awt.Color(255, 0, 0));

        jLabelNewDebtTotalCount.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewDebtTotalCount.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDebtTotalCount.setText("Total count");

        jTextFieldNewDebtTotalCount.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jLabelNewDebtName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewDebtName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDebtName.setText("Name");
        jLabelNewDebtName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldNewDebtName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jLabelNewDebtDescription.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewDebtDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDebtDescription.setText("Description");
        jLabelNewDebtDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextAreaNewDebtDescription.setColumns(20);
        jTextAreaNewDebtDescription.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextAreaNewDebtDescription.setRows(5);
        jScrollPaneNewDebt.setViewportView(jTextAreaNewDebtDescription);

        javax.swing.GroupLayout jPanelNewDebtLayout = new javax.swing.GroupLayout(jPanelNewDebt);
        jPanelNewDebt.setLayout(jPanelNewDebtLayout);
        jPanelNewDebtLayout.setHorizontalGroup(
                jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDebtLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDebtLayout.createSequentialGroup()
                                                .addComponent(jLabelNewDebtDescription)
                                                .addGap(223, 223, 223))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDebtLayout.createSequentialGroup()
                                                .addComponent(jScrollPaneNewDebt, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(111, 111, 111))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDebtLayout.createSequentialGroup()
                                                .addGroup(jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelNewDebtDate)
                                                        .addComponent(jLabelNewDebtName)
                                                        .addComponent(jLabelNewDebtTotalCount))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jTextFieldNewDebtName, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                                                        .addComponent(jTextFieldNewDebtDate)
                                                        .addComponent(jTextFieldNewDebtTotalCount))
                                                .addGap(173, 173, 173))))
                        .addGroup(jPanelNewDebtLayout.createSequentialGroup()
                                .addGroup(jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelNewDebtLayout.createSequentialGroup()
                                                .addGap(183, 183, 183)
                                                .addComponent(jLabelNewDebt, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelNewDebtLayout.createSequentialGroup()
                                                .addGap(162, 162, 162)
                                                .addGroup(jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jLabelNewDebtError, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(jPanelNewDebtLayout.createSequentialGroup()
                                                                .addComponent(jButtonNewDebtCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                .addComponent(jButtonNewDebtCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelNewDebtLayout.setVerticalGroup(
                jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewDebtLayout.createSequentialGroup()
                                .addGap(94, 94, 94)
                                .addComponent(jLabelNewDebt, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addGroup(jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldNewDebtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNewDebtName))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewDebtDate)
                                        .addComponent(jTextFieldNewDebtDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldNewDebtTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNewDebtTotalCount))
                                .addGap(5, 5, 5)
                                .addComponent(jLabelNewDebtDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneNewDebt, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelNewDebtError, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37)
                                .addGroup(jPanelNewDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewDebtCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonNewDebtCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(59, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewDebt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewDebt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initPurchaseDetailPanel() {
        jPanelPurchase = new javax.swing.JPanel();
        jLabelPurchase = new javax.swing.JLabel();
        jButtonPurchaseBack = new javax.swing.JButton();
        jButtonPurchaseNew = new javax.swing.JButton();
        jLabelPruchaseName = new javax.swing.JLabel();
        jTextFieldPruchaseName = new javax.swing.JTextField();
        jLabelPruchaseDescription = new javax.swing.JLabel();
        jScrollPanelPurchaseDescription = new javax.swing.JScrollPane();
        jTextAreaPruchaseDescription = new javax.swing.JTextArea();
        jButtonPurchasePay = new javax.swing.JButton();
        jButtonPurchaseDelete = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabelPruchasePot = new javax.swing.JLabel();
        jTextFieldPruchaseName1 = new javax.swing.JTextField();
        jPanelPurchaseList = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabelPruchasePot1 = new javax.swing.JLabel();
        jTextFieldPruchaseTotal = new javax.swing.JTextField();

        String[] head = {"Market", "Name", "Price", "Amount", "Paid"};
        this.table = null;

        if (this.articleList.size() > 0) {
            table = new String[this.articleList.size()][head.length];
        }

        this.jPanelPurchaseList.setBackground(new java.awt.Color(153, 102, 0));
        this.jScrollPane5.setBackground(new java.awt.Color(153, 102, 0));
        this.jTable1.setBackground(new java.awt.Color(153, 102, 0));
        this.jTable1.setGridColor(new java.awt.Color(153, 102, 0));

        int i = 0;
        for (Article item : this.articleList) {
            table[i][0] = item.getMarket();
            table[i][1] = item.getName();
            table[i][2] = Double.toString(item.getPrice());
            table[i][3] = Double.toString(item.getAmount());
            table[i][4] = (item.getPaid() ? "paid" : "not paid");
            i++;
        }

        if (this.articleList.size() > 0) {
            javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(table, head) {
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            this.jTable1.setModel(model);
        }
        this.jTable1.setColumnSelectionAllowed(false);
        this.jTable1.setDragEnabled(false);
        this.jTable1.setRowSelectionAllowed(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelPurchase.setBackground(new java.awt.Color(255, 204, 102));
        jPanelPurchase.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelPurchase.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelPurchase.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelPurchase.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPurchase.setText("Purchase");
        jLabelPurchase.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelPurchase.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelPurchase.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelPurchase.setPreferredSize(new java.awt.Dimension(620, 720));

        jButtonPurchaseBack.setBackground(new java.awt.Color(153, 102, 0));
        jButtonPurchaseBack.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonPurchaseBack.setForeground(new java.awt.Color(0, 0, 0));
        jButtonPurchaseBack.setText("Back");
        jButtonPurchaseBack.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonPurchaseBack.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonPurchaseBack.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonPurchaseBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPurchaseBackActionPerformed(evt);
            }
        });

        jButtonPurchaseNew.setBackground(new java.awt.Color(153, 102, 0));
        jButtonPurchaseNew.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonPurchaseNew.setForeground(new java.awt.Color(0, 0, 0));
        jButtonPurchaseNew.setText("New article");
        jButtonPurchaseNew.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonPurchaseNew.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonPurchaseNew.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonPurchaseNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPurchaseNewActionPerformed(evt);
            }
        });

        jLabelPruchaseName.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelPruchaseName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPruchaseName.setText("Name");
        jLabelPruchaseName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldPruchaseName.setEditable(false);
        jTextFieldPruchaseName.setFont(new java.awt.Font("Showcard Gothic", 0, 14)); // NOI18N
        jTextFieldPruchaseName.setEnabled(false);

        jLabelPruchaseDescription.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelPruchaseDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPruchaseDescription.setText("Description");
        jLabelPruchaseDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextAreaPruchaseDescription.setEditable(false);
        jTextAreaPruchaseDescription.setColumns(20);
        jTextAreaPruchaseDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 14)); // NOI18N
        jTextAreaPruchaseDescription.setRows(5);
        jTextAreaPruchaseDescription.setEnabled(false);
        jScrollPanelPurchaseDescription.setViewportView(jTextAreaPruchaseDescription);

        jButtonPurchasePay.setBackground(new java.awt.Color(153, 102, 0));
        jButtonPurchasePay.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonPurchasePay.setForeground(new java.awt.Color(0, 0, 0));
        jButtonPurchasePay.setText("Pay");
        jButtonPurchasePay.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonPurchasePay.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonPurchasePay.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonPurchasePay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPurchasePayActionPerformed(evt);
            }
        });

        jButtonPurchaseDelete.setBackground(new java.awt.Color(153, 102, 0));
        jButtonPurchaseDelete.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonPurchaseDelete.setForeground(new java.awt.Color(0, 0, 0));
        jButtonPurchaseDelete.setText("Delete");
        jButtonPurchaseDelete.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonPurchaseDelete.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonPurchaseDelete.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonPurchaseDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPurchaseDeleteActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Showcard Gothic", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setToolTipText("");

        jLabelPruchasePot.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelPruchasePot.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPruchasePot.setText("Pot");
        jLabelPruchasePot.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldPruchaseName1.setEditable(false);
        jTextFieldPruchaseName1.setFont(new java.awt.Font("Showcard Gothic", 0, 14)); // NOI18N
        jTextFieldPruchaseName1.setEnabled(false);

        jScrollPane5.setViewportView(jTable1);

        javax.swing.GroupLayout jPanelPurchaseListLayout = new javax.swing.GroupLayout(jPanelPurchaseList);
        jPanelPurchaseList.setLayout(jPanelPurchaseListLayout);
        jPanelPurchaseListLayout.setHorizontalGroup(
                jPanelPurchaseListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
        );
        jPanelPurchaseListLayout.setVerticalGroup(
                jPanelPurchaseListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
        );

        jLabelPruchasePot1.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelPruchasePot1.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPruchasePot1.setText("Total");
        jLabelPruchasePot1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldPruchaseTotal.setEditable(false);
        jTextFieldPruchaseTotal.setFont(new java.awt.Font("Showcard Gothic", 0, 14)); // NOI18N
        jTextFieldPruchaseTotal.setEnabled(false);

        javax.swing.GroupLayout jPanelPurchaseLayout = new javax.swing.GroupLayout(jPanelPurchase);
        jPanelPurchase.setLayout(jPanelPurchaseLayout);
        jPanelPurchaseLayout.setHorizontalGroup(
                jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPurchaseLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabelPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(168, 168, 168))
                        .addGroup(jPanelPurchaseLayout.createSequentialGroup()
                                .addGap(74, 74, 74)
                                .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelPurchaseLayout.createSequentialGroup()
                                                .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jButtonPurchaseBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addComponent(jLabelPruchaseDescription)
                                                                .addGroup(jPanelPurchaseLayout.createSequentialGroup()
                                                                        .addComponent(jLabelPruchaseName)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                        .addComponent(jTextFieldPruchaseName, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addComponent(jScrollPanelPurchaseDescription)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelPurchaseLayout.createSequentialGroup()
                                                                .addComponent(jLabelPruchasePot1)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jTextFieldPruchaseTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jButtonPurchaseDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(jPanelPurchaseLayout.createSequentialGroup()
                                                                .addComponent(jLabelPruchasePot)
                                                                .addGap(28, 28, 28)
                                                                .addComponent(jTextFieldPruchaseName1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jButtonPurchaseNew, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jButtonPurchasePay, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addComponent(jPanelPurchaseList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(89, Short.MAX_VALUE))
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelPurchaseLayout.setVerticalGroup(
                jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelPurchaseLayout.createSequentialGroup()
                                .addGap(51, 51, 51)
                                .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelPurchaseLayout.createSequentialGroup()
                                                .addComponent(jLabelPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addComponent(jLabelPruchaseName)
                                                                .addComponent(jTextFieldPruchaseName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jTextFieldPruchaseName1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addComponent(jLabelPruchasePot))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabelPruchaseDescription)
                                                .addComponent(jLabelPruchasePot1))
                                        .addComponent(jTextFieldPruchaseTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanelPurchaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelPurchaseLayout.createSequentialGroup()
                                                .addGap(11, 11, 11)
                                                .addComponent(jScrollPanelPurchaseDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPurchaseLayout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonPurchaseNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonPurchasePay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonPurchaseDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(24, 24, 24)))
                                .addComponent(jPanelPurchaseList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonPurchaseBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(39, 39, 39))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initDebtDetailPanel() {
        jPanelDebt = new javax.swing.JPanel();
        jLabelDebt = new javax.swing.JLabel();
        jLabelDebtDate = new javax.swing.JLabel();
        jButtonDebtBack = new javax.swing.JButton();
        jButtonDebtNew = new javax.swing.JButton();
        jLabelDebtTotalCount = new javax.swing.JLabel();
        jLabelDebtName = new javax.swing.JLabel();
        jTextFieldDebtName = new javax.swing.JTextField();
        jScrollPaneDebt = new javax.swing.JScrollPane();
        jTextAreaDebtDescription = new javax.swing.JTextArea();
        jLabelJournalDebtDescription = new javax.swing.JLabel();
        jTextFieldDebtTotalCount = new javax.swing.JTextField();
        jTextFieldDebtDate = new javax.swing.JTextField();
        jButtonDebtModify = new javax.swing.JButton();
        jButtonDebtDelete = new javax.swing.JButton();
        jPanelDebtList = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTableDebt = new javax.swing.JTable();

        String[] head = {"Name", "User", "Amount", "paid"};
        this.table = null;

        if (this.chargeList.size() > 0) {
            table = new String[this.chargeList.size()][head.length];
        }

        this.jPanelDebtList.setBackground(new java.awt.Color(153, 102, 0));
        this.jScrollPane6.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableDebt.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableDebt.setGridColor(new java.awt.Color(153, 102, 0));

        int i = 0;
        for (Charge item : this.chargeList) {
            table[i][0] = item.getName();
            table[i][1] = item.getUser().getUser();
            table[i][2] = Double.toString(item.getAmount());
            table[i][3] = (item.isPaid() ? "paid" : "not paid");
            i++;
        }

        if (this.chargeList.size() > 0) {
            javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(table, head) {
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            this.jTableDebt.setModel(model);
        }
        this.jTableDebt.setColumnSelectionAllowed(false);
        this.jTableDebt.setDragEnabled(false);
        this.jTableDebt.setRowSelectionAllowed(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelDebt.setBackground(new java.awt.Color(255, 204, 102));
        jPanelDebt.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelDebt.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelDebt.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelDebt.setForeground(new java.awt.Color(153, 102, 0));
        jLabelDebt.setText("debt");
        jLabelDebt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelDebt.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelDebt.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelDebt.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelDebtDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelDebtDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelDebtDate.setText("date");

        jButtonDebtBack.setBackground(new java.awt.Color(153, 102, 0));
        jButtonDebtBack.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonDebtBack.setForeground(new java.awt.Color(0, 0, 0));
        jButtonDebtBack.setText("Back");
        jButtonDebtBack.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonDebtBack.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonDebtBack.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonDebtBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDebtBackActionPerformed(evt);
            }
        });

        jButtonDebtNew.setBackground(new java.awt.Color(153, 102, 0));
        jButtonDebtNew.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonDebtNew.setForeground(new java.awt.Color(0, 0, 0));
        jButtonDebtNew.setText("New charge");
        jButtonDebtNew.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonDebtNew.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonDebtNew.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonDebtNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDebtNewActionPerformed(evt);
            }
        });

        jLabelDebtTotalCount.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelDebtTotalCount.setForeground(new java.awt.Color(153, 102, 0));
        jLabelDebtTotalCount.setText("Total count");

        jLabelDebtName.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelDebtName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelDebtName.setText("Name");
        jLabelDebtName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldDebtName.setEditable(false);
        jTextFieldDebtName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldDebtName.setEnabled(false);

        jTextAreaDebtDescription.setEditable(false);
        jTextAreaDebtDescription.setColumns(20);
        jTextAreaDebtDescription.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextAreaDebtDescription.setRows(5);
        jTextAreaDebtDescription.setEnabled(false);
        jScrollPaneDebt.setViewportView(jTextAreaDebtDescription);

        jLabelJournalDebtDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelJournalDebtDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelJournalDebtDescription.setText("Description");
        jLabelJournalDebtDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldDebtTotalCount.setEditable(false);
        jTextFieldDebtTotalCount.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldDebtTotalCount.setEnabled(false);

        jTextFieldDebtDate.setEditable(false);
        jTextFieldDebtDate.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldDebtDate.setEnabled(false);

        jButtonDebtModify.setBackground(new java.awt.Color(153, 102, 0));
        jButtonDebtModify.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonDebtModify.setForeground(new java.awt.Color(0, 0, 0));
        jButtonDebtModify.setText("Modify");
        jButtonDebtModify.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonDebtModify.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonDebtModify.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonDebtModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDebtModifyActionPerformed(evt);
            }
        });

        jButtonDebtDelete.setBackground(new java.awt.Color(153, 102, 0));
        jButtonDebtDelete.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonDebtDelete.setForeground(new java.awt.Color(0, 0, 0));
        jButtonDebtDelete.setText("Delete");
        jButtonDebtDelete.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonDebtDelete.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonDebtDelete.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonDebtDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDebtDeleteActionPerformed(evt);
            }
        });

        jScrollPane6.setViewportView(jTableDebt);

        javax.swing.GroupLayout jPanelDebtListLayout = new javax.swing.GroupLayout(jPanelDebtList);
        jPanelDebtList.setLayout(jPanelDebtListLayout);
        jPanelDebtListLayout.setHorizontalGroup(
                jPanelDebtListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 513, Short.MAX_VALUE)
        );
        jPanelDebtListLayout.setVerticalGroup(
                jPanelDebtListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanelDebtLayout = new javax.swing.GroupLayout(jPanelDebt);
        jPanelDebt.setLayout(jPanelDebtLayout);
        jPanelDebtLayout.setHorizontalGroup(
                jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                .addGap(242, 242, 242)
                                                .addComponent(jLabelDebt, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                .addGap(28, 28, 28)
                                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                                .addComponent(jLabelDebtName)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jTextFieldDebtName))
                                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jLabelJournalDebtDescription)
                                                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                                                        .addComponent(jScrollPaneDebt, javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelDebtLayout.createSequentialGroup()
                                                                                                .addComponent(jLabelDebtTotalCount)
                                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                                .addComponent(jTextFieldDebtTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                                .addComponent(jLabelDebtDate)
                                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                                .addComponent(jTextFieldDebtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                                                                .addGap(44, 44, 44)
                                                                                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                                        .addComponent(jButtonDebtModify, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                        .addComponent(jButtonDebtNew, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                                        .addComponent(jButtonDebtDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                                                        .addComponent(jPanelDebtList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(0, 32, Short.MAX_VALUE)))))
                                .addGap(24, 24, 24))
                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                .addGap(253, 253, 253)
                                .addComponent(jButtonDebtBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelDebtLayout.setVerticalGroup(
                jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                .addGap(55, 55, 55)
                                .addComponent(jLabelDebt, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                                .addGap(13, 13, 13)
                                                                .addComponent(jLabelDebtName))
                                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jTextFieldDebtName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelDebtTotalCount)
                                                        .addComponent(jTextFieldDebtTotalCount, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabelDebtDate)
                                                        .addComponent(jTextFieldDebtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelJournalDebtDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelDebtLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanelDebtLayout.createSequentialGroup()
                                                .addComponent(jButtonDebtNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonDebtModify, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonDebtDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jScrollPaneDebt))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanelDebtList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonDebtBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelDebt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelDebt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initNewArticlePanel() {
        jPanelNewArticle = new javax.swing.JPanel();
        jLabelNewArticle = new javax.swing.JLabel();
        jLabelNewArticleMarket = new javax.swing.JLabel();
        jButtonNewArticleCancel = new javax.swing.JButton();
        jLabelNewArticlePrice = new javax.swing.JLabel();
        jTextFieldNewArticleMarket = new javax.swing.JTextField();
        jTextFieldNewArticlePrice = new javax.swing.JTextField();
        jButtonNewArticleCreate = new javax.swing.JButton();
        jLabelNewArticleError = new javax.swing.JLabel();
        jLabelNewArticleName = new javax.swing.JLabel();
        jTextFieldNewArticleName = new javax.swing.JTextField();
        jLabelNewArticleAmount = new javax.swing.JLabel();
        jTextFieldNewArticleAmount = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewArticle.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewArticle.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewArticle.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewArticle.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewArticle.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewArticle.setText("New article");
        jLabelNewArticle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelNewArticle.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelNewArticle.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelNewArticle.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelNewArticleMarket.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewArticleMarket.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewArticleMarket.setText("Market");

        jButtonNewArticleCancel.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewArticleCancel.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewArticleCancel.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewArticleCancel.setText("Cancel");
        jButtonNewArticleCancel.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewArticleCancel.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewArticleCancel.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewArticleCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewArticleCancelActionPerformed(evt);
            }
        });

        jLabelNewArticlePrice.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewArticlePrice.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewArticlePrice.setText("Price");

        jTextFieldNewArticleMarket.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewArticlePrice.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewArticleCreate.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewArticleCreate.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewArticleCreate.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewArticleCreate.setText("Create");
        jButtonNewArticleCreate.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewArticleCreate.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewArticleCreate.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewArticleCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewArticleCreateActionPerformed(evt);
            }
        });

        jLabelNewArticleError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewArticleError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelNewArticleError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabelNewArticleName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewArticleName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewArticleName.setText("Name");

        jTextFieldNewArticleName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jLabelNewArticleAmount.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewArticleAmount.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewArticleAmount.setText("Amount");

        jTextFieldNewArticleAmount.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanelNewArticleLayout = new javax.swing.GroupLayout(jPanelNewArticle);
        jPanelNewArticle.setLayout(jPanelNewArticleLayout);
        jPanelNewArticleLayout.setHorizontalGroup(
                jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewArticleLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewArticleLayout.createSequentialGroup()
                                                .addGap(0, 144, Short.MAX_VALUE)
                                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewArticleLayout.createSequentialGroup()
                                                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(jLabelNewArticle, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewArticleLayout.createSequentialGroup()
                                                                                .addComponent(jButtonNewArticleCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                .addComponent(jButtonNewArticleCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addGap(124, 124, 124))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewArticleLayout.createSequentialGroup()
                                                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(jLabelNewArticleName)
                                                                        .addComponent(jLabelNewArticleMarket)
                                                                        .addComponent(jLabelNewArticlePrice)
                                                                        .addComponent(jLabelNewArticleAmount))
                                                                .addGap(38, 38, 38)
                                                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jTextFieldNewArticleMarket, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextFieldNewArticlePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextFieldNewArticleName, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextFieldNewArticleAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(156, 156, 156))))
                                        .addComponent(jLabelNewArticleError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanelNewArticleLayout.setVerticalGroup(
                jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewArticleLayout.createSequentialGroup()
                                .addGap(84, 84, 84)
                                .addComponent(jLabelNewArticle, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(55, 55, 55)
                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelNewArticleName)
                                        .addComponent(jTextFieldNewArticleName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewArticleMarket)
                                        .addComponent(jTextFieldNewArticleMarket, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewArticlePrice)
                                        .addComponent(jTextFieldNewArticlePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelNewArticleAmount)
                                        .addComponent(jTextFieldNewArticleAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 217, Short.MAX_VALUE)
                                .addComponent(jLabelNewArticleError, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addGroup(jPanelNewArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewArticleCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonNewArticleCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(71, 71, 71))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 233, Short.MAX_VALUE)
                                        .addComponent(jPanelNewArticle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 233, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewArticle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initNewChargePanel() {
        jPanelNewCharge = new javax.swing.JPanel();
        jLabelNewCharge = new javax.swing.JLabel();
        jLabelNewChargeAmount = new javax.swing.JLabel();
        jButtonNewChargeCancel = new javax.swing.JButton();
        jLabelNewChargeUser = new javax.swing.JLabel();
        jTextFieldNewChargeAmount = new javax.swing.JTextField();
        jButtonNewChargeCreate = new javax.swing.JButton();
        jLabelNewChargeError = new javax.swing.JLabel();
        jLabelNewChargeName = new javax.swing.JLabel();
        jTextFieldNewChargeName = new javax.swing.JTextField();
        jComboBoxNewChargeUsers = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewCharge.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewCharge.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewCharge.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewCharge.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewCharge.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewCharge.setText("New charge");
        jLabelNewCharge.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelNewCharge.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelNewCharge.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelNewCharge.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelNewChargeAmount.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewChargeAmount.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewChargeAmount.setText("Amount");

        jButtonNewChargeCancel.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewChargeCancel.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewChargeCancel.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewChargeCancel.setText("Cancel");
        jButtonNewChargeCancel.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewChargeCancel.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewChargeCancel.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewChargeCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewChargeCancelActionPerformed(evt);
            }
        });

        jLabelNewChargeUser.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewChargeUser.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewChargeUser.setText("User");

        jTextFieldNewChargeAmount.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewChargeCreate.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewChargeCreate.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewChargeCreate.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewChargeCreate.setText("Create");
        jButtonNewChargeCreate.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewChargeCreate.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewChargeCreate.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewChargeCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewChargeCreateActionPerformed(evt);
            }
        });

        jLabelNewChargeError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewChargeError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelNewChargeError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabelNewChargeName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewChargeName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewChargeName.setText("Name");

        jTextFieldNewChargeName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jComboBoxNewChargeUsers.setBackground(new java.awt.Color(153, 102, 0));
        jComboBoxNewChargeUsers.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jComboBoxNewChargeUsers.setForeground(new java.awt.Color(0, 0, 0));
        jComboBoxNewChargeUsers.setMaximumRowCount(4);
        jComboBoxNewChargeUsers.setMaximumSize(new java.awt.Dimension(114, 29));
        jComboBoxNewChargeUsers.setMinimumSize(new java.awt.Dimension(114, 29));
        jComboBoxNewChargeUsers.setPreferredSize(new java.awt.Dimension(114, 29));

        javax.swing.GroupLayout jPanelNewChargeLayout = new javax.swing.GroupLayout(jPanelNewCharge);
        jPanelNewCharge.setLayout(jPanelNewChargeLayout);
        jPanelNewChargeLayout.setHorizontalGroup(
                jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewChargeLayout.createSequentialGroup()
                                .addGroup(jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewChargeLayout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jButtonNewChargeCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewChargeLayout.createSequentialGroup()
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelNewChargeAmount)
                                                        .addComponent(jLabelNewChargeUser)
                                                        .addComponent(jButtonNewChargeCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabelNewChargeName))
                                                .addGap(38, 38, 38)
                                                .addGroup(jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jTextFieldNewChargeAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                                                        .addComponent(jTextFieldNewChargeName, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                                                        .addComponent(jComboBoxNewChargeUsers, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                .addGap(157, 157, 157))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewChargeLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewChargeLayout.createSequentialGroup()
                                                .addComponent(jLabelNewCharge, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(136, 136, 136))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewChargeLayout.createSequentialGroup()
                                                .addComponent(jLabelNewChargeError, javax.swing.GroupLayout.PREFERRED_SIZE, 608, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addContainerGap())))
        );
        jPanelNewChargeLayout.setVerticalGroup(
                jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewChargeLayout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(jLabelNewCharge, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelNewChargeLayout.createSequentialGroup()
                                                .addGap(23, 23, 23)
                                                .addComponent(jLabelNewChargeName))
                                        .addGroup(jPanelNewChargeLayout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldNewChargeName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewChargeAmount)
                                        .addComponent(jTextFieldNewChargeAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewChargeUser)
                                        .addComponent(jComboBoxNewChargeUsers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 295, Short.MAX_VALUE)
                                .addComponent(jLabelNewChargeError, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewChargeCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonNewChargeCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(71, 71, 71))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewCharge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewCharge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initChargePanel() {
        jPanelCharge = new javax.swing.JPanel();
        jLabelCharge = new javax.swing.JLabel();
        jLabelChargeAmount = new javax.swing.JLabel();
        jButtonChargeBack = new javax.swing.JButton();
        jLabelChargeName = new javax.swing.JLabel();
        jTextFieldChargeName = new javax.swing.JTextField();
        jLabelNewChargeUser1 = new javax.swing.JLabel();
        jTextFieldChargeAmount = new javax.swing.JTextField();
        jTextFieldChargeUser = new javax.swing.JTextField();
        jCheckBoxPaid = new javax.swing.JCheckBox();
        jButtonChargeAcept = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(620, 720));
        setMinimumSize(new java.awt.Dimension(620, 720));
        setPreferredSize(new java.awt.Dimension(620, 720));

        jPanelCharge.setBackground(new java.awt.Color(255, 204, 102));
        jPanelCharge.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelCharge.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelCharge.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelCharge.setForeground(new java.awt.Color(153, 102, 0));
        jLabelCharge.setText("charge");
        jLabelCharge.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelCharge.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelCharge.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelCharge.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelChargeAmount.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelChargeAmount.setForeground(new java.awt.Color(153, 102, 0));
        jLabelChargeAmount.setText("Amount");

        jButtonChargeBack.setBackground(new java.awt.Color(153, 102, 0));
        jButtonChargeBack.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonChargeBack.setForeground(new java.awt.Color(0, 0, 0));
        jButtonChargeBack.setText("Back");
        jButtonChargeBack.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonChargeBack.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonChargeBack.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonChargeBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonChargeBackActionPerformed(evt);
            }
        });

        jLabelChargeName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelChargeName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelChargeName.setText("Name");

        jTextFieldChargeName.setEditable(false);
        jTextFieldChargeName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldChargeName.setEnabled(false);

        jLabelNewChargeUser1.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewChargeUser1.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewChargeUser1.setText("User");

        jTextFieldChargeAmount.setEditable(false);
        jTextFieldChargeAmount.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldChargeAmount.setEnabled(false);

        jTextFieldChargeUser.setEditable(false);
        jTextFieldChargeUser.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldChargeUser.setEnabled(false);

        jCheckBoxPaid.setBackground(new java.awt.Color(255, 204, 102));
        jCheckBoxPaid.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jCheckBoxPaid.setForeground(new java.awt.Color(153, 102, 0));
        jCheckBoxPaid.setText("Paid");

        jButtonChargeAcept.setBackground(new java.awt.Color(153, 102, 0));
        jButtonChargeAcept.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonChargeAcept.setForeground(new java.awt.Color(0, 0, 0));
        jButtonChargeAcept.setText("Acept");
        jButtonChargeAcept.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonChargeAcept.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonChargeAcept.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonChargeAcept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonChargeAceptActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelChargeLayout = new javax.swing.GroupLayout(jPanelCharge);
        jPanelCharge.setLayout(jPanelChargeLayout);
        jPanelChargeLayout.setHorizontalGroup(
                jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelChargeLayout.createSequentialGroup()
                                .addGroup(jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelChargeLayout.createSequentialGroup()
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelNewChargeUser1)
                                                        .addComponent(jLabelChargeAmount))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jTextFieldChargeAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jTextFieldChargeUser, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jCheckBoxPaid)))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelChargeLayout.createSequentialGroup()
                                                .addGap(205, 205, 205)
                                                .addGroup(jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelChargeLayout.createSequentialGroup()
                                                                .addGap(14, 14, 14)
                                                                .addComponent(jLabelChargeName)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jTextFieldChargeName, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE))
                                                        .addComponent(jLabelCharge, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(169, 169, 169))
                        .addGroup(jPanelChargeLayout.createSequentialGroup()
                                .addGap(148, 148, 148)
                                .addComponent(jButtonChargeBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(108, 108, 108)
                                .addComponent(jButtonChargeAcept, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelChargeLayout.setVerticalGroup(
                jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelChargeLayout.createSequentialGroup()
                                .addGap(85, 85, 85)
                                .addComponent(jLabelCharge, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(42, 42, 42)
                                .addGroup(jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelChargeName)
                                        .addComponent(jTextFieldChargeName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelChargeAmount)
                                        .addComponent(jTextFieldChargeAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewChargeUser1)
                                        .addComponent(jTextFieldChargeUser, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(27, 27, 27)
                                .addComponent(jCheckBoxPaid)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 272, Short.MAX_VALUE)
                                .addGroup(jPanelChargeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonChargeBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonChargeAcept, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(60, 60, 60))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelCharge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelCharge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initNewTaskPanel() {

        jPanelNewTask = new javax.swing.JPanel();
        jLabelNewTask = new javax.swing.JLabel();
        jLabelNewTaskEndDate = new javax.swing.JLabel();
        jTextFieldNewTaskEndDate = new javax.swing.JTextField();
        jButtonNewTaskCancel = new javax.swing.JButton();
        jButtonNewTaskCreate = new javax.swing.JButton();
        jLabelNewTaskError = new javax.swing.JLabel();
        jLabelNewTaskName = new javax.swing.JLabel();
        jTextFieldNewTaskName = new javax.swing.JTextField();
        jLabelNewTaskDescription = new javax.swing.JLabel();
        jTextFieldNewTaskDescription = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewTask.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewTask.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewTask.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewTask.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewTask.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTask.setText("New Task");
        jLabelNewTask.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelNewTask.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelNewTask.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelNewTask.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelNewTaskEndDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewTaskEndDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTaskEndDate.setText("End date");

        jTextFieldNewTaskEndDate.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewTaskCancel.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewTaskCancel.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewTaskCancel.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewTaskCancel.setText("Cancel");
        jButtonNewTaskCancel.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewTaskCancel.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewTaskCancel.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewTaskCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewTaskCancelActionPerformed(evt);
            }
        });

        jButtonNewTaskCreate.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewTaskCreate.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewTaskCreate.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewTaskCreate.setText("Create");
        jButtonNewTaskCreate.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewTaskCreate.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewTaskCreate.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewTaskCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewTaskCreateActionPerformed(evt);
            }
        });

        jLabelNewTaskError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewTaskError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelNewTaskError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelNewTaskError.setToolTipText("");

        jLabelNewTaskName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewTaskName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTaskName.setText("Name");

        jTextFieldNewTaskName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jLabelNewTaskDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewTaskDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewTaskDescription.setText("Description");

        jTextFieldNewTaskDescription.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanelNewTaskLayout = new javax.swing.GroupLayout(jPanelNewTask);
        jPanelNewTask.setLayout(jPanelNewTaskLayout);
        jPanelNewTaskLayout.setHorizontalGroup(
                jPanelNewTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewTaskLayout.createSequentialGroup()
                                .addGap(171, 171, 171)
                                .addGroup(jPanelNewTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jTextFieldNewTaskDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanelNewTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(jPanelNewTaskLayout.createSequentialGroup()
                                                        .addComponent(jButtonNewTaskCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jButtonNewTaskCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jTextFieldNewTaskName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGroup(jPanelNewTaskLayout.createSequentialGroup()
                                                        .addGroup(jPanelNewTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addComponent(jLabelNewTaskName)
                                                                .addComponent(jLabelNewTaskEndDate))
                                                        .addGap(51, 51, 51)
                                                        .addComponent(jTextFieldNewTaskEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addContainerGap(132, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewTaskLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelNewTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewTaskLayout.createSequentialGroup()
                                                .addComponent(jLabelNewTask, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(177, 177, 177))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewTaskLayout.createSequentialGroup()
                                                .addComponent(jLabelNewTaskDescription)
                                                .addGap(232, 232, 232))))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewTaskLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabelNewTaskError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );
        jPanelNewTaskLayout.setVerticalGroup(
                jPanelNewTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewTaskLayout.createSequentialGroup()
                                .addGap(84, 84, 84)
                                .addComponent(jLabelNewTask, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(50, 50, 50)
                                .addGroup(jPanelNewTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewTaskName)
                                        .addComponent(jTextFieldNewTaskName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewTaskEndDate)
                                        .addComponent(jTextFieldNewTaskEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelNewTaskDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldNewTaskDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(33, 33, 33)
                                .addComponent(jLabelNewTaskError, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
                                .addGroup(jPanelNewTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewTaskCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonNewTaskCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(92, 92, 92))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 233, Short.MAX_VALUE)
                                        .addComponent(jPanelNewTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 233, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initTaskDetailPanel() {

        jPanelTask = new javax.swing.JPanel();
        jLabelTask = new javax.swing.JLabel();
        jLabelTaskEndDate = new javax.swing.JLabel();
        jButtonTaskBack = new javax.swing.JButton();
        jButtonTaskNew = new javax.swing.JButton();
        jLabelTaskName = new javax.swing.JLabel();
        jTextFieldTaskName = new javax.swing.JTextField();
        jTextFieldNewTaskName2 = new javax.swing.JTextField();
        jLabelTaskDescription = new javax.swing.JLabel();
        jTextFieldTaskDescription = new javax.swing.JTextField();
        jButtonTaskModify = new javax.swing.JButton();
        jButtonTaskDelete = new javax.swing.JButton();
        jPanelTaskList = new javax.swing.JPanel();
        jScrollPanelTask = new javax.swing.JScrollPane();
        jTableDuty = new javax.swing.JTable();

        String[] head = {"Name", "End date", "User", "Finished"};
        this.table = null;

        if (this.dutyList.size() > 0) {
            table = new String[this.dutyList.size()][head.length];
        }

        this.jPanelTaskList.setBackground(new java.awt.Color(153, 102, 0));
        this.jScrollPanelTask.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableDuty.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableDuty.setGridColor(new java.awt.Color(153, 102, 0));

        int i = 0;
        for (Duty item : this.dutyList) {
            table[i][0] = item.getName();
            table[i][1] = Utils.dateToString(item.getEndDate().getTime());
            table[i][2] = item.getUser().getUser();
            table[i][3] = (item.isFinished() ? "finished" : "not finished");
            i++;
        }

        if (this.dutyList.size() > 0) {
            javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(table, head) {
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            this.jTableDuty.setModel(model);
        }
        this.jTableDuty.setColumnSelectionAllowed(false);
        this.jTableDuty.setDragEnabled(false);
        this.jTableDuty.setRowSelectionAllowed(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelTask.setBackground(new java.awt.Color(255, 204, 102));
        jPanelTask.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelTask.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelTask.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelTask.setForeground(new java.awt.Color(153, 102, 0));
        jLabelTask.setText("Task");
        jLabelTask.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelTask.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelTask.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelTask.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelTaskEndDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelTaskEndDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelTaskEndDate.setText("End date");

        jButtonTaskBack.setBackground(new java.awt.Color(153, 102, 0));
        jButtonTaskBack.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonTaskBack.setForeground(new java.awt.Color(0, 0, 0));
        jButtonTaskBack.setText("Back");
        jButtonTaskBack.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonTaskBack.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonTaskBack.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonTaskBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTaskBackActionPerformed(evt);
            }
        });

        jButtonTaskNew.setBackground(new java.awt.Color(153, 102, 0));
        jButtonTaskNew.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonTaskNew.setForeground(new java.awt.Color(0, 0, 0));
        jButtonTaskNew.setText("New duty");
        jButtonTaskNew.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonTaskNew.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonTaskNew.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonTaskNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTaskNewActionPerformed(evt);
            }
        });

        jLabelTaskName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelTaskName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelTaskName.setText("Name");

        jTextFieldTaskName.setEditable(false);
        jTextFieldTaskName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldTaskName.setEnabled(false);

        jTextFieldNewTaskName2.setEditable(false);
        jTextFieldNewTaskName2.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldNewTaskName2.setEnabled(false);

        jLabelTaskDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelTaskDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelTaskDescription.setText("Description");

        jTextFieldTaskDescription.setEditable(false);
        jTextFieldTaskDescription.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldTaskDescription.setEnabled(false);

        jButtonTaskModify.setBackground(new java.awt.Color(153, 102, 0));
        jButtonTaskModify.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonTaskModify.setForeground(new java.awt.Color(0, 0, 0));
        jButtonTaskModify.setText("Modify");
        jButtonTaskModify.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonTaskModify.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonTaskModify.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonTaskModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTaskModifyActionPerformed(evt);
            }
        });

        jButtonTaskDelete.setBackground(new java.awt.Color(153, 102, 0));
        jButtonTaskDelete.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonTaskDelete.setForeground(new java.awt.Color(0, 0, 0));
        jButtonTaskDelete.setText("Delete");
        jButtonTaskDelete.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonTaskDelete.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonTaskDelete.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonTaskDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTaskDeleteActionPerformed(evt);
            }
        });

        jScrollPanelTask.setViewportView(jTableDuty);

        javax.swing.GroupLayout jPanelTaskListLayout = new javax.swing.GroupLayout(jPanelTaskList);
        jPanelTaskList.setLayout(jPanelTaskListLayout);
        jPanelTaskListLayout.setHorizontalGroup(
                jPanelTaskListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPanelTask, javax.swing.GroupLayout.DEFAULT_SIZE, 546, Short.MAX_VALUE)
        );
        jPanelTaskListLayout.setVerticalGroup(
                jPanelTaskListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPanelTask, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout jPanelTaskLayout = new javax.swing.GroupLayout(jPanelTask);
        jPanelTask.setLayout(jPanelTaskLayout);
        jPanelTaskLayout.setHorizontalGroup(
                jPanelTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTaskLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabelTask, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(231, 231, 231))
                        .addGroup(jPanelTaskLayout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addGroup(jPanelTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelTaskLayout.createSequentialGroup()
                                                .addGroup(jPanelTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelTaskLayout.createSequentialGroup()
                                                                .addGap(217, 217, 217)
                                                                .addComponent(jButtonTaskBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanelTaskLayout.createSequentialGroup()
                                                                .addComponent(jTextFieldTaskDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 391, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(jPanelTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jButtonTaskNew, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jButtonTaskModify, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jButtonTaskDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addComponent(jLabelTaskDescription)
                                                        .addComponent(jPanelTaskList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(jPanelTaskLayout.createSequentialGroup()
                                                .addComponent(jLabelTaskName)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jTextFieldTaskName, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabelTaskEndDate)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jTextFieldNewTaskName2, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(16, 16, 16))))
        );
        jPanelTaskLayout.setVerticalGroup(
                jPanelTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelTaskLayout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabelTask, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelTaskLayout.createSequentialGroup()
                                                .addGap(3, 3, 3)
                                                .addGroup(jPanelTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabelTaskEndDate)
                                                        .addComponent(jTextFieldNewTaskName2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(jPanelTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jLabelTaskName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldTaskName)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelTaskDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelTaskLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelTaskLayout.createSequentialGroup()
                                                .addComponent(jTextFieldTaskDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jPanelTaskList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jButtonTaskBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelTaskLayout.createSequentialGroup()
                                                .addComponent(jButtonTaskNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonTaskModify, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonTaskDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(139, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initNewDutyPanel() {

        jPanelNewDuty = new javax.swing.JPanel();
        jLabelNewDuty = new javax.swing.JLabel();
        jLabelNewDutyEndDate = new javax.swing.JLabel();
        jButtonNewDutyCancel = new javax.swing.JButton();
        jLabelNewDutyDescription = new javax.swing.JLabel();
        jLabelNewDutyUser = new javax.swing.JLabel();
        jScrollPaneNewDutyDescription = new javax.swing.JScrollPane();
        jTextAreaNewDuty = new javax.swing.JTextArea();
        jTextFieldNewDutyEndDate = new javax.swing.JTextField();
        jButtonNewDutyCreate = new javax.swing.JButton();
        jLabelNewDutyError = new javax.swing.JLabel();
        jLabelNewDutyName = new javax.swing.JLabel();
        jTextFieldNewDutyName = new javax.swing.JTextField();
        jComboBoxNewDutyUsers = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewDuty.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewDuty.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewDuty.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewDuty.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewDuty.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDuty.setText("New duty");
        jLabelNewDuty.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelNewDuty.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelNewDuty.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelNewDuty.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelNewDutyEndDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewDutyEndDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDutyEndDate.setText("End date");

        jButtonNewDutyCancel.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewDutyCancel.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewDutyCancel.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewDutyCancel.setText("Cancel");
        jButtonNewDutyCancel.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewDutyCancel.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewDutyCancel.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewDutyCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewDutyCancelActionPerformed(evt);
            }
        });

        jLabelNewDutyDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewDutyDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDutyDescription.setText("Description");

        jLabelNewDutyUser.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewDutyUser.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDutyUser.setText("User");

        jTextAreaNewDuty.setColumns(20);
        jTextAreaNewDuty.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextAreaNewDuty.setRows(5);
        jScrollPaneNewDutyDescription.setViewportView(jTextAreaNewDuty);

        jTextFieldNewDutyEndDate.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewDutyCreate.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewDutyCreate.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewDutyCreate.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewDutyCreate.setText("Create");
        jButtonNewDutyCreate.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewDutyCreate.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewDutyCreate.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewDutyCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewDutyCreateActionPerformed(evt);
            }
        });

        jLabelNewDutyError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewDutyError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelNewDutyError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabelNewDutyName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewDutyName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewDutyName.setText("Name");

        jTextFieldNewDutyName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jComboBoxNewDutyUsers.setBackground(new java.awt.Color(153, 102, 0));
        jComboBoxNewDutyUsers.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jComboBoxNewDutyUsers.setForeground(new java.awt.Color(0, 0, 0));
        jComboBoxNewDutyUsers.setMaximumRowCount(4);
        jComboBoxNewDutyUsers.setMaximumSize(new java.awt.Dimension(114, 29));
        jComboBoxNewDutyUsers.setMinimumSize(new java.awt.Dimension(114, 29));
        jComboBoxNewDutyUsers.setPreferredSize(new java.awt.Dimension(114, 29));

        javax.swing.GroupLayout jPanelNewDutyLayout = new javax.swing.GroupLayout(jPanelNewDuty);
        jPanelNewDuty.setLayout(jPanelNewDutyLayout);
        jPanelNewDutyLayout.setHorizontalGroup(
                jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDutyLayout.createSequentialGroup()
                                .addGroup(jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDutyLayout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jButtonNewDutyCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDutyLayout.createSequentialGroup()
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDutyLayout.createSequentialGroup()
                                                                .addGroup(jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(jLabelNewDutyEndDate)
                                                                        .addComponent(jLabelNewDutyUser)
                                                                        .addComponent(jButtonNewDutyCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jLabelNewDutyName))
                                                                .addGap(38, 38, 38)
                                                                .addGroup(jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(jTextFieldNewDutyEndDate, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                                                                        .addComponent(jTextFieldNewDutyName, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                                                                        .addComponent(jComboBoxNewDutyUsers, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                                        .addComponent(jLabelNewDuty, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(157, 157, 157))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDutyLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDutyLayout.createSequentialGroup()
                                                .addComponent(jLabelNewDutyDescription)
                                                .addGap(232, 232, 232))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDutyLayout.createSequentialGroup()
                                                .addComponent(jScrollPaneNewDutyDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(189, 189, 189))))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewDutyLayout.createSequentialGroup()
                                .addGap(0, 6, Short.MAX_VALUE)
                                .addComponent(jLabelNewDutyError, javax.swing.GroupLayout.PREFERRED_SIZE, 608, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );
        jPanelNewDutyLayout.setVerticalGroup(
                jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewDutyLayout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(jLabelNewDuty, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelNewDutyLayout.createSequentialGroup()
                                                .addGap(23, 23, 23)
                                                .addComponent(jLabelNewDutyName))
                                        .addGroup(jPanelNewDutyLayout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addComponent(jTextFieldNewDutyName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewDutyEndDate)
                                        .addComponent(jTextFieldNewDutyEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewDutyUser)
                                        .addComponent(jComboBoxNewDutyUsers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNewDutyDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneNewDutyDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 109, Short.MAX_VALUE)
                                .addComponent(jLabelNewDutyError, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewDutyCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonNewDutyCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(71, 71, 71))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewDuty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewDuty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initDutyPanel() {

        jPanelDuty = new javax.swing.JPanel();
        jLabelDuty = new javax.swing.JLabel();
        jLabelDutyEndDate = new javax.swing.JLabel();
        jLabelDutyDescription = new javax.swing.JLabel();
        jButtonDutyBack = new javax.swing.JButton();
        jLabelDutyUser = new javax.swing.JLabel();
        jTextFieldDutyEndDate = new javax.swing.JTextField();
        jTextFieldDutyUser = new javax.swing.JTextField();
        jTextFieldDutyDescription = new javax.swing.JTextField();
        jCheckBoxDutyFinished = new javax.swing.JCheckBox();
        jButtonDutyAcept = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(620, 720));
        setMinimumSize(new java.awt.Dimension(620, 720));
        setPreferredSize(new java.awt.Dimension(620, 720));

        jPanelDuty.setBackground(new java.awt.Color(255, 204, 102));
        jPanelDuty.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelDuty.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelDuty.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelDuty.setForeground(new java.awt.Color(153, 102, 0));
        jLabelDuty.setText("duty");
        jLabelDuty.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelDuty.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelDuty.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelDuty.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelDutyEndDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelDutyEndDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelDutyEndDate.setText("End date");

        jLabelDutyDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelDutyDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelDutyDescription.setText("Description");

        jButtonDutyBack.setBackground(new java.awt.Color(153, 102, 0));
        jButtonDutyBack.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonDutyBack.setForeground(new java.awt.Color(0, 0, 0));
        jButtonDutyBack.setText("Back");
        jButtonDutyBack.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonDutyBack.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonDutyBack.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonDutyBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDutyBackActionPerformed(evt);
            }
        });

        jLabelDutyUser.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelDutyUser.setForeground(new java.awt.Color(153, 102, 0));
        jLabelDutyUser.setText("User");

        jTextFieldDutyEndDate.setEditable(false);
        jTextFieldDutyEndDate.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldDutyEndDate.setEnabled(false);

        jTextFieldDutyUser.setEditable(false);
        jTextFieldDutyUser.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldDutyUser.setEnabled(false);

        jTextFieldDutyDescription.setEditable(false);
        jTextFieldDutyDescription.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldDutyDescription.setEnabled(false);

        jCheckBoxDutyFinished.setBackground(new java.awt.Color(255, 204, 102));
        jCheckBoxDutyFinished.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jCheckBoxDutyFinished.setForeground(new java.awt.Color(153, 102, 0));
        jCheckBoxDutyFinished.setText("Finished");

        jButtonDutyAcept.setBackground(new java.awt.Color(153, 102, 0));
        jButtonDutyAcept.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonDutyAcept.setForeground(new java.awt.Color(0, 0, 0));
        jButtonDutyAcept.setText("Acept");
        jButtonDutyAcept.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonDutyAcept.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonDutyAcept.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonDutyAcept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDutyAceptActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelDutyLayout = new javax.swing.GroupLayout(jPanelDuty);
        jPanelDuty.setLayout(jPanelDutyLayout);
        jPanelDutyLayout.setHorizontalGroup(
                jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelDutyLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabelDuty, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(224, 224, 224))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelDutyLayout.createSequentialGroup()
                                .addContainerGap(177, Short.MAX_VALUE)
                                .addGroup(jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelDutyLayout.createSequentialGroup()
                                                .addGroup(jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelDutyUser)
                                                        .addComponent(jLabelDutyEndDate))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jTextFieldDutyEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jTextFieldDutyUser, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(jPanelDutyLayout.createSequentialGroup()
                                                .addGroup(jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jCheckBoxDutyFinished)
                                                        .addComponent(jLabelDutyDescription))
                                                .addGap(62, 62, 62))
                                        .addGroup(jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(jPanelDutyLayout.createSequentialGroup()
                                                        .addComponent(jButtonDutyBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jButtonDutyAcept, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jTextFieldDutyDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(162, 162, 162))
        );
        jPanelDutyLayout.setVerticalGroup(
                jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelDutyLayout.createSequentialGroup()
                                .addGap(84, 84, 84)
                                .addComponent(jLabelDuty, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(39, 39, 39)
                                .addGroup(jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldDutyEndDate)
                                        .addComponent(jLabelDutyEndDate))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabelDutyUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jTextFieldDutyUser))
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxDutyFinished)
                                .addGap(78, 78, 78)
                                .addComponent(jLabelDutyDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldDutyDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(89, 89, 89)
                                .addGroup(jPanelDutyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonDutyBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonDutyAcept, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(71, 71, 71))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelDuty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelDuty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initNewJournalPlacePanel() {

        jPanelNewJournalPlace = new javax.swing.JPanel();
        jLabelNewJournalPlace = new javax.swing.JLabel();
        jLabelNewJournalPlaceEndDate = new javax.swing.JLabel();
        jTextFieldNewJournalPlaceEndDate = new javax.swing.JTextField();
        jButtonNewJournalPlaceCancel = new javax.swing.JButton();
        jButtonNewJournalPlaceCreate = new javax.swing.JButton();
        jLabelNewJournalPlaceError = new javax.swing.JLabel();
        jLabelNewJournalPlaceName = new javax.swing.JLabel();
        jLabelNewJournalPlaceDescription = new javax.swing.JLabel();
        jScrollPaneNewJournalPlace = new javax.swing.JScrollPane();
        jTextAreaNewJournalPlace = new javax.swing.JTextArea();
        jTextFieldNewJournalPlaceName = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewJournalPlace.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewJournalPlace.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewJournalPlace.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewJournalPlace.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewJournalPlace.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewJournalPlace.setText("New Journal Place");
        jLabelNewJournalPlace.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelNewJournalPlace.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelNewJournalPlace.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelNewJournalPlace.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelNewJournalPlaceEndDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewJournalPlaceEndDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewJournalPlaceEndDate.setText("End date");

        jTextFieldNewJournalPlaceEndDate.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewJournalPlaceCancel.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewJournalPlaceCancel.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewJournalPlaceCancel.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewJournalPlaceCancel.setText("Cancel");
        jButtonNewJournalPlaceCancel.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewJournalPlaceCancel.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewJournalPlaceCancel.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewJournalPlaceCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewJournalPlaceCancelActionPerformed(evt);
            }
        });

        jButtonNewJournalPlaceCreate.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewJournalPlaceCreate.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewJournalPlaceCreate.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewJournalPlaceCreate.setText("Create");
        jButtonNewJournalPlaceCreate.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewJournalPlaceCreate.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewJournalPlaceCreate.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewJournalPlaceCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewJournalPlaceCreateActionPerformed(evt);
            }
        });

        jLabelNewJournalPlaceError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewJournalPlaceError.setForeground(new java.awt.Color(255, 0, 0));

        jLabelNewJournalPlaceName.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewJournalPlaceName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewJournalPlaceName.setText("Name");
        jLabelNewJournalPlaceName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabelNewJournalPlaceDescription.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelNewJournalPlaceDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewJournalPlaceDescription.setText("Description");
        jLabelNewJournalPlaceDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextAreaNewJournalPlace.setColumns(20);
        jTextAreaNewJournalPlace.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextAreaNewJournalPlace.setRows(5);
        jScrollPaneNewJournalPlace.setViewportView(jTextAreaNewJournalPlace);

        jTextFieldNewJournalPlaceName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanelNewJournalPlaceLayout = new javax.swing.GroupLayout(jPanelNewJournalPlace);
        jPanelNewJournalPlace.setLayout(jPanelNewJournalPlaceLayout);
        jPanelNewJournalPlaceLayout.setHorizontalGroup(
                jPanelNewJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewJournalPlaceLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelNewJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jScrollPaneNewJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jPanelNewJournalPlaceLayout.createSequentialGroup()
                                                .addGap(22, 22, 22)
                                                .addGroup(jPanelNewJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabelNewJournalPlaceName, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabelNewJournalPlaceEndDate, javax.swing.GroupLayout.Alignment.TRAILING))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanelNewJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jTextFieldNewJournalPlaceName)
                                                        .addComponent(jTextFieldNewJournalPlaceEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(jPanelNewJournalPlaceLayout.createSequentialGroup()
                                                .addComponent(jButtonNewJournalPlaceCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jButtonNewJournalPlaceCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabelNewJournalPlaceError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanelNewJournalPlaceLayout.createSequentialGroup()
                                .addGap(53, 53, 53)
                                .addComponent(jLabelNewJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 531, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(46, Short.MAX_VALUE))
                        .addGroup(jPanelNewJournalPlaceLayout.createSequentialGroup()
                                .addGap(229, 229, 229)
                                .addComponent(jLabelNewJournalPlaceDescription)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelNewJournalPlaceLayout.setVerticalGroup(
                jPanelNewJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewJournalPlaceLayout.createSequentialGroup()
                                .addGap(37, 37, 37)
                                .addComponent(jLabelNewJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                                .addGroup(jPanelNewJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jTextFieldNewJournalPlaceName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNewJournalPlaceName))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jTextFieldNewJournalPlaceEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelNewJournalPlaceEndDate))
                                .addGap(18, 18, 18)
                                .addComponent(jLabelNewJournalPlaceDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPaneNewJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(102, 102, 102)
                                .addComponent(jLabelNewJournalPlaceError, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(40, 40, 40)
                                .addGroup(jPanelNewJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewJournalPlaceCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonNewJournalPlaceCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(41, 41, 41))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initJournalPlaceDetailPanel() {

        jPanelJournalPlace = new javax.swing.JPanel();
        jLabelJournalPlace = new javax.swing.JLabel();
        jLabelJournalPlaceEndDate = new javax.swing.JLabel();
        jButtonJournalPlaceBack = new javax.swing.JButton();
        jButtonJournalPlaceNew = new javax.swing.JButton();
        jLabelJournalPlaceName = new javax.swing.JLabel();
        jTextFieldJournalPlaceName = new javax.swing.JTextField();
        jScrollPaneJournalPlace = new javax.swing.JScrollPane();
        jTextAreaJournalPlace = new javax.swing.JTextArea();
        jLabelJournalPlaceDescription = new javax.swing.JLabel();
        jTextFieldJournalPlaceEndDate = new javax.swing.JTextField();
        jLabelJournalPlacePot = new javax.swing.JLabel();
        jTextFieldJournalPlacePot = new javax.swing.JTextField();
        jButtonJournalPlaceModify = new javax.swing.JButton();
        jButtonJorunalPlaceDelete = new javax.swing.JButton();
        jPanelJournalPlaceList = new javax.swing.JPanel();
        jScrollPanelJournalPlace = new javax.swing.JScrollPane();
        jTablePlace = new javax.swing.JTable();

        String[] head = {"Name", "Date", "Price", "Address", "Visited"};
        this.table = null;

        if (this.placeList.size() > 0) {
            table = new String[this.placeList.size()][head.length];
        }

        this.jPanelJournalPlaceList.setBackground(new java.awt.Color(153, 102, 0));
        this.jScrollPanelJournalPlace.setBackground(new java.awt.Color(153, 102, 0));
        this.jTablePlace.setBackground(new java.awt.Color(153, 102, 0));
        this.jTablePlace.setGridColor(new java.awt.Color(153, 102, 0));

        int i = 0;
        for (Place item : this.placeList) {
            table[i][0] = item.getName();
            table[i][1] = Utils.dateToString(item.getDate().getTime());
            table[i][2] = Double.toString(item.getPrice());
            table[i][3] = item.getAddress();
            table[i][4] = (item.isVisited() ? "visited" : "not visited");
            i++;
        }

        if (this.placeList.size() > 0) {
            javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(table, head) {
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            this.jTablePlace.setModel(model);
        }
        this.jTablePlace.setColumnSelectionAllowed(false);
        this.jTablePlace.setDragEnabled(false);
        this.jTablePlace.setRowSelectionAllowed(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(620, 720));
        setMinimumSize(new java.awt.Dimension(620, 720));
        setPreferredSize(new java.awt.Dimension(620, 720));

        jPanelJournalPlace.setBackground(new java.awt.Color(255, 204, 102));
        jPanelJournalPlace.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelJournalPlace.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelJournalPlace.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelJournalPlace.setForeground(new java.awt.Color(153, 102, 0));
        jLabelJournalPlace.setText("Journal Place");
        jLabelJournalPlace.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelJournalPlace.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelJournalPlace.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelJournalPlace.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelJournalPlaceEndDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelJournalPlaceEndDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelJournalPlaceEndDate.setText("End date");

        jButtonJournalPlaceBack.setBackground(new java.awt.Color(153, 102, 0));
        jButtonJournalPlaceBack.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonJournalPlaceBack.setForeground(new java.awt.Color(0, 0, 0));
        jButtonJournalPlaceBack.setText("Back");
        jButtonJournalPlaceBack.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonJournalPlaceBack.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonJournalPlaceBack.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonJournalPlaceBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonJournalPlaceBackActionPerformed(evt);
            }
        });

        jButtonJournalPlaceNew.setBackground(new java.awt.Color(153, 102, 0));
        jButtonJournalPlaceNew.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonJournalPlaceNew.setForeground(new java.awt.Color(0, 0, 0));
        jButtonJournalPlaceNew.setText("New Place");
        jButtonJournalPlaceNew.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonJournalPlaceNew.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonJournalPlaceNew.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonJournalPlaceNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonJournalPlaceNewActionPerformed(evt);
            }
        });

        jLabelJournalPlaceName.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelJournalPlaceName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelJournalPlaceName.setText("Name");
        jLabelJournalPlaceName.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldJournalPlaceName.setEditable(false);
        jTextFieldJournalPlaceName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldJournalPlaceName.setEnabled(false);

        jTextAreaJournalPlace.setEditable(false);
        jTextAreaJournalPlace.setColumns(20);
        jTextAreaJournalPlace.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextAreaJournalPlace.setRows(5);
        jTextAreaJournalPlace.setEnabled(false);
        jScrollPaneJournalPlace.setViewportView(jTextAreaJournalPlace);

        jLabelJournalPlaceDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelJournalPlaceDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelJournalPlaceDescription.setText("Description");
        jLabelJournalPlaceDescription.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldJournalPlaceEndDate.setEditable(false);
        jTextFieldJournalPlaceEndDate.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextFieldJournalPlaceEndDate.setEnabled(false);

        jLabelJournalPlacePot.setFont(new java.awt.Font("Showcard Gothic", 1, 24)); // NOI18N
        jLabelJournalPlacePot.setForeground(new java.awt.Color(153, 102, 0));
        jLabelJournalPlacePot.setText("Pot");
        jLabelJournalPlacePot.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jTextFieldJournalPlacePot.setEditable(false);
        jTextFieldJournalPlacePot.setFont(new java.awt.Font("Showcard Gothic", 0, 14)); // NOI18N
        jTextFieldJournalPlacePot.setEnabled(false);

        jButtonJournalPlaceModify.setBackground(new java.awt.Color(153, 102, 0));
        jButtonJournalPlaceModify.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonJournalPlaceModify.setForeground(new java.awt.Color(0, 0, 0));
        jButtonJournalPlaceModify.setText("Modify");
        jButtonJournalPlaceModify.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonJournalPlaceModify.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonJournalPlaceModify.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonJournalPlaceModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonJournalPlaceModifyActionPerformed(evt);
            }
        });

        jButtonJorunalPlaceDelete.setBackground(new java.awt.Color(153, 102, 0));
        jButtonJorunalPlaceDelete.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonJorunalPlaceDelete.setForeground(new java.awt.Color(0, 0, 0));
        jButtonJorunalPlaceDelete.setText("Delete");
        jButtonJorunalPlaceDelete.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonJorunalPlaceDelete.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonJorunalPlaceDelete.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonJorunalPlaceDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonJorunalPlaceDeleteActionPerformed(evt);
            }
        });

        jScrollPanelJournalPlace.setViewportView(jTablePlace);

        javax.swing.GroupLayout jPanelJournalPlaceListLayout = new javax.swing.GroupLayout(jPanelJournalPlaceList);
        jPanelJournalPlaceList.setLayout(jPanelJournalPlaceListLayout);
        jPanelJournalPlaceListLayout.setHorizontalGroup(
                jPanelJournalPlaceListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPanelJournalPlace)
        );
        jPanelJournalPlaceListLayout.setVerticalGroup(
                jPanelJournalPlaceListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelJournalPlaceListLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jScrollPanelJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanelJournalPlaceLayout = new javax.swing.GroupLayout(jPanelJournalPlace);
        jPanelJournalPlace.setLayout(jPanelJournalPlaceLayout);
        jPanelJournalPlaceLayout.setHorizontalGroup(
                jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelJournalPlaceLayout.createSequentialGroup()
                                .addGroup(jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelJournalPlaceLayout.createSequentialGroup()
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabelJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(68, 68, 68))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelJournalPlaceLayout.createSequentialGroup()
                                                .addGap(38, 38, 38)
                                                .addGroup(jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jPanelJournalPlaceList, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addGroup(jPanelJournalPlaceLayout.createSequentialGroup()
                                                                .addComponent(jLabelJournalPlaceName)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jTextFieldJournalPlaceName)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(jLabelJournalPlacePot)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jTextFieldJournalPlacePot, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanelJournalPlaceLayout.createSequentialGroup()
                                                                .addComponent(jScrollPaneJournalPlace, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                                                                .addGap(36, 36, 36)
                                                                .addGroup(jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jButtonJournalPlaceNew, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jButtonJournalPlaceModify, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jButtonJorunalPlaceDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGroup(jPanelJournalPlaceLayout.createSequentialGroup()
                                                                .addGroup(jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jLabelJournalPlaceDescription)
                                                                        .addGroup(jPanelJournalPlaceLayout.createSequentialGroup()
                                                                                .addComponent(jLabelJournalPlaceEndDate)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(jTextFieldJournalPlaceEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                .addGap(0, 0, Short.MAX_VALUE)))))
                                .addGap(35, 35, 35))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelJournalPlaceLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButtonJournalPlaceBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(240, 240, 240))
        );
        jPanelJournalPlaceLayout.setVerticalGroup(
                jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelJournalPlaceLayout.createSequentialGroup()
                                .addGap(86, 86, 86)
                                .addComponent(jLabelJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addGroup(jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabelJournalPlaceName)
                                                .addComponent(jTextFieldJournalPlaceName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jTextFieldJournalPlacePot, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelJournalPlacePot))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabelJournalPlaceEndDate)
                                        .addComponent(jTextFieldJournalPlaceEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelJournalPlaceDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelJournalPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanelJournalPlaceLayout.createSequentialGroup()
                                                .addComponent(jButtonJournalPlaceNew, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jButtonJournalPlaceModify, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jButtonJorunalPlaceDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jScrollPaneJournalPlace))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanelJournalPlaceList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonJournalPlaceBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelJournalPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initNewPlacePanel() {

        jPanelNewPlace = new javax.swing.JPanel();
        jLabelNewPlace = new javax.swing.JLabel();
        jLabelNewPlaceDate = new javax.swing.JLabel();
        jButtonNewPlaceCancel = new javax.swing.JButton();
        jLabelNewPlaceDescription = new javax.swing.JLabel();
        jLabelNewPlacePrice = new javax.swing.JLabel();
        jLabelNewPlaceJournalDescription = new javax.swing.JLabel();
        jLabelNewPlaceAddress = new javax.swing.JLabel();
        jScrollPaneNewPlaceJournalDescription = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jScrollPaneNewPlaceDescription = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jTextFieldNewPlaceDate = new javax.swing.JTextField();
        jTextFieldNewPlacePrice = new javax.swing.JTextField();
        jTextFieldNewPlaceAddress = new javax.swing.JTextField();
        jButtonNewPlaceCreate = new javax.swing.JButton();
        jLabelNewPlaceError = new javax.swing.JLabel();
        jLabelNewPlaceName = new javax.swing.JLabel();
        jTextFieldNewPlaceName = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelNewPlace.setBackground(new java.awt.Color(255, 204, 102));
        jPanelNewPlace.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelNewPlace.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelNewPlace.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelNewPlace.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPlace.setText("New Place");
        jLabelNewPlace.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelNewPlace.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelNewPlace.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelNewPlace.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelNewPlaceDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewPlaceDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPlaceDate.setText("date");

        jButtonNewPlaceCancel.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewPlaceCancel.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewPlaceCancel.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewPlaceCancel.setText("Cancel");
        jButtonNewPlaceCancel.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewPlaceCancel.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewPlaceCancel.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewPlaceCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewPlaceCancelActionPerformed(evt);
            }
        });

        jLabelNewPlaceDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewPlaceDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPlaceDescription.setText("Description");

        jLabelNewPlacePrice.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewPlacePrice.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPlacePrice.setText("Price");

        jLabelNewPlaceJournalDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewPlaceJournalDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPlaceJournalDescription.setText("Journal description");

        jLabelNewPlaceAddress.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewPlaceAddress.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPlaceAddress.setText("Address");

        jTextArea2.setColumns(20);
        jTextArea2.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextArea2.setRows(5);
        jScrollPaneNewPlaceJournalDescription.setViewportView(jTextArea2);

        jTextArea3.setColumns(20);
        jTextArea3.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N
        jTextArea3.setRows(5);
        jScrollPaneNewPlaceDescription.setViewportView(jTextArea3);

        jTextFieldNewPlaceDate.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewPlacePrice.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jTextFieldNewPlaceAddress.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        jButtonNewPlaceCreate.setBackground(new java.awt.Color(153, 102, 0));
        jButtonNewPlaceCreate.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonNewPlaceCreate.setForeground(new java.awt.Color(0, 0, 0));
        jButtonNewPlaceCreate.setText("Create");
        jButtonNewPlaceCreate.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonNewPlaceCreate.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonNewPlaceCreate.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonNewPlaceCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewPlaceCreateActionPerformed(evt);
            }
        });

        jLabelNewPlaceError.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewPlaceError.setForeground(new java.awt.Color(255, 0, 0));
        jLabelNewPlaceError.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabelNewPlaceName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelNewPlaceName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelNewPlaceName.setText("Name");

        jTextFieldNewPlaceName.setFont(new java.awt.Font("Source Sans Pro Black", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanelNewPlaceLayout = new javax.swing.GroupLayout(jPanelNewPlace);
        jPanelNewPlace.setLayout(jPanelNewPlaceLayout);
        jPanelNewPlaceLayout.setHorizontalGroup(
                jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewPlaceLayout.createSequentialGroup()
                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelNewPlaceLayout.createSequentialGroup()
                                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelNewPlaceLayout.createSequentialGroup()
                                                                .addGap(34, 34, 34)
                                                                .addComponent(jScrollPaneNewPlaceDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanelNewPlaceLayout.createSequentialGroup()
                                                                .addGap(82, 82, 82)
                                                                .addComponent(jLabelNewPlaceDescription)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabelNewPlaceJournalDescription))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewPlaceLayout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(jScrollPaneNewPlaceJournalDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(10, 10, 10))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewPlaceLayout.createSequentialGroup()
                                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewPlaceLayout.createSequentialGroup()
                                                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(jLabelNewPlaceAddress)
                                                                        .addComponent(jLabelNewPlaceDate)
                                                                        .addComponent(jLabelNewPlacePrice)
                                                                        .addComponent(jLabelNewPlaceName))
                                                                .addGap(38, 38, 38)
                                                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jTextFieldNewPlaceDate, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextFieldNewPlacePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextFieldNewPlaceAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextFieldNewPlaceName, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addComponent(jLabelNewPlace, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(133, 133, 133)))
                                .addGap(24, 24, 24))
                        .addGroup(jPanelNewPlaceLayout.createSequentialGroup()
                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelNewPlaceLayout.createSequentialGroup()
                                                .addGap(74, 74, 74)
                                                .addComponent(jButtonNewPlaceCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(147, 147, 147)
                                                .addComponent(jButtonNewPlaceCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanelNewPlaceLayout.createSequentialGroup()
                                                .addContainerGap()
                                                .addComponent(jLabelNewPlaceError, javax.swing.GroupLayout.PREFERRED_SIZE, 511, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelNewPlaceLayout.setVerticalGroup(
                jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelNewPlaceLayout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(jLabelNewPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(23, 23, 23)
                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewPlaceName)
                                        .addComponent(jTextFieldNewPlaceName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewPlaceDate)
                                        .addComponent(jTextFieldNewPlaceDate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewPlacePrice)
                                        .addComponent(jTextFieldNewPlacePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelNewPlaceLayout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabelNewPlaceAddress)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 1, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNewPlaceLayout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldNewPlaceAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(57, 57, 57)
                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabelNewPlaceJournalDescription)
                                        .addComponent(jLabelNewPlaceDescription))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPaneNewPlaceDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jScrollPaneNewPlaceJournalDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelNewPlaceError, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanelNewPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonNewPlaceCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonNewPlaceCreate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(91, 91, 91))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelNewPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initPlacePanel() {

        jPanelPlace = new javax.swing.JPanel();
        jLabelPlace = new javax.swing.JLabel();
        jLabelPlaceDate = new javax.swing.JLabel();
        jLabelPlaceDescription = new javax.swing.JLabel();
        jLabelPlacePrice = new javax.swing.JLabel();
        jLabelPlaceJournalDescription = new javax.swing.JLabel();
        jLabelPlaceAddress = new javax.swing.JLabel();
        jButtonPlaceBack = new javax.swing.JButton();
        jLabelPlaceName = new javax.swing.JLabel();
        jTextFieldPlaceName = new javax.swing.JTextField();
        jTextFieldPlaceDate = new javax.swing.JTextField();
        jTextFieldPlaceAddress = new javax.swing.JTextField();
        jTextFieldPlacePrice = new javax.swing.JTextField();
        jTextFieldPlaceDescription = new javax.swing.JTextField();
        jTextFieldPlaceJournalDescription = new javax.swing.JTextField();
        jCheckBoxPlaceVisited = new javax.swing.JCheckBox();
        jButtonPlaceAcept = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(620, 720));
        setMinimumSize(new java.awt.Dimension(620, 720));
        setPreferredSize(new java.awt.Dimension(620, 720));

        jPanelPlace.setBackground(new java.awt.Color(255, 204, 102));
        jPanelPlace.setMaximumSize(new java.awt.Dimension(620, 720));
        jPanelPlace.setMinimumSize(new java.awt.Dimension(620, 720));

        jLabelPlace.setFont(new java.awt.Font("Showcard Gothic", 1, 48)); // NOI18N
        jLabelPlace.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPlace.setText("Place");
        jLabelPlace.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelPlace.setMaximumSize(new java.awt.Dimension(620, 720));
        jLabelPlace.setMinimumSize(new java.awt.Dimension(620, 720));
        jLabelPlace.setPreferredSize(new java.awt.Dimension(620, 720));

        jLabelPlaceDate.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelPlaceDate.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPlaceDate.setText("date");

        jLabelPlaceDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelPlaceDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPlaceDescription.setText("Description");

        jLabelPlacePrice.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelPlacePrice.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPlacePrice.setText("Price");

        jLabelPlaceJournalDescription.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelPlaceJournalDescription.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPlaceJournalDescription.setText("Journal description");

        jLabelPlaceAddress.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelPlaceAddress.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPlaceAddress.setText("Address");

        jButtonPlaceBack.setBackground(new java.awt.Color(153, 102, 0));
        jButtonPlaceBack.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonPlaceBack.setForeground(new java.awt.Color(0, 0, 0));
        jButtonPlaceBack.setText("Back");
        jButtonPlaceBack.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonPlaceBack.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonPlaceBack.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonPlaceBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPlaceBackActionPerformed(evt);
            }
        });

        jLabelPlaceName.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jLabelPlaceName.setForeground(new java.awt.Color(153, 102, 0));
        jLabelPlaceName.setText("Name");

        jTextFieldPlaceName.setEditable(false);
        jTextFieldPlaceName.setEnabled(false);

        jTextFieldPlaceDate.setEditable(false);
        jTextFieldPlaceDate.setEnabled(false);

        jTextFieldPlaceAddress.setEditable(false);
        jTextFieldPlaceAddress.setEnabled(false);

        jTextFieldPlacePrice.setEditable(false);
        jTextFieldPlacePrice.setEnabled(false);

        jTextFieldPlaceDescription.setEditable(false);
        jTextFieldPlaceDescription.setEnabled(false);

        jTextFieldPlaceJournalDescription.setEditable(false);
        jTextFieldPlaceJournalDescription.setEnabled(false);

        jCheckBoxPlaceVisited.setBackground(new java.awt.Color(255, 204, 102));
        jCheckBoxPlaceVisited.setFont(new java.awt.Font("Showcard Gothic", 0, 24)); // NOI18N
        jCheckBoxPlaceVisited.setForeground(new java.awt.Color(153, 102, 0));
        jCheckBoxPlaceVisited.setText("Visited");

        jButtonPlaceAcept.setBackground(new java.awt.Color(153, 102, 0));
        jButtonPlaceAcept.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonPlaceAcept.setForeground(new java.awt.Color(0, 0, 0));
        jButtonPlaceAcept.setText("Acept");
        jButtonPlaceAcept.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonPlaceAcept.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonPlaceAcept.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonPlaceAcept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPlaceAceptActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelPlaceLayout = new javax.swing.GroupLayout(jPanelPlace);
        jPanelPlace.setLayout(jPanelPlaceLayout);
        jPanelPlaceLayout.setHorizontalGroup(
                jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPlaceLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPlaceLayout.createSequentialGroup()
                                                .addComponent(jLabelPlaceDescription)
                                                .addGap(230, 230, 230))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPlaceLayout.createSequentialGroup()
                                                .addComponent(jLabelPlaceJournalDescription)
                                                .addGap(174, 174, 174))))
                        .addGroup(jPanelPlaceLayout.createSequentialGroup()
                                .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelPlaceLayout.createSequentialGroup()
                                                .addGap(32, 32, 32)
                                                .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                .addGroup(jPanelPlaceLayout.createSequentialGroup()
                                                                        .addComponent(jLabelPlaceAddress)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jTextFieldPlaceAddress))
                                                                .addGroup(jPanelPlaceLayout.createSequentialGroup()
                                                                        .addComponent(jLabelPlaceName)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jTextFieldPlaceName, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jLabelPlaceDate)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jTextFieldPlaceDate, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addComponent(jTextFieldPlaceDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGroup(jPanelPlaceLayout.createSequentialGroup()
                                                                        .addComponent(jLabelPlacePrice)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(jTextFieldPlacePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGap(186, 186, 186))
                                                                .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addGroup(jPanelPlaceLayout.createSequentialGroup()
                                                                                .addComponent(jButtonPlaceBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                .addComponent(jButtonPlaceAcept, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addComponent(jTextFieldPlaceJournalDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                        .addGroup(jPanelPlaceLayout.createSequentialGroup()
                                                .addGap(227, 227, 227)
                                                .addComponent(jLabelPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(27, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPlaceLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jCheckBoxPlaceVisited)
                                .addGap(249, 249, 249))
        );
        jPanelPlaceLayout.setVerticalGroup(
                jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelPlaceLayout.createSequentialGroup()
                                .addGap(49, 49, 49)
                                .addComponent(jLabelPlace, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(35, 35, 35)
                                .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jLabelPlaceName)
                                                .addGroup(jPanelPlaceLayout.createSequentialGroup()
                                                        .addComponent(jTextFieldPlaceName)
                                                        .addGap(2, 2, 2)))
                                        .addComponent(jLabelPlaceDate)
                                        .addComponent(jTextFieldPlaceDate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jTextFieldPlaceAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabelPlaceAddress))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabelPlacePrice)
                                        .addComponent(jTextFieldPlacePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelPlaceDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldPlaceDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelPlaceJournalDescription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldPlaceJournalDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxPlaceVisited)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                                .addGroup(jPanelPlaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonPlaceAcept, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonPlaceBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(43, 43, 43))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jPanelPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void initManagerUserPanel() {
        ManageUser = new javax.swing.JPanel();
        jLabelManageUser = new javax.swing.JLabel();
        jComboBoxCreateUsers = new javax.swing.JComboBox<>();
        jButtonManageUserDelete = new javax.swing.JButton();
        jButtonManageUserBack = new javax.swing.JButton();
        jPaneManagerUserList = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTableManagerUserTable = new javax.swing.JTable();

        String[] head = {"Name", "Surnames", "Username"};
        this.table = null;

        if (this.userList.size() > 0) {
            table = new String[this.userList.size()][head.length];
        }

        this.ManageUser.setBackground(new java.awt.Color(153, 102, 0));
        this.jScrollPane7.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableManagerUserTable.setBackground(new java.awt.Color(153, 102, 0));
        this.jTableManagerUserTable.setGridColor(new java.awt.Color(153, 102, 0));

        int i = 0;
        for (User user : this.userList) {
            table[i][0] = user.getName();
            table[i][1] = user.getSurnames();
            table[i][2] = user.getUser();
            i++;
        }

        if (this.userList.size() > 0) {
            javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(table, head) {
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            this.jTableManagerUserTable.setModel(model);
        }
        this.jTableManagerUserTable.setColumnSelectionAllowed(false);
        this.jTableManagerUserTable.setDragEnabled(false);
        this.jTableManagerUserTable.setRowSelectionAllowed(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        ManageUser.setBackground(new java.awt.Color(255, 204, 102));
        ManageUser.setMaximumSize(new java.awt.Dimension(620, 720));
        ManageUser.setMinimumSize(new java.awt.Dimension(620, 720));
        ManageUser.setName(""); // NOI18N

        jLabelManageUser.setFont(new java.awt.Font("Showcard Gothic", 1, 55)); // NOI18N
        jLabelManageUser.setForeground(new java.awt.Color(153, 102, 0));
        jLabelManageUser.setText("Manage user");
        jLabelManageUser.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jComboBoxCreateUsers.setBackground(new java.awt.Color(153, 102, 0));
        jComboBoxCreateUsers.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jComboBoxCreateUsers.setForeground(new java.awt.Color(0, 0, 0));
        jComboBoxCreateUsers.setMaximumRowCount(4);
        jComboBoxCreateUsers.setMaximumSize(new java.awt.Dimension(114, 29));
        jComboBoxCreateUsers.setMinimumSize(new java.awt.Dimension(114, 29));
        jComboBoxCreateUsers.setPreferredSize(new java.awt.Dimension(114, 29));
        jComboBoxCreateUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxCreateUsersActionPerformed(evt);
            }
        });
        String[] combo = null;
        if (this.notUserList.size() != 0) {
            combo = new String[this.notUserList.size()];
            i = 0;
            for (User user : notUserList) {
                combo[i] = user.getUser();
                i++;
            }
        }
        if (this.notUserList.size() != 0) {
            jComboBoxCreateUsers.setModel(new javax.swing.DefaultComboBoxModel<>(combo));
        }

        jButtonManageUserDelete.setBackground(new java.awt.Color(153, 102, 0));
        jButtonManageUserDelete.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonManageUserDelete.setForeground(new java.awt.Color(0, 0, 0));
        jButtonManageUserDelete.setText("Delete");
        jButtonManageUserDelete.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonManageUserDelete.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonManageUserDelete.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonManageUserDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonManageUserDeleteActionPerformed(evt);
            }
        });

        jButtonManageUserBack.setBackground(new java.awt.Color(153, 102, 0));
        jButtonManageUserBack.setFont(new java.awt.Font("Showcard Gothic", 0, 18)); // NOI18N
        jButtonManageUserBack.setForeground(new java.awt.Color(0, 0, 0));
        jButtonManageUserBack.setText("Back");
        jButtonManageUserBack.setMaximumSize(new java.awt.Dimension(114, 29));
        jButtonManageUserBack.setMinimumSize(new java.awt.Dimension(114, 29));
        jButtonManageUserBack.setPreferredSize(new java.awt.Dimension(114, 29));
        jButtonManageUserBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonManageUserBackActionPerformed(evt);
            }
        });

        jScrollPane7.setViewportView(jTableManagerUserTable);

        javax.swing.GroupLayout jPaneManagerUserListLayout = new javax.swing.GroupLayout(jPaneManagerUserList);
        jPaneManagerUserList.setLayout(jPaneManagerUserListLayout);
        jPaneManagerUserListLayout.setHorizontalGroup(
                jPaneManagerUserListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 577, Short.MAX_VALUE)
        );
        jPaneManagerUserListLayout.setVerticalGroup(
                jPaneManagerUserListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout ManageUserLayout = new javax.swing.GroupLayout(ManageUser);
        ManageUser.setLayout(ManageUserLayout);
        ManageUserLayout.setHorizontalGroup(
                ManageUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(ManageUserLayout.createSequentialGroup()
                                .addGap(113, 113, 113)
                                .addGroup(ManageUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(ManageUserLayout.createSequentialGroup()
                                                .addComponent(jComboBoxCreateUsers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jButtonManageUserDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(26, 26, 26)
                                                .addComponent(jButtonManageUserBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabelManageUser))
                                .addContainerGap(112, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ManageUserLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jPaneManagerUserList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(21, 21, 21))
        );
        ManageUserLayout.setVerticalGroup(
                ManageUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(ManageUserLayout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addComponent(jLabelManageUser)
                                .addGap(18, 18, 18)
                                .addGroup(ManageUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jComboBoxCreateUsers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonManageUserBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonManageUserDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPaneManagerUserList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(162, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1086, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(ManageUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 766, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(ManageUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }

    private void moveToEvent() {
        this.connect();
        this.getContentPane().removeAll();
        this.initEventPanel();
        this.setContentPane(this.jPanelNewEvent);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToTrip() {
        this.connect();
        this.getContentPane().removeAll();
        this.initTripPanel();
        this.setContentPane(this.jPanelNewTrip);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToPurchase() {
        this.connect();
        this.getContentPane().removeAll();
        this.initnewPurchasePanel();
        this.setContentPane(this.jPanelNewPurchase);
        this.invalidate();
        this.validate();
        this.setVisible(true);
    }

    private void moveToDebt() {
        this.connect();
        this.getContentPane().removeAll();
        this.initnewDebtPanel();
        this.setContentPane(this.jPanelNewDebt);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    public void moveToApartmentDetail() {
        this.connect();
        try {
            Apartment apartment = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            this.debtList = Debt.getDebtsByGroup(apartment.getId());
            this.journalPlaceList = JournalPlace.getJournalPlacesByGroup(apartment.getId());
            this.purchaseList = Purchase.getPurchasesByGroup(apartment.getId());
            this.taskList = Task.getTasksByGroup(apartment.getId());
            this.getContentPane().removeAll();
            this.initApartmentDetailPanel();
            this.setContentPane(this.jPanelApartment);
            this.initApartmentDetail();
            this.invalidate();
            this.validate();
            this.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();

    }

    private void moveToPurchaseDetail(String message) {
        this.connect();
        try {
            Purchase purchase = purchaseList.get(this.indexSelectedActivity);
            this.articleList = Article.getArticlesByActivity(purchase.getId());
            this.getContentPane().removeAll();
            this.initPurchaseDetailPanel();
            this.setContentPane(this.jPanelPurchase);
            this.initPurchaseDetail();
            this.jLabel1.setText(message);
            this.invalidate();
            this.validate();
            this.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void moveToDebtDetail() {
        this.connect();
        try {
            Debt debt = debtList.get(this.indexSelectedActivity);
            this.chargeList = Charge.getChargesByActivity(debt.getId());
            this.getContentPane().removeAll();
            this.initDebtDetailPanel();
            this.setContentPane(this.jPanelDebt);
            this.initDebtDetail();
            this.invalidate();
            this.validate();
            this.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void moveToNewArticle() {
        this.connect();
        this.getContentPane().removeAll();
        this.initNewArticlePanel();
        this.setContentPane(this.jPanelNewArticle);
        this.invalidate();
        this.validate();
        this.setVisible(true);
    }

    private void moveToNewCharge() {
        this.connect();
        this.getContentPane().removeAll();
        this.initNewChargePanel();
        this.setContentPane(this.jPanelNewCharge);
        this.initNewDebt();
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToCharge() {
        this.connect();
        this.getContentPane().removeAll();
        this.initChargePanel();
        this.setContentPane(this.jPanelCharge);
        this.initCharge();
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToTask() {
        this.connect();
        this.getContentPane().removeAll();
        this.initNewTaskPanel();
        this.setContentPane(this.jPanelNewTask);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToTaskDetail() {
        this.connect();
        try {
            Task task = taskList.get(this.indexSelectedActivity);
            this.dutyList = Duty.getDutiesByActivity(task.getId());
            this.getContentPane().removeAll();
            this.initTaskDetailPanel();
            this.setContentPane(this.jPanelTask);
            this.initTaskDetail();
            this.invalidate();
            this.validate();
            this.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void moveToNewDuty() {
        this.connect();
        this.getContentPane().removeAll();
        this.initNewDutyPanel();
        this.setContentPane(this.jPanelNewDuty);
        this.initNewTask();
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToDuty() {
        this.connect();
        this.getContentPane().removeAll();
        this.initDutyPanel();
        this.setContentPane(this.jPanelDuty);
        this.initDuty();
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToJournalPlace() {
        this.connect();
        this.getContentPane().removeAll();
        this.initNewJournalPlacePanel();
        this.setContentPane(this.jPanelNewJournalPlace);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToJournalPlaceDetail() {
        this.connect();
        try {
            JournalPlace journalPlace = journalPlaceList.get(this.indexSelectedActivity);
            this.placeList = Place.getPlacesByActivity(journalPlace.getId());
            this.getContentPane().removeAll();
            this.initJournalPlaceDetailPanel();
            this.setContentPane(this.jPanelJournalPlace);
            this.initJournalPlaceDetail();
            this.invalidate();
            this.validate();
            this.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void moveToNewPlace() {
        this.connect();
        this.getContentPane().removeAll();
        this.initNewPlacePanel();
        this.setContentPane(this.jPanelNewPlace);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToPlaceDetail() {
        this.connect();
        this.getContentPane().removeAll();
        this.initPlacePanel();
        this.setContentPane(this.jPanelPlace);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void moveToPlace() {
        this.connect();
        this.getContentPane().removeAll();
        this.initPlacePanel();
        this.setContentPane(this.jPanelPlace);
        this.initPlace();
        this.invalidate();
        this.validate();
        this.setVisible(true);
    }

    public void moveToManagerUser() {
        this.connect();
        this.getContentPane().removeAll();
        this.initManagerUser();
        this.initManagerUserPanel();
        this.setContentPane(this.ManageUser);
        this.invalidate();
        this.validate();
        this.setVisible(true);
        this.disconnect();
    }

    private void initPlace() {
        int index = this.jTablePlace.getSelectedRow();
        this.indexSelectedItem = index;
        this.typeSelectedItem = "place";
        Place place = placeList.get(index);
        this.jTextFieldPlaceName.setText(place.getName());
        this.jTextFieldPlaceAddress.setText(place.getAddress());
        this.jTextFieldPlacePrice.setText(Double.toString(place.getPrice()));
        this.jTextFieldPlaceDate.setText(Utils.dateToString(place.getDate().getTime()));
        this.jTextFieldPlaceDescription.setText(place.getDescription());
        this.jTextFieldPlaceJournalDescription.setText(place.getJournalDescription());
    }

    private void initApartmentDetail() {
        Apartment apartment = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
        this.jTextFieldApartmentDetailName1.setText(apartment.getName());
        this.jTextFieldApartmentDetailAddress.setText(apartment.getAddress());
        this.jTextFieldApartmentDetailPot.setText(Double.toString(apartment.getPot()));
        this.jTextAreaApartmentDetailDescription.setText(apartment.getDescription());

    }

    private void initPurchaseDetail() {
        Purchase purchase = purchaseList.get(this.indexSelectedActivity);
        Group group = null;

        if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
            group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
        } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
            group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
        } else {
            group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
        }

        this.jTextFieldPruchaseName.setText(purchase.getName());
        this.jTextAreaPruchaseDescription.setText(purchase.getDescription());
        this.jTextFieldPruchaseName1.setText(Double.toString(group.getPot()));

        double pay = 0;
        for (Article article : articleList) {
            if (!article.getPaid()) {
                pay += article.getAmount() * article.getPrice();
            }
        }

        this.jTextFieldPruchaseTotal.setText(Double.toString(pay));

    }

    private void initDebtDetail() {
        Debt debt = debtList.get(this.indexSelectedActivity);

        this.jTextFieldDebtName.setText(debt.getName());
        this.jTextFieldDebtTotalCount.setText(Double.toString(debt.getCountTotal()));
        this.jTextAreaDebtDescription.setText(debt.getDescription());
        this.jTextFieldDebtDate.setText(Utils.dateToString(debt.getDate().getTime()));

    }

    private void initNewDebt() {
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }
            List<User> lst = User.getUsersByGroup(group.getId());
            for (User user : lst) {
                this.jComboBoxNewChargeUsers.addItem(user.getUser());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void initCharge() {
        int index = this.jTableDebt.getSelectedRow();
        this.indexSelectedItem = index;
        this.typeSelectedItem = "charge";
        Charge charge = chargeList.get(index);
        this.jTextFieldChargeName.setText(charge.getName());
        this.jTextFieldChargeAmount.setText(Double.toString(charge.getAmount()));
        this.jTextFieldChargeUser.setText(charge.getUser().getUser());
    }

    private void initTaskDetail() {
        Task task = taskList.get(this.indexSelectedActivity);

        this.jTextFieldTaskName.setText(task.getName());
        this.jTextFieldTaskDescription.setText(task.getDescription());
        this.jTextFieldNewTaskName2.setText(Utils.dateToString(task.getEndDate().getTime()));
    }

    private void initNewTask() {
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }
            List<User> lst = User.getUsersByGroup(group.getId());
            for (User user : lst) {
                this.jComboBoxNewDutyUsers.addItem(user.getUser());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void initDuty() {
        int index = this.jTableDuty.getSelectedRow();
        this.indexSelectedItem = index;
        this.typeSelectedItem = "duty";
        Duty duty = dutyList.get(index);
        this.jTextFieldDutyDescription.setText(duty.getDescription());
        this.jTextFieldDutyEndDate.setText(Utils.dateToString(duty.getEndDate().getTime()));
        this.jTextFieldDutyUser.setText(duty.getUser().getUser());
    }

    private void initManagerUser() {
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }

            this.notUserList = User.getUsersNotInGroup(group.getId());
            this.userList = User.getUsersByGroup(group.getId());
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initJournalPlaceDetail() {
        JournalPlace journalPlace = journalPlaceList.get(this.indexSelectedActivity);

        Group group = null;

        if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
            group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
        } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
            group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
        } else {
            group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
        }

        this.jTextFieldJournalPlaceName.setText(journalPlace.getName());
        this.jTextFieldJournalPlacePot.setText(Double.toString(group.getPot()));
        this.jTextAreaJournalPlace.setText(journalPlace.getDescription());
        this.jTextFieldJournalPlaceEndDate.setText(Utils.dateToString(journalPlace.getEndDate().getTime()));
    }

    private int calculateIndexActivity(int index) {
        int indexList = 0;
        if (index < this.debtList.size()) {
            indexList = index;
        } else if (index < (this.debtList.size() + this.journalPlaceList.size())) {
            indexList = index - this.debtList.size();
        } else if (index < (this.debtList.size() + this.journalPlaceList.size() + purchaseList.size())) {
            indexList = index - (this.debtList.size() + this.journalPlaceList.size());
        } else {
            indexList = index - (this.debtList.size() + this.journalPlaceList.size() + purchaseList.size());
        }
        return indexList;
    }

    //internal Controller
    private void jButtonNewTripCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToGroup(this.controllerGroup.getUserLogin().getName());
    }

    private void jButtonNewApartmentCreateActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            String groupName = this.jTextFieldNewApartmentGroupName.getText();
            String address = this.jTextFieldNewApartmentAddress.getText();
            String description = this.jTextAreaNewApartmentDescription.getText();
            double pot = Double.parseDouble(this.jTextFieldNewApartmentPot.getText());
            if (groupName.trim().equals("") || address.trim().trim().equals("")) {
                this.jLabelNewApartmentError.setText(this.messageErrorEmpty);
            } else {
                Apartment apartment = new Apartment(groupName, description, address, pot);
                apartment.addUser(this.controllerGroup.getUserLogin());
                this.moveToGroup(this.controllerGroup.getUserLogin().getName());
            }
        } catch (SQLException ex) {
            this.jLabelNewApartmentError.setText(this.messageErrorGroupTaken);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        } catch (NumberFormatException ex) {
            this.jLabelNewApartmentError.setText(this.messageErrorPot);
        }
        this.disconnect();
    }

    private void jButtonNewEventCreate1ActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            String groupName = this.jTextFieldNewEventGroupName1.getText();
            Calendar date = Calendar.getInstance();
            date.setTime(Utils.stringToDate(this.jTextFieldNewEventDate1.getText()));
            String description = this.jTextAreaNewEventDescription1.getText();
            double pot = Double.parseDouble(this.jTextFieldNewEventPot1.getText());
            String place = this.jTextFieldNewEventPlace1.getText();
            if (groupName.trim().equals("") || place.trim().equals("")) {
                this.jLabelNewEventError.setText(this.messageErrorEmpty);
            } else {
                Event event = new Event(groupName, place, date, pot, description);
                event.addUser(this.controllerGroup.getUserLogin());
                this.moveToGroup(this.controllerGroup.getUserLogin().getName());
            }
        } catch (SQLException ex) {
            this.jLabelNewEventError.setText(this.messageErrorGroupTaken);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        } catch (NumberFormatException ex) {
            this.jLabelNewEventError.setText(this.messageErrorPot);
        } catch (ParseException ex) {
            this.jLabelNewEventError.setText(this.messageErrorDate);
        }
        this.disconnect();
    }

    private void jButtonNewTripCreate1ActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            String groupName = this.jTextFieldNewTripGroupName.getText();
            Calendar date = Calendar.getInstance();
            date.setTime(Utils.stringToDate(this.jTextFieldNewTripDate.getText()));
            String description = this.jTextAreaNewTripDescription.getText();
            double pot = Double.parseDouble(this.jTextFieldNewTripPot.getText());
            String place = this.jTextFieldNewTripPlace.getText();
            String lodgin = this.jTextFieldNewTripLodging.getText();
            String transport = this.jTextFieldNewTripTransport.getText();
            if (groupName.trim().equals("") || place.trim().equals("") || lodgin.trim().equals("") || transport.trim().equals("")) {
                this.jLabelNewTripError.setText(this.messageErrorEmpty);
            } else {
                Trip trip = new Trip(place, lodgin, transport, groupName, date, pot, description);
                trip.addUser(this.controllerGroup.getUserLogin());
                this.moveToGroup(this.controllerGroup.getUserLogin().getName());
            }
        } catch (SQLException ex) {
            this.jLabelNewTripError.setText(this.messageErrorGroupTaken);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        } catch (NumberFormatException ex) {
            this.jLabelNewTripError.setText(this.messageErrorPot);
        } catch (ParseException ex) {
            this.jLabelNewTripError.setText(this.messageErrorDate);
        }
        this.disconnect();
    }

    private void jButtonNewApartmentCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToGroup(this.controllerGroup.getUserLogin().getName());
    }

    private void jButtonNewEventCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToGroup(this.controllerGroup.getUserLogin().getName());
    }

    private void jComboBoxApartmentNewActivityActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        String selectedItem = (String) this.jComboBoxApartmentNewActivity.getSelectedItem();
        if (selectedItem.equalsIgnoreCase("Debt")) {
            this.moveToDebt();
        } else if (selectedItem.equalsIgnoreCase("Journal Place")) {
            this.moveToJournalPlace();
        } else if (selectedItem.equalsIgnoreCase("Purchase")) {
            this.moveToPurchase();
        } else {
            this.moveToTask();
        }
        this.disconnect();
    }

    private void jButtonApartmentDetailViewActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        int index = this.jTableApartmentDetailTable.getSelectedRow();
        if (this.jTableGroup.getSelectedRowCount() == 1) {
            if (this.table[index][0].equalsIgnoreCase("Debt")) {
                this.indexSelectedActivity = calculateIndexActivity(index);
                this.typeSelectedActivity = "Debt";
                this.moveToDebtDetail();
            } else if (this.table[index][0].equalsIgnoreCase("Journal Place")) {
                this.indexSelectedActivity = calculateIndexActivity(index);
                this.typeSelectedActivity = "Journal Place";
                this.moveToJournalPlaceDetail();
            } else if (this.table[index][0].equalsIgnoreCase("Purchase")) {
                this.indexSelectedActivity = calculateIndexActivity(index);
                this.typeSelectedActivity = "Purchase";
                this.moveToPurchaseDetail("");
            } else {
                this.indexSelectedActivity = calculateIndexActivity(index);
                this.typeSelectedActivity = "Task";
                this.moveToTaskDetail();
            }
        }
        this.disconnect();
    }

    private void jButtonApartmentDetailDeleteActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            int index = this.jTableApartmentDetailTable.getSelectedRow();
            if (this.jTableGroup.getSelectedRowCount() == 1) {
                if (this.table[index][0].equalsIgnoreCase("Debt")) {
                    this.indexSelectedActivity = calculateIndexActivity(index);
                    this.typeSelectedActivity = "Debt";
                    this.debtList.get(indexSelectedActivity).delete();
                } else if (this.table[index][0].equalsIgnoreCase("Journal Place")) {
                    this.indexSelectedActivity = calculateIndexActivity(index);
                    this.typeSelectedActivity = "Journal Place";
                    this.journalPlaceList.get(indexSelectedActivity).delete();
                } else if (this.table[index][0].equalsIgnoreCase("Purchase")) {
                    this.indexSelectedActivity = calculateIndexActivity(index);
                    this.typeSelectedActivity = "Purchase";
                    this.purchaseList.get(indexSelectedActivity).delete();

                } else {
                    this.indexSelectedActivity = calculateIndexActivity(index);
                    this.typeSelectedActivity = "Task";
                    this.taskList.get(indexSelectedActivity).delete();
                }
                this.moveToApartmentDetail();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        this.disconnect();
    }

    private void jButtonNewPruchaseCancelActionPerformed(java.awt.event.ActionEvent evt) {
        moveToApartmentDetail();
    }

    private void jButtonNewPruchaseCreateActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }

            String name = this.jTextFieldNewPruchaseName.getText().trim();
            String description = this.jTextAreaNewPruchaseDescription.getText();
            if (name.equals("")) {
                this.jLabelNewPruchaseError.setText(this.messageErrorEmpty);
            } else {
                Purchase purchase = new Purchase(false, name, description, group, null);
                this.moveToApartmentDetail();
            }
        } catch (SQLException ex) {
            this.jLabelNewPruchaseError.setText(this.messageErrorActivityTaken);
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        }
        this.disconnect();
    }

    private void jButtonPurchaseBackActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToApartmentDetail();
    }

    private void jButtonPurchaseNewActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToNewArticle();
    }

    private void jButtonPurchasePayActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Purchase purchase = purchaseList.get(this.indexSelectedActivity);
            String message = "";

            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }

            if (!purchase.isPaid()) {
                this.articleList = Article.getArticlesByActivity(purchase.getId());
                double pot = group.getPot();
                double pay = 0;
                double totalpay = 0;
                double notpay = 0;

                for (Article article : articleList) {
                    if (!article.getPaid()) {
                        pay = article.getAmount() * article.getPrice();

                        if (pot - pay >= 0) {
                            pot -= pay;
                            totalpay += pay;
                            article.setPaid(true);
                        } else {
                            notpay += pay;
                        }

                    }

                }
                group.setPot(pot);
                message = "Price total paid is " + totalpay + "€";
                if (notpay > 0) {
                    message += " and is not paid " + notpay + "€. It need to put money in the pot";
                }

            } else {
                message = messageErrorEverythingPaid;
            }
            this.moveToPurchaseDetail(message);
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void jButtonPurchaseDeleteActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        int index = this.jTable1.getSelectedRow();
        try {
            this.articleList.get(index).delete();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        this.moveToPurchaseDetail("");
        this.disconnect();
    }

    private void jButtonApartmentBack1ActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToGroup(this.controllerGroup.getUserLogin().getName());
    }

    private void jButtonNewJournalPlaceCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToApartmentDetail();
    }

    private void jButtonNewJournalPlaceCreateActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }

            String name = this.jTextFieldNewJournalPlaceName.getText().trim();
            Calendar endDate = Calendar.getInstance();
            endDate.setTime(Utils.stringToDate(this.jTextFieldNewJournalPlaceEndDate.getText()));
            String description = this.jTextAreaNewJournalPlace.getText().trim();
            if (name.equals("")) {
                this.jLabelNewJournalPlaceError.setText(this.messageErrorEmpty);
            } else {
                JournalPlace journalPlace = new JournalPlace(endDate, name, description, group, null);
                this.moveToApartmentDetail();
            }
        } catch (SQLException ex) {
            this.jLabelNewJournalPlaceError.setText(this.messageErrorActivityTaken);
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        } catch (ParseException ex) {
            this.jLabelNewJournalPlaceError.setText(this.messageErrorDate);
        }
        this.disconnect();
    }

    private void jButtonJournalPlaceBackActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToApartmentDetail();
    }

    private void jButtonJournalPlaceNewActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToNewPlace();
    }

    private void jButtonJournalPlaceModifyActionPerformed(java.awt.event.ActionEvent evt) {
        if (this.jTablePlace.getSelectedRow() != -1)
        this.moveToPlace();
    }

    private void jButtonJorunalPlaceDeleteActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        int index = this.jTablePlace.getSelectedRow();
        try {
            this.placeList.get(index).delete();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        this.moveToJournalPlaceDetail();
        this.disconnect();
    }

    private void jButtonNewDutyCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToTaskDetail();
    }

    private void jButtonNewDutyCreateActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }

            String name = this.jTextFieldNewDutyName.getText().trim();
            Calendar endDate = Calendar.getInstance();
            endDate.setTime(Utils.stringToDate(this.jTextFieldNewDutyEndDate.getText()));
            String selectedNickname = (String) this.jComboBoxNewDutyUsers.getSelectedItem();
            User user = User.getUserbyNickName(selectedNickname);
            String description = this.jTextAreaNewDuty.getText().trim();
            Task task = taskList.get(this.indexSelectedActivity);
            if (name.equals("")) {
                this.jLabelNewDutyError.setText(this.messageErrorEmpty);
            } else {
                Duty duty = new Duty(description, user, false, name, task, endDate);
                this.moveToTaskDetail();
            }
        } catch (SQLException ex) {
            this.jLabelNewDutyError.setText(this.messageErrorItemTaken);
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        } catch (ParseException ex) {
            this.jLabelNewDutyError.setText(this.messageErrorDate);
        }
        this.disconnect();
    }

    private void jButtonChargeBackActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToDebtDetail();
    }

    private void jButtonTaskBackActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToApartmentDetail();
    }

    private void jButtonTaskNewActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToNewDuty();
    }

    private void jButtonTaskModifyActionPerformed(java.awt.event.ActionEvent evt) {
        if (this.jTableDuty.getSelectedRow() != -1){
            this.moveToDuty();
        }
    }

    private void jButtonTaskDeleteActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        int index = this.jTableDuty.getSelectedRow();
        try {
            this.dutyList.get(index).delete();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        this.moveToTaskDetail();
        this.disconnect();
    }

    private void jButtonNewTaskCancelActionPerformed(java.awt.event.ActionEvent evt) {
        moveToApartmentDetail();
    }

    private void jButtonNewTaskCreateActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }

            String name = this.jTextFieldNewTaskName.getText().trim();
            Calendar endDate = Calendar.getInstance();
            endDate.setTime(Utils.stringToDate(this.jTextFieldNewTaskEndDate.getText()));
            String description = this.jTextFieldNewTaskDescription.getText().trim();
            if (name.equals("")) {
                this.jLabelNewTaskError.setText(this.messageErrorEmpty);
            } else {
                Task task = new Task(false, endDate, name, description, group, null);
                this.moveToApartmentDetail();
            }
        } catch (SQLException ex) {
            this.jLabelNewTaskError.setText(this.messageErrorActivityTaken);
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        } catch (ParseException ex) {
            this.jLabelNewTaskError.setText(this.messageErrorDate);
        }
        this.disconnect();
    }

    private void jButtonNewDebtCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToApartmentDetail();
    }

    private void jButtonNewDebtCreateActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }

            String name = this.jTextFieldNewDebtName.getText().trim();
            String description = this.jTextAreaNewDebtDescription.getText().trim();
            double totalCount = Double.parseDouble(this.jTextFieldNewDebtTotalCount.getText());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(Utils.stringToDate(this.jTextFieldNewDebtDate.getText()));
            if (name.equals("")) {
                this.jLabelNewDebtError.setText(this.messageErrorEmpty);
            } else {
                Debt debt = new Debt(totalCount, calendar, name, description, group, null);
                this.moveToApartmentDetail();
            }
        } catch (SQLException ex) {
            this.jLabelNewDebtError.setText(this.messageErrorActivityTaken);
            ex.printStackTrace();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        } catch (NumberFormatException ex) {
            this.jLabelNewDebtError.setText(this.messageErrorPot);
        } catch (ParseException ex) {
            this.jLabelNewDebtError.setText(this.messageErrorDate);
        }
        this.disconnect();
    }

    private void jButtonDutyBackActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToTaskDetail();
    }

    private void jButtonNewPlaceCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToJournalPlaceDetail();
    }

    private void jButtonNewPlaceCreateActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }

            String name = this.jTextFieldNewPlaceName.getText().trim();
            Calendar date = Calendar.getInstance();
            date.setTime(Utils.stringToDate(this.jTextFieldNewPlaceDate.getText()));
            double price = Double.parseDouble(this.jTextFieldNewPlacePrice.getText());
            String address = this.jTextFieldNewPlaceAddress.getText().trim();
            String description = this.jTextArea3.getText().trim();
            String journalDescription = this.jTextArea2.getText().trim();
            JournalPlace journalPlace = journalPlaceList.get(this.indexSelectedActivity);
            if (name.equals("") || address.equals("")) {
                this.jLabelNewPlaceError.setText(this.messageErrorEmpty);
            } else {
                Place place = new Place(description, price, date, address, journalDescription, name, false, journalPlace);
                this.moveToJournalPlaceDetail();
            }
        } catch (SQLException ex) {
            this.jLabelNewPlaceError.setText(this.messageErrorItemTaken);
            ex.printStackTrace();
        } catch (ParseException ex) {
            this.jLabelNewPlaceError.setText(this.messageErrorDate);
        } catch (NumberFormatException ex) {
            this.jLabelNewPlaceError.setText(this.messageErrorPot);
        }
        this.disconnect();
    }

    private void jButtonDebtBackActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToApartmentDetail();
    }

    private void jButtonDebtNewActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToNewCharge();
    }

    private void jButtonDebtModifyActionPerformed(java.awt.event.ActionEvent evt) {
        if(this.jTableDebt.getSelectedRow() != -1)
        this.moveToCharge();
    }

    private void jButtonDebtDeleteActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        int index = this.jTableDebt.getSelectedRow();
        try {
            this.chargeList.get(index).delete();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        this.moveToDebtDetail();
        this.disconnect();
    }

    private void jButtonPlaceBackActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToJournalPlaceDetail();
    }

    private void jButtonNewArticleCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToPurchaseDetail("");
    }

    private void jButtonNewArticleCreateActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            String name = this.jTextFieldNewArticleName.getText().trim();
            String market = this.jTextFieldNewArticleMarket.getText().trim();
            double amount = Double.parseDouble(this.jTextFieldNewArticleAmount.getText());
            double price = Double.parseDouble(this.jTextFieldNewArticlePrice.getText());
            Purchase purchase = purchaseList.get(this.indexSelectedActivity);
            if (name.equals("") || market.equals("")) {
                this.jLabelNewArticleError.setText(this.messageErrorEmpty);
            } else {
                Article article = new Article(name, market, price, amount, false, purchase);
                purchase.setPaid(false);
                this.moveToPurchaseDetail("");
            }
        } catch (SQLException ex) {
            this.jLabelNewArticleError.setText(this.messageErrorItemTaken);
            ex.printStackTrace();
        } catch (NumberFormatException ex) {
            this.jLabelNewArticleError.setText(this.messageErrorPriceAmount);
        }
        this.disconnect();
    }

    private void jButtonNewChargeCancelActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToDebtDetail();
    }

    private void jButtonNewChargeCreateActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            String name = this.jTextFieldNewChargeName.getText().trim();
            double amount = Double.parseDouble(this.jTextFieldNewChargeAmount.getText());
            User user = User.getUserbyNickName((String) this.jComboBoxNewChargeUsers.getSelectedItem());
            Debt debt = debtList.get(this.indexSelectedActivity);
            if (name.equals("")) {
                this.jLabelNewChargeError.setText(this.messageErrorEmpty);
            } else {
                Charge charge = new Charge(false, amount, user, name, debt);
                this.moveToDebtDetail();
            }
        } catch (SQLException ex) {
            this.jLabelNewChargeError.setText(this.messageErrorItemTaken);
            ex.printStackTrace();
        } catch (NumberFormatException ex) {
            this.jLabelNewChargeError.setText(this.messageErrorPriceAmount);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void jComboBoxCreateUsersActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }
            String nickname = (String) this.jComboBoxCreateUsers.getSelectedItem();

            if (nickname.length() > 0) {
                group.addUser(User.getUserbyNickName(nickname));
                this.moveToManagerUser();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.moveToManagerUser();
        this.disconnect();
    }

    private void jButtonManageUserDeleteActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Group group = null;

            if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Apartment")) {
                group = this.controllerGroup.getApartmentList().get(this.controllerGroup.getIndexSelectedGroup());
            } else if (this.controllerGroup.getTypeSelectedGroup().equalsIgnoreCase("Event")) {
                group = this.controllerGroup.getEventList().get(this.controllerGroup.getIndexSelectedGroup());
            } else {
                group = this.controllerGroup.getTripList().get(this.controllerGroup.getIndexSelectedGroup());
            }

            this.indexselectedUser = this.jTableManagerUserTable.getSelectedRow();

            if (this.indexselectedUser >= 0) {
                User user = userList.get(indexselectedUser);
                group.deleteUser(user);
            }

            this.moveToManagerUser();
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void jButtonManageUserBackActionPerformed(java.awt.event.ActionEvent evt) {
        this.moveToGroup(this.controllerGroup.getUserLogin().getName());
    }

    private void jButtonChargeAceptActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Charge charge = chargeList.get(this.indexSelectedItem);
            charge.setPaid(this.jCheckBoxPaid.isSelected());
            this.moveToDebtDetail();
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void jButtonDutyAceptActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Duty duty = dutyList.get(this.indexSelectedItem);
            duty.setFinished(this.jCheckBoxDutyFinished.isSelected());
            this.moveToTaskDetail();
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void jButtonPlaceAceptActionPerformed(java.awt.event.ActionEvent evt) {
        this.connect();
        try {
            Place place = placeList.get(this.indexSelectedItem);
            place.setVisited(this.jCheckBoxPlaceVisited.isSelected());
            this.moveToJournalPlaceDetail();
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.disconnect();
    }

    private void connect() {
        User.setConnection(ConnectionDB.getConnectionDB().getConnection());
        Group.setConnection(ConnectionDB.getConnectionDB().getConnection());
        Activity.setConnection(ConnectionDB.getConnectionDB().getConnection());
        Item.setConnection(ConnectionDB.getConnectionDB().getConnection());
    }

    private void disconnect() {
        try {
            User.closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
