package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import temis.Activity;
import temis.Apartment;
import temis.Article;
import temis.Charge;
import temis.ConnectionDB;
import temis.Debt;
import temis.Duty;
import temis.Event;
import temis.Group;
import temis.Item;
import temis.JournalPlace;
import temis.Place;
import temis.Purchase;
import temis.Task;
import temis.Trip;
import temis.User;
import temis.Utils;

public class GroupController implements ActionListener {

    //MVC
    private TemisView view;
    private String[][] table;
    private String[] head;

    private String typeSelectedGroup;
    private int indexSelectedGroup;

    private List<Apartment> apartmentList = new ArrayList<>();
    private List<Event> eventList = new ArrayList<>();
    private List<Trip> tripList = new ArrayList<>();

    private User userLogin;
    private String language = "spanish";

    public GroupController(TemisView view) {
        this.view = view;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String command = ae.getActionCommand();
        this.connect();
        switch (command) {
            case "jButtonRegister":
                view.moveToRegister(false, "", "", "");
                break;
            case "jButtonLogin":
                try {
                    this.userLogin = null;
                    Map<String, String> map = view.getUserLoginData();
                    String username = map.get("username");
                    String password = map.get("password");
                    this.userLogin = User.getUser(username, password);
                    if (this.userLogin != null) {
                        view.disableMenuButtons();
                        view.moveToGroup(this.userLogin.getName());
                    } else {
                        view.setLoginError(view.messageLoginError);
                    }
                } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
                    ex.printStackTrace();
                }
                break;
            case "jButtonLanguage":
                String path = GroupController.class.getPackage().getName() + ".messages";
                Locale locale = null;
                ResourceBundle bundle;

                if (language.equals("english")) {
                    language = "spanish";
                    locale = Locale.getDefault();
                } else if (language.equals("spanish")) {
                    language = "english";
                    locale = new Locale("en", "US");
                }

                bundle = ResourceBundle.getBundle(path, locale);
                view.changeLanguajeLogin(bundle.getString("cambialenguaje"), bundle.getString("contrasena"),
                        bundle.getString("usuario"),
                        bundle.getString("registro"),
                        bundle.getString("entrar"));

                break;

            case "jButtonRegisterCancel":
                if (view.isRegisterUsernameEnabled()) {
                    view.moveToLogin();
                } else {
                    view.moveToGroup(userLogin.getName());
                }
                break;
            case "jButtonRegisterRegister":
                try {
                    Map<String, String> map = view.getUserRegisterData();
                    String name = map.get("name").trim();
                    String surnames = map.get("surname").trim();
                    String username = map.get("username").trim();
                    String password = map.get("password").trim();
                    String passwordConfirm = map.get("passwordConfirm");

                    if (view.isRegisterUsernameEnabled()) {
                        if (password.trim().equals("") || name.trim().equals("") || surnames.trim().equals("") || username.trim().equals("")) {
                            view.setRegisterError(view.messageErrorEmpty);
                        } else if (password.equals(passwordConfirm)) {
                            User user = new User(name, surnames, username, password);
                            view.moveToLogin();
                        } else {
                            view.setRegisterError(view.messageRegisterErrorMatch);
                        }
                    } else {
                        this.userLogin.setName(name);
                        this.userLogin.setSurnames(surnames);
                        if (password.trim().equals("") || name.trim().equals("") || surnames.trim().equals("") || username.trim().equals("")) {
                            view.setRegisterError(view.messageErrorEmpty);
                        } else if (!password.trim().equals("") && !passwordConfirm.trim().equals("") && password.trim().equals(passwordConfirm.trim())) {
                            this.userLogin.setPassword(password.trim());
                            view.moveToGroup(this.userLogin.getName());
                        } else {
                            view.setRegisterError(view.messageRegisterErrorMatch);
                        }
                    }
                } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException ex) {
                    view.setRegisterError(view.messageRegisterErrorTaken);
                    ex.printStackTrace();
                }
                break;
            case "jButtonGroupsModify":
                int index = view.getSelectedGroupIndex();
                if (this.table[index][0].equals("Apartment")) {
                    this.indexSelectedGroup = calculateIndexGroup(index);
                    this.typeSelectedGroup = "Apartment";
                    view.moveToApartmentDetail();
                } else if (this.table[index][0].equals("Event")) {
                    this.indexSelectedGroup = calculateIndexGroup(index);
                    this.typeSelectedGroup = "Event";
                    //this.moveToEventDetail();
                } else {
                    this.indexSelectedGroup = calculateIndexGroup(index);
                    this.typeSelectedGroup = "Trip";
                    //this.moveToTripDetail();
                }

                view.moveToManagerUser();
                break;
            case "jButtonGroupsDelete":
                index = view.getSelectedGroupIndex();

                if (this.table[index][0].equals("Apartment")) {
                    this.indexSelectedGroup = calculateIndexGroup(index);
                    this.typeSelectedGroup = "Apartment";
                    this.apartmentList.get(this.indexSelectedGroup).delete();
                } else if (this.table[index][0].equals("Event")) {
                    this.indexSelectedGroup = calculateIndexGroup(index);
                    this.typeSelectedGroup = "Event";
                    this.eventList.get(this.indexSelectedGroup).delete();
                } else {
                    this.indexSelectedGroup = calculateIndexGroup(index);
                    this.typeSelectedGroup = "Trip";
                    this.tripList.get(this.indexSelectedGroup).delete();
                }

                view.moveToGroup(this.userLogin.getName());
                break;
            case "jButtonGroupsView":
                index = view.getSelectedGroupIndex();
                if (this.table[index][0].equals("Apartment")) {
                    this.indexSelectedGroup = this.calculateIndexGroup(index);
                    this.typeSelectedGroup = "Apartment";
                    view.moveToApartmentDetail();
                } else if (this.table[index][0].equals("Event")) {
                    this.indexSelectedGroup = this.calculateIndexGroup(index);
                    this.typeSelectedGroup = "Event";
                    //this.moveToEventDetail();
                } else {
                    this.indexSelectedGroup = this.calculateIndexGroup(index);
                    this.typeSelectedGroup = "Trip";
                    //this.moveToTripDetail();
                }
                break;
            case "jComboBoxCreate":
                String selectedItem = view.getCreateSelectedItem();
                if (selectedItem.equalsIgnoreCase("apartment")) {
                    view.moveToApartment();
                } else if (selectedItem.equalsIgnoreCase("event")) {
                    //view.moveToEvent();
                } else {
                    //view.moveToTrip();
                }
                break;
            case "jButtonNewApartmentCreate":
                view.moveToApartment();
                break;
            case "jButtonNewApartmentCancel":
                try {
                    Map<String, String> map = view.getNewApartmentData();
                    String groupName = map.get("name");
                    String address = map.get("address");
                    String description = map.get("description");
                    double pot = Double.parseDouble(map.get("pot"));
                    Apartment apartment = new Apartment(groupName, description, address, pot);
                    apartment.addUser(userLogin);
                    view.moveToGroup(userLogin.getName());
                } catch (SQLException ex) {
                    view.setNewApartmentError(view.messageErrorGroupTaken);
                    ex.printStackTrace();
                } catch (NoSuchAlgorithmException ex) {
                    ex.printStackTrace();
                } catch (InvalidKeySpecException ex) {
                    ex.printStackTrace();
                } catch (NumberFormatException ex) {
                    view.setNewApartmentError(view.messageErrorPot);
                }
                break;
        }
        this.disconnect();
    }

    public void logoutMenu() {
        view.activeButtonLogoutMenu();
    }

    public void profileMenu() {
        if (view.jMenuProfileisActive()) {
            view.moveToRegister(true, this.userLogin.getName(), this.userLogin.getSurnames(), this.userLogin.getUser());
        }
    }

    public void initGroupData() {
        try {
            this.apartmentList = Apartment.getApartmentsByUser(this.userLogin);
            this.eventList = Event.getEventsByUser(this.userLogin);
            this.tripList = Trip.getTripsByUser(this.userLogin);

            this.table = null;
            this.head = new String[3];
            this.head[0] = "Type";
            this.head[1] = "Name";
            this.head[2] = "Pot";

            if (apartmentList.size() > 0 || eventList.size() > 0 || tripList.size() > 0) {
                table = new String[apartmentList.size() + eventList.size() + tripList.size()][3];
            }

            int i = 0;
            for (Apartment group : apartmentList) {
                table[i][0] = "Apartment";
                table[i][1] = group.getName();
                table[i][2] = Double.toString(group.getPot());
                i++;
            }
            for (Event group : eventList) {
                table[i][0] = "Event";
                table[i][1] = group.getName();
                table[i][2] = Double.toString(group.getPot());
                i++;
            }

            for (Trip group : tripList) {
                table[i][0] = "Trip";
                table[i][1] = group.getName();
                table[i][2] = Double.toString(group.getPot());
                i++;

            }
        } catch (SQLException ex) {
            Logger.getLogger(GroupController.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GroupController.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(GroupController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String[][] getTable() {
        return table;
    }

    public String[] getHead() {
        return head;
    }

    private int calculateIndexGroup(int index) {
        int indexList = 0;
        if (index < this.apartmentList.size()) {
            indexList = index;
        } else if (index < (this.apartmentList.size() + this.eventList.size())) {
            indexList = index - this.apartmentList.size();
        } else {
            indexList = index - (this.apartmentList.size() + this.eventList.size());
        }
        return indexList;
    }

    private void connect() {
        User.setConnection(ConnectionDB.getConnectionDB().getConnection());
        Group.setConnection(ConnectionDB.getConnectionDB().getConnection());
        Activity.setConnection(ConnectionDB.getConnectionDB().getConnection());
        Item.setConnection(ConnectionDB.getConnectionDB().getConnection());
    }

    private void disconnect() {
        try {
            User.closeConnection();
        } catch (SQLException ex) {
            Logger.getLogger(TemisView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //end MVC
    public String getTypeSelectedGroup() {
        return typeSelectedGroup;
    }

    public void setTypeSelectedGroup(String typeSelectedGroup) {
        this.typeSelectedGroup = typeSelectedGroup;
    }

    public int getIndexSelectedGroup() {
        return indexSelectedGroup;
    }

    public void setIndexSelectedGroup(int indexSelectedGroup) {
        this.indexSelectedGroup = indexSelectedGroup;
    }

    public List<Apartment> getApartmentList() {
        return apartmentList;
    }

    public void setApartmentList(List<Apartment> apartmentList) {
        this.apartmentList = apartmentList;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    public List<Trip> getTripList() {
        return tripList;
    }

    public void setTripList(List<Trip> tripList) {
        this.tripList = tripList;
    }

    public User getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(User userLogin) {
        this.userLogin = userLogin;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

}
