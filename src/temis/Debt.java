package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a debt a user can have with another one
 */
public class Debt extends Activity {

    private double countTotal;  // The total amount (monetary) of the debt
    private Calendar date;      // The date of the debt payment

    /**
     * Creates a new Debt.
     *
     * <p>
     * The Debt will be retrieved from persistency if it already exists, if not,
     * it will be created and persisted.
     *
     * @param countTotal The total amount (monetary) of the debt
     * @param date The date limit for the payment
     * @param name The name of the Debt
     * @param description The description of the Debt
     * @param group The group this Debt belongs to
     * @param listItem The list of charges of the debt
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Debt(double countTotal, Calendar date, String name, String description, Group group, List<Charge> listItem) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(name, description, group, listItem);

        long id = super.getId();

        String query = "INSERT INTO Debt (id_activity, date_, countTotal) values (?, ?, ?)";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);

        statement.setLong(1, id);
        statement.setDate(2, new Date(date.getTime().getTime()));
        statement.setDouble(3, countTotal);
        statement.execute();

        this.date = date;
        this.countTotal = countTotal;

        statement.close();
    }

    /**
     * Creates a new Debt. The Debt will be retrieved from persistency.
     *
     * @param id The ID of the Debt
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Debt(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);

        String query = "SELECT * from Debt WHERE id_activity = ?";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.date = Calendar.getInstance();

            this.date.setTime(result.getDate("date_"));
            this.countTotal = result.getDouble("countTotal");
            this.getListItem().addAll(Charge.getChargesByActivity(id));
        }
        statement.close();
    }

    /**
     * Get the total amount of the Debt.
     *
     * @return The total amount of the Debt
     */
    public double getCountTotal() {
        return countTotal;
    }

    /**
     * Set the total amount of the Debt. The amount is persisted when set.
     *
     * @param countTotal The total amount of the Debt
     *
     * @throws SQLException
     */
    public void setCountTotal(double countTotal) throws SQLException {
        String query = "UPDATE Debt SET countTotal = ? WHERE id_activity = ?";

        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setDouble(1, countTotal);
        statement.setLong(2, this.getId());
        statement.execute();
        this.countTotal = countTotal;
        statement.close();
    }

    /**
     * Get the date limit of the debt payment.
     *
     * @return The date limit of the debt payment
     */
    public Calendar getDate() {
        return date;
    }

    /**
     * Set the date limit of the debt payment. The date is persisted when set.
     *
     * @param date The date limit of the debt payment
     *
     * @throws SQLException
     */
    public void setDate(Calendar date) throws SQLException {
        String query = "UPDATE Debt SET date_ = ? WHERE id_activity = ?";

        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setDate(1, new Date(date.getTime().getTime()));
        statement.setLong(2, this.getId());
        statement.execute();
        this.date = date;
        statement.close();
    }

    @Override
    public void delete() throws SQLException {

        try {
            List<Charge> itemList = Charge.getChargesByActivity(this.getId());

            for (Charge article : itemList) {
                article.delete();
            }

            String query = "Delete from Debt where id_activity = ?";
            PreparedStatement statement = Activity.getConnection().prepareStatement(query);
            statement.setLong(1, this.getId());
            statement.execute();

            super.delete();
            statement.close();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Purchase.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Purchase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Gets a lisf of Debts given a Group ID.
     *
     * @param id The ID of the group
     *
     * @return A list of Articles to buy/bought from the given Activity
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Debt> getDebtsByGroup(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from Activity a, Debt p WHERE a.id_group = ? and a.id = p.id_activity";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        List<Debt> list = new ArrayList<>();
        while (result.next()) {
            list.add(new Debt(result.getLong("id")));
        }
        statement.close();
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Debt{" + "countTotal=" + countTotal + ", date=" + date + '}';
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.countTotal) ^ (Double.doubleToLongBits(this.countTotal) >>> 32));
        hash = 59 * hash + Objects.hashCode(this.date);
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Debt other = (Debt) obj;
        if (Double.doubleToLongBits(this.countTotal) != Double.doubleToLongBits(other.countTotal)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

}
