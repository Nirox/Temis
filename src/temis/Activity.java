package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an activity that should be performed by a group of Users.
 *
 * Activities have a list of items that should be done.
 *
 * @param <E> A type of Item for the Activity list of Items
 */
public class Activity<E extends Item> {

    private String name;                   // The name of this activity
    private String description;            // The description of this activity
    private long id;                       // The ID of this Class in persistency
    private Group group;                   // The group of users this Activity belongs
    private List<E> listItem = null;       // The list of items that should be done to finalize this activity
    private static Connection connection;  // The database connection used to persist this Class

    /**
     * Creates a new Activity.
     *
     * <p>
     * The Activity will be retrieved from persistency if it already exists, if
     * not, it will be created and persisted.
     *
     * @param name The name of the activity
     * @param description The description of the activity
     * @param group The group this activity belongs to
     * @param listItem The list of items belonging to this activity
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Activity(String name, String description, Group group, List<E> listItem) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {

        this.id = 0L;
        String query = "select ACTIVITYSECUENCE.NEXTVAL from dual";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        ResultSet result = statement.executeQuery();
        if (result.next()) {
            this.id = result.getLong(1);
        }

        query = "INSERT INTO Activity (name, description, id_group, id) values (?, ?, ?, ?)";

        statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setLong(3, group.getId());
        statement.setLong(4, this.id);
        statement.execute();

        this.name = name;
        this.description = description;
        this.group = group;
        this.listItem = new ArrayList<>();
        statement.close();

    }

    /**
     * Creates a new Activity. The activity will be retrieved from persistency.
     *
     * @param id The ID of the activity in the persistency
     *
     * @throws SQLException
     */
    public Activity(long id) throws SQLException {
        String query = "SELECT * from Activity where id = ?";

        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.name = result.getString("name");
            this.description = result.getString("description");
            this.id = result.getLong("id");
            this.listItem = new ArrayList<>();
        }
        statement.close();

    }

    /**
     * Get the Activity name.
     *
     * @return The Activity name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the Activity name. The name is persisted when set.
     *
     * @param name The Activity name
     *
     * @throws SQLException
     */
    public void setName(String name) throws SQLException {
        String query = "UPDATE Activity SET name = ? WHERE id = ?";

        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setLong(2, this.getId());
        statement.execute();
        this.name = name;
        statement.close();
    }

    /**
     * Get the Activity description.
     *
     * @return The Activity description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the Activity description. The description is persisted when set.
     *
     * @param description The Activity description
     *
     * @throws SQLException
     */
    public void setDescription(String description) throws SQLException {
        String query = "UPDATE Activity SET description = ? WHERE id = ?";

        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setString(1, description);
        statement.setLong(2, this.getId());
        statement.execute();
        this.description = description;
        statement.close();
    }

    /**
     * Get the Activity ID.
     *
     * @return The Activity ID
     */
    public long getId() {
        return id;
    }

    /**
     * Get the Activity group.
     *
     * @return The Activity group
     */
    public Group getGroup() {
        return group;
    }

    /**
     * Set the Activity group.
     *
     * @param group The Activity group
     *
     * @throws SQLException
     */
    public void setGroup(Group group) throws SQLException {
        String query = "UPDATE Activity SET id_group = ? WHERE id = ?";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);

        statement.setLong(1, group.getId());
        statement.setLong(1, this.id);
        statement.execute();

        this.group = group;
        statement.close();
    }

    /**
     * Get the Activity list of Items.
     *
     * @return The Activity list of Items
     */
    public List<E> getListItem() {
        return listItem;
    }

    /**
     * Adds an Item to the Activity.
     *
     * @param item The item to be added to the Activity
     */
    public void addItem(E item) {
        this.listItem.add(item);
    }

    /**
     * Removes an Item from the Activity.
     *
     * @param item The item to be removed from the Activity
     */
    public void deleteItem(E item) {
        listItem.remove(item);
    }

    /**
     * Get the list of activities from persistency belonging to a Group.
     *
     * @param activity The ID of the activity to retrieve
     *
     * @return A list of activities belonging to the Group
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Activity> getActivitiesByGroup(long activity) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from Activity WHERE id_group = ?";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, activity);
        ResultSet result = statement.executeQuery();

        List<Activity> list = new ArrayList<>();
        while (result.next()) {
            list.add(new Activity(result.getLong("id")));
        }
        statement.close();
        return list;
    }

    /**
     * Sets the connection to the persistency
     *
     * @param connection The connection to the persistency
     */
    public static void setConnection(Connection connection) {
        Activity.connection = connection;
    }

    /**
     * Get the connection to the persistency.
     *
     * @return The connection to the persistency
     */
    public static Connection getConnection() {
        return Activity.connection;
    }

    public void delete() throws SQLException {

        String query = "Delete from Activity where id = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, this.id);
        statement.execute();
        statement.close();
    }

    /**
     * Closes connection to the persistency.
     *
     * @throws SQLException
     */
    public static void closeConnection() throws SQLException {
        if (!Activity.connection.isClosed()) {
            Activity.connection.close();
            System.err.println("Connection closed");
        }
    }

}
