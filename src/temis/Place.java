package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

/**
 * Represents a place that can be visited.
 */
public class Place extends Item {

    private String description;         // The description of the place
    private double price;               // The price to visit the place
    private Calendar date;              // The date  to visit the place
    private String address;             // The address     of the place
    private String journalDescription;  // The events that happend on the place
    private boolean visited;            // A flag indicating if the place has been visited

    /**
     * Creates a new Place.
     *
     * <p>
     * The Place will be retrieved from persistency if it already exists, if
     * not, it will be created and persisted.
     *
     * @param description The description of the Place
     * @param price The price to visit the Place
     * @param date The date to visit the Place
     * @param address The address of the Place
     * @param journalDescription The events that happend on the Place
     * @param name The name of the Place
     * @param visited A flag indicating if the place has been visited
     * @param activity The Activity this Place belongs to
     *
     * @throws SQLException
     */
    public Place(String description, double price, Calendar date, String address, String journalDescription, String name, boolean visited, Activity activity) throws SQLException {
        super(name, activity);

        long id = super.getId();

        String query = "INSERT INTO Place (id_item, description, price, date_, address, journal_Description, visited) values (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);

        statement.setLong(1, id);
        statement.setString(2, description);
        statement.setDouble(3, price);
        statement.setDate(4, new Date(date.getTime().getTime()));
        statement.setString(5, address);
        statement.setString(6, journalDescription);
        statement.setString(7, (visited ? "1" : "0"));
        statement.execute();

        this.description = description;
        this.price = price;
        this.date = date;
        this.address = address;
        this.journalDescription = journalDescription;
        this.visited = visited;

        statement.close();
    }

    /**
     * Creates a new Place. The Place will be retrieved from persistency.
     *
     * @param id The ID of the Place in the persistency
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Place(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);
        String query = "SELECT * from Place WHERE id_item = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.date = Calendar.getInstance();

            this.date.setTime(result.getDate("date_"));
            this.price = result.getDouble("price");
            this.description = result.getString("description");
            this.address = result.getString("address");
            this.journalDescription = result.getString("journal_Description");
            this.visited = result.getString("visited").equals("1");
        }
        statement.close();
    }

    /**
     * Get the Place description.
     *
     * @return The Place description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the Place description. The description is persisted when set.
     *
     * @param description The Place description
     *
     * @throws SQLException
     */
    public void setDescription(String description) throws SQLException {
        String query = "UPDATE Place SET description = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, description);
        statement.setLong(2, this.getId());
        statement.execute();
        this.description = description;
        statement.close();
    }

    /**
     * Get the price to visit the Place.
     *
     * @return The price to visit the Place
     */
    public double getPrice() {
        return price;
    }

    /**
     * Set the price to visit the Place. The price is persisted when set.
     *
     * @param price The price to visit the Place
     *
     * @throws SQLException
     */
    public void setPrice(double price) throws SQLException {
        String query = "UPDATE Place SET price = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setDouble(1, price);
        statement.setLong(2, this.getId());
        statement.execute();
        this.price = price;
        statement.close();
    }

    /**
     * Get the date to visit the Place.
     *
     * @return The date to visit the Place
     */
    public Calendar getDate() {
        return date;
    }

    /**
     * Set the date to visit the Place. The date is persisted when set.
     *
     * @param date The date to visit the Place
     *
     * @throws SQLException
     */
    public void setDate(Calendar date) throws SQLException {
        String query = "UPDATE Place SET date_ = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setDate(1, (Date) date.getTime());
        statement.setLong(2, this.getId());
        statement.execute();
        this.date = date;
        statement.close();
    }

    /**
     * Get the Place address.
     *
     * @return The Place address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Set the Place address. The address is persisted when set.
     *
     * @param address The Place address
     *
     * @throws SQLException
     */
    public void setAddress(String address) throws SQLException {
        String query = "UPDATE Place SET address = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, address);
        statement.setLong(2, this.getId());
        statement.execute();
        this.address = address;
        statement.close();
    }

    /**
     * Get the Place journal.
     *
     * @return The Place journal
     */
    public String getJournalDescription() {
        return journalDescription;
    }

    /**
     * Set the Place journal. The journal is persisted when set.
     *
     * @param journalDescription The Place journal
     *
     * @throws SQLException
     */
    public void setJournalDescription(String journalDescription) throws SQLException {
        String query = "UPDATE Place SET journal_Description = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, journalDescription);
        statement.setLong(2, this.getId());
        statement.execute();
        this.journalDescription = journalDescription;
        statement.close();
    }

    /**
     * Checks whether the place has been visited.
     *
     * @return {@code True} if it has been visited, {@code False} otherwise.
     */
    public boolean isVisited() {
        return visited;
    }

    /**
     * Sets whether the place has been visited. The flag is persisted when set.
     *
     * @param visited {@code True} if it has been visited, {@code False}
     * otherwise.
     *
     * @throws SQLException
     */
    public void setVisited(boolean visited) throws SQLException {
        String query = "UPDATE Place SET visited = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, (visited ? "1" : "0"));
        statement.setLong(2, this.getId());
        statement.execute();
        this.visited = visited;
        statement.close();
    }

    public void delete() throws SQLException {
        String query = "Delete from Place where id_item = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, this.getId());
        statement.execute();
        super.delete();
        statement.close();
    }

    /**
     * Get a list of Places given an Activity ID.
     *
     * @param activity The ID of the Activity to retrieve Places from
     *
     * @return A list of Places belonging to the Activity
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Place> getPlacesByActivity(long activity) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from item WHERE id_activity = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, activity);
        ResultSet result = statement.executeQuery();

        List<Place> list = new ArrayList<>();
        while (result.next()) {
            list.add(new Place(result.getLong("id")));
        }
        statement.close();
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Place{" + "name=" + this.getName() + "description=" + description + ", price=" + price + ", date=" + date + ", address=" + address + ", journalDescription=" + journalDescription + ", visited=" + visited + '}';
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.description);
        hash = 41 * hash + (int) (Double.doubleToLongBits(this.price) ^ (Double.doubleToLongBits(this.price) >>> 32));
        hash = 41 * hash + Objects.hashCode(this.date);
        hash = 41 * hash + Objects.hashCode(this.address);
        hash = 41 * hash + Objects.hashCode(this.journalDescription);
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Place other = (Place) obj;
        if (Double.doubleToLongBits(this.price) != Double.doubleToLongBits(other.price)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.journalDescription, other.journalDescription)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }

        if (!Objects.equals(this.getName(), other.getName())) {
            return false;
        }

        if (!Objects.equals(this.getId(), other.getId())) {
            return false;
        }

        if (!Objects.equals(this.getName(), other.getName())) {
            return false;
        }

        if (!Objects.equals(this.getId(), other.getId())) {
            return false;
        }
        return true;

    }
}
