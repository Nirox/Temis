package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a Travel plan.
 *
 * The travel may have a list of places to visit.
 */
public class JournalPlace extends Activity {

    private Calendar endDate;  // The end date of the travel

    /**
     * Creates a new JournalPlace.
     *
     * <p>
     * The JournalPlace will be retrieved from persistency if it already exists,
     * if not, it will be created and persisted.
     *
     * @param endDate The date the travel ends
     * @param name The name of the Travel
     * @param description The description of this travel
     * @param group The group that is part of this travel
     * @param listItem The list of places to visit
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public JournalPlace(Calendar endDate, String name, String description, Group group, List<Place> listItem) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(name, description, group, listItem);
        long id = super.getId();

        String query = "INSERT INTO journal_place (id_activity, endDate) values (?, ?)";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);

        statement.setLong(1, id);
        statement.setDate(2, new Date(endDate.getTime().getTime()));
        statement.execute();

        this.endDate = endDate;

        statement.close();
    }

    /**
     * Creates a new JournalPlace. The JournalPlace will be retrieved from
     * persistency.
     *
     * @param id The ID of the JournalPlace
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public JournalPlace(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);

        String query = "SELECT * from journal_place WHERE id_activity = ?";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.endDate = Calendar.getInstance();

            this.endDate.setTime(result.getDate("endDate"));
            this.getListItem().addAll(Charge.getChargesByActivity(id));
        }
        statement.close();
    }

    /**
     * Get the end date of the travel.
     *
     * @return The end date of the travel
     */
    public Calendar getEndDate() {
        return endDate;
    }

    /**
     * Set the end data of the travel. The date is persisted when set.
     *
     * @param endDate The end data of the travel
     *
     * @throws SQLException
     */
    public void setEndDate(Calendar endDate) throws SQLException {
        String query = "UPDATE Journal_Place SET endDate = ? WHERE id_activity = ?";

        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setDate(1, (Date) endDate.getTime());
        statement.setLong(2, this.getId());
        statement.execute();
        this.endDate = endDate;
        statement.close();
    }

    /**
     * Gets a lisf of travels given a Group ID.
     *
     * @param id The Group ID to retrieve the travels
     *
     * @return A list of travels from the Group
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<JournalPlace> getJournalPlacesByGroup(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from Activity a, Journal_Place p WHERE a.id_group = ? and a.id = p.id_activity";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        List<JournalPlace> list = new ArrayList<>();
        while (result.next()) {
            list.add(new JournalPlace(result.getLong("id")));
        }
        statement.close();
        return list;
    }

    public void delete() throws SQLException {

        try {
            List<Place> itemList = Place.getPlacesByActivity(this.getId());

            for (Place place : itemList) {
                place.delete();
            }

            String query = "Delete from Journal_place where id_activity = ?";
            PreparedStatement statement = Activity.getConnection().prepareStatement(query);
            statement.setLong(1, this.getId());
            statement.execute();
            super.delete();
            statement.close();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Purchase.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Purchase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "JournalPlace{" + "endDate=" + endDate + '}';
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.endDate);
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JournalPlace other = (JournalPlace) obj;
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        return true;
    }

}
