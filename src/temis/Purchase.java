package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a purchase of a group of articles.
 */
public class Purchase extends Activity {

    private boolean paid;  // The flag indicating if this purchase has been paid

    /**
     * Creates a new Purchase.
     *
     * <p>
     * The Purchase will be retrieved from persistency if it already exists, if
     * not, it will be created and persisted.
     *
     * @param paid The flag indicating if this Purchase has been paid
     * @param name The name of the Purchase
     * @param description The description of the Purchase
     * @param group The Group this Purchase if part of
     * @param listItem The list of items (Articles) of this Purchase
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Purchase(boolean paid, String name, String description, Group group, List<Article> listItem) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(name, description, group, listItem);

        long id = super.getId();

        String query = "INSERT INTO Purchase (id_activity, paid) values (?, ?)";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);

        statement.setLong(1, id);
        statement.setString(2, (paid ? "1" : "0"));
        statement.execute();

        this.paid = paid;

        statement.close();
    }

    /**
     * Creates a new Purchase. The Purchase will be retrieved from persistency.
     *
     * @param id The ID of the Purchase in the persistency
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Purchase(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);

        String query = "SELECT * from Purchase WHERE id_activity = ?";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.paid = result.getString("paid").equals("1");
            this.getListItem().addAll(Activity.getActivitiesByGroup(id));
        }
        statement.close();

    }

    /**
     * Checks whether the Purchase has been paid.
     *
     * @return {@code True} if it has been paid, {@code False} otherwise.
     */
    public boolean isPaid() {
        return paid;
    }

    /**
     * Sets whether the Purchase has been paid. The flag is persisted when set.
     *
     * @param paid {@code True} if it has been paid, {@code False} otherwise.
     *
     * @throws SQLException
     */
    public void setPaid(boolean paid) throws SQLException {
        String query = "UPDATE Purchase SET paid = ? WHERE id_activity = ?";

        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setString(1, (paid ? "1" : "0"));
        statement.setLong(2, this.getId());
        statement.execute();
        this.paid = paid;
        statement.close();
    }

    @Override
    public void delete() throws SQLException {

        try {
            List<Article> itemList = Article.getArticlesByActivity(this.getId());

            for (Article article : itemList) {
                article.delete();
            }

            String query = "Delete from Purchase where id_activity = ?";
            PreparedStatement statement = Activity.getConnection().prepareStatement(query);
            statement.setLong(1, this.getId());
            statement.execute();

            super.delete();
            statement.close();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Purchase.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Purchase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Get a list of Purchases given an Group ID.
     *
     * @param id The ID of the Group to retrieve Purchases from
     *
     * @return A list of Purchases belonging to the Group
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Purchase> getPurchasesByGroup(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from Activity a, Purchase p WHERE a.id_group = ? and a.id = p.id_activity";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        List<Purchase> list = new ArrayList<>();
        while (result.next()) {
            list.add(new Purchase(result.getLong("id")));
        }
        statement.close();
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Purchase{" + "paid=" + paid + '}';
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.paid ? 1 : 0);
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Purchase other = (Purchase) obj;
        if (this.paid != other.paid) {
            return false;
        }
        return true;
    }

}
