package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a group of Users
 *
 * Groups have a list of Users and Activities
 */
public class Group {

    private String name;                       // The name of the Group
    private double pot;                        // The amount of money this group shares
    private String description;                // The Group description
    private List<User> users = null;           // The list of Users that are part of this Group
    private List<Activity> activities = null;  // The list of Activities of the Group
    private long id;                           // The ID of the Group
    private static Connection connection;      // The database connection used to persist this Class

    /**
     * Creates a new Group.
     *
     * <p>
     * The Group will be retrieved from persistency if it already exists, if
     * not, it will be created and persisted.
     *
     * @param name The name of the Group
     * @param pot The amount of money this group shares
     * @param description The Group description
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    protected Group(String name, double pot, String description) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {

        String query = "select GROUPSECUENCE.NEXTVAL from dual";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.id = result.getLong(1);
        }

        query = "INSERT INTO group_ (name, description, pot, id) values (?,?,?,?)";

        statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setDouble(3, pot);
        statement.setLong(4, this.id);
        statement.execute();

        this.name = name;
        this.description = description;
        this.pot = pot;
        this.activities = new ArrayList<>();
        this.users = new ArrayList<>();

        statement.close();
    }

    /**
     * Creates a new Group. The Group will be retrieved from persistency.
     *
     * @param id The ID of the Group
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Group(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT * from Group_ ap where ap.id = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.id = result.getLong("id");
            this.name = result.getString("name");
            this.description = result.getString("description");
            this.pot = result.getDouble("pot");
            User.setConnection(connection);
            Activity.setConnection(connection);
            this.users = User.getUsersByGroup(this.id);
            this.activities = Activity.getActivitiesByGroup(this.id);
        }
        statement.close();
    }

    /**
     * Get the name of the Group.
     *
     * @return The name of the Group
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the Group. The name is persisted when set.
     *
     * @param name The name of the Group
     *
     * @throws SQLException
     */
    public void setName(String name) throws SQLException {
        String query = "UPDATE Group_ SET name = ? WHERE id = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setLong(2, this.getId());
        statement.execute();

        this.name = name;
        statement.close();
    }

    /**
     * Get the money shared by the Group.
     *
     * @return The money shared by the Group
     */
    public double getPot() {
        return pot;
    }

    /**
     * Set the money shared by the Group. The value is persisted when set.
     *
     * @param pot The money shared by the Group
     *
     * @throws SQLException
     */
    public void setPot(double pot) throws SQLException {
        String query = "UPDATE Group_ SET pot = ? WHERE id = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setDouble(1, pot);
        statement.setLong(2, this.getId());
        statement.execute();

        this.pot = pot;
        statement.close();
    }

    /**
     * Get the Group description.
     *
     * @return The Group description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the Group description. The description is persisted when set.
     *
     * @param description The description of the Group
     *
     * @throws SQLException
     */
    public void setDescription(String description) throws SQLException {
        String query = "UPDATE Group_ SET description = ? WHERE id = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, description);
        statement.setLong(2, this.getId());
        statement.execute();

        this.description = description;
        statement.close();
    }

    /**
     * Get the Group ID.
     *
     * @return The Group ID
     */
    public long getId() {
        return id;
    }

    /**
     * Set Group database connection.
     *
     * @param connection The database connection
     */
    public static void setConnection(Connection connection) {
        Group.connection = connection;
    }

    /**
     * Get the Group database connection.
     *
     * @return The Group database connection
     */
    public static Connection getConnection() {
        return Group.connection;
    }

    /**
     * Closes the database connection
     *
     * @throws SQLException
     */
    public static void closeConnection() throws SQLException {
        if (!Group.connection.isClosed()) {
            Group.connection.close();
            System.err.println("Connection closed");
        }
    }

    /**
     * Get a list of users that are part of this Group.
     *
     * @return The list of users that are part of this Group
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * Adds a user to this Group.
     *
     * @param user The User to be added
     *
     * @throws SQLException
     */
    public void addUser(User user) throws SQLException {
        String query = "INSERT INTO User_Group (id_user,id_group) values (?, ?)";
        PreparedStatement statement = Group.getConnection().prepareStatement(query);

        statement.setLong(1, user.getId());
        statement.setLong(2, this.id);
        statement.execute();

        this.users.add(user);
        statement.close();
    }

    /**
     * Get a list of Groups that contain a User.
     *
     * @param user The User to use on the search
     *
     * @return A list of Groups that contain a User
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Group> getGroupsByUser(User user) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT * from User_Group WHERE id_user = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, user.getId());
        ResultSet result = statement.executeQuery();

        List<Group> list = new ArrayList<>();

        while (result.next()) {
            list.add(new Group(result.getLong("id_group")));
        }
        statement.close();
        return list;
    }

    /**
     * Removes a user from this Group.
     *
     * @param user The User to be removed
     */
    public void deleteUser(User user) throws SQLException {
        String query = "Delete from User_Group where id_group = ? and id_user = ?";
        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, this.getId());
        statement.setLong(2, user.getId());
        statement.execute();

        users.remove(user);
        statement.close();
    }

    /**
     * Get a list of Activities of this Group.
     *
     * @return The list of Activities of this Group
     */
    public List<Activity> getActivities() {
        return activities;
    }

    /**
     * Adds an Activity to this Group.
     *
     * @param activity The Activity to be added
     */
    public void addActivity(Activity activity) {
        this.activities.add(activity);
    }

    /**
     * Removes an Activity from this Group.
     *
     * @param activity The Activity to be removed
     */
    public void deleteActivity(Activity activity) {
        activities.remove(activity);
    }

    public void delete() throws SQLException {
        String query = "Delete from User_Group where id_group = ?";
        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, this.getId());
        statement.execute();

        query = "Delete from Group_ where id = ?";
        statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, this.getId());
        statement.execute();
        statement.close();

    }

}
