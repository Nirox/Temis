package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a Task that should be done by a Group.
 *
 * <p>
 * Tasks have a list of Duties to accomplish.
 */
public class Task extends Activity {

    private boolean finished;  // Flag that indicates if the Task has been finished
    private Calendar endDate;  // A date limit to accomplish the Task

    /**
     * Creates a new Task.
     *
     * <p>
     * The aleatory User asigned Activity Item will be retrieved from
     * persistency if it already exists, if not, it will be created and
     * persisted.
     *
     * @param finished The flag that indicates if the Task has been finished
     * @param endDate The date limit to accomplish the Task
     * @param name The name of the Task
     * @param description The description of the Task
     * @param group The Group this Task belongs to
     * @param listItem The list of Duties that form this Task
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Task(boolean finished, Calendar endDate, String name, String description, Group group, List<Duty> listItem) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(name, description, group, listItem);
        long id = super.getId();

        String query = "INSERT INTO TAsk (id_activity, endDate, finished) values (?, ?, ?)";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);

        statement.setLong(1, id);
        statement.setDate(2, new Date(endDate.getTime().getTime()));
        statement.setString(3, (finished ? "1" : "0"));
        statement.execute();

        this.endDate = endDate;
        this.finished = finished;

        statement.close();
    }

    /**
     * Creates a new Task. The Task will be retrieved from persistency.
     *
     * @param id The ID of the Task in the persistency
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Task(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);
        String query = "SELECT * from Task WHERE id_activity = ?";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.endDate = Calendar.getInstance();

            this.endDate.setTime(result.getDate("endDate"));
            this.finished = result.getString("finished").equals("1");
            this.getListItem().addAll(Duty.getDutiesByActivity(id));
        }
        statement.close();
    }

    /**
     * Checks whether the Task has been finished.
     *
     * @return {@code True} if the Task has been finished, False otherwise
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * Get the date limit to accomplish this Task.
     *
     * @return The date limit to accomplish this Task
     */
    public Calendar getEndDate() {
        return endDate;
    }

    /**
     * Set the date limit to accomplish this Task. The date is persisted when
     * set.
     *
     * @param endDate The date limit to accomplish this Task
     *
     * @throws SQLException
     */
    public void setEndDate(Calendar endDate) throws SQLException {
        String query = "UPDATE Task SET endDate = ? WHERE id_activity = ?";

        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setDate(1, (Date) endDate.getTime());
        statement.setLong(2, this.getId());
        statement.execute();
        this.endDate = endDate;
        statement.close();
    }

    /**
     * Gets a lisf of Taks given an Group ID.
     *
     * @param id The Group ID to retrieve the Tasks from
     *
     * @return A list of Tasks belonging to the Group
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Task> getTasksByGroup(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from Activity a, Task p WHERE a.id_group = ? and a.id = p.id_activity";
        PreparedStatement statement = Activity.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        List<Task> list = new ArrayList<>();
        while (result.next()) {
            list.add(new Task(result.getLong("id")));
        }
        statement.close();
        return list;
    }

    public void delete() throws SQLException {

        try {
            List<Duty> itemList = Duty.getDutiesByActivity(this.getId());

            for (Duty duty : itemList) {
                duty.delete();
            }

            String query = "Delete from Task where id_activity = ?";
            PreparedStatement statement = Activity.getConnection().prepareStatement(query);
            statement.setLong(1, this.getId());
            statement.execute();

            super.delete();
            statement.close();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Purchase.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Purchase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.finished ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.endDate);
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Task other = (Task) obj;
        if (this.finished != other.finished) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Task{" + "finished=" + finished + ", endDate=" + endDate + '}';
    }

}
