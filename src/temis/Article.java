package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents an Item that should be bought
 *
 * After buying the Item, the user would set the place and price of it.
 */
public class Article extends Item {

    private String market;  // The name  of the market from where the Item was bought
    private double price;   // The price of the Item        that           was bought
    private double amount;  // The amount of the Item       that           was bought
    private boolean paid;   // The paid of the Item       that           was bought

    /**
     * Creates a new Article.
     *
     * <p>
     * The Article will be retrieved from persistency if it already exists, if
     * not, it will be created and persisted.
     *
     * @param name The name of the Item that should be/was bought
     * @param market The name of the market from where the Item was bought
     * @param price The price of the item
     * @param amount The amount of the item
     * @param paid The paid of the item
     * @param activity The activity this Article belongs to
     *
     * @throws SQLException
     */
    public Article(String name, String market, double price, double amount, boolean paid, Activity activity) throws SQLException {
        super(name, activity);

        long id = super.getId();

        String query = "INSERT INTO Article (id_item, market, price, amount, paid) values (?, ?, ?, ?,?)";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);

        statement.setLong(1, id);
        statement.setString(2, market);
        statement.setDouble(3, price);
        statement.setDouble(4, amount);
        statement.setString(5, (paid ? "1" : "0"));
        statement.execute();

        this.market = market;
        this.price = price;

        statement.close();
    }

    /**
     * Creates a new Article. The Article will be retrieved from persistency.
     *
     * @param id The ID of the Article
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Article(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);
        String query = "SELECT * from Article WHERE id_item = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.market = result.getString("market");
            this.price = result.getDouble("price");
            this.amount = result.getDouble("amount");
            this.paid = result.getString("paid").equals("1");
        }
        statement.close();
    }

    /**
     * Get the name of the market from where the Article was bought.
     *
     * @return The name of the market
     */
    public String getMarket() {
        return market;
    }

    /**
     * Set the name of the market from where the Article was bought. The name is
     * persisted when set.
     *
     * @param market The name of the market
     *
     * @throws SQLException
     */
    public void setMarket(String market) throws SQLException {
        String query = "UPDATE Article SET market = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, market);
        statement.setLong(2, this.getId());
        statement.execute();

        this.market = market;
        statement.close();
    }

    /**
     * Get the price of the Article.
     *
     * @return The price of the Article
     */
    public double getPrice() {
        return price;
    }

    /**
     * Set the price of the Article. The price is persisted when set.
     *
     * @param price The price of the Article
     *
     * @throws SQLException
     */
    public void setPrice(double price) throws SQLException {
        String query = "UPDATE Article SET price = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setDouble(1, price);
        statement.setLong(2, this.getId());
        statement.execute();
        this.price = price;
        statement.close();
    }

    /**
     * Set the price of the Article. The amount is persisted when set.
     *
     * @param amount The amount of the Article
     *
     * @throws SQLException
     */
    public void setAmount(double amount) throws SQLException {
        String query = "UPDATE Article SET amount = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setDouble(1, amount);
        statement.setLong(2, this.getId());
        statement.execute();
        this.amount = amount;
        statement.close();
    }

    /**
     * Get the amount of the Article.
     *
     * @return The amount of the Article
     */
    public double getAmount() {
        return this.amount;
    }

    /**
     * Set the paid of the Article. The paid is persisted when set.
     *
     * @param paid The amount of the Article
     *
     * @throws SQLException
     */
    public void setPaid(boolean paid) throws SQLException {
        String query = "UPDATE Article SET paid = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, (paid ? "1" : "0"));
        statement.setLong(2, this.getId());
        statement.execute();
        this.paid = paid;
        statement.close();
    }

    /**
     * Get the paid of the Article.
     *
     * @return The paid of the Article
     */
    public boolean getPaid() {
        return this.paid;
    }

    public void delete() throws SQLException {
        String query = "Delete from Article where id_item = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, this.getId());
        statement.execute();
        super.delete();
        statement.close();
    }

    /**
     * Gets a lisf of Articles to buy/bought given an Activity ID.
     *
     * @param activity The Activity ID to retrieve the Articles
     *
     * @return A list of Articles to buy/bought from the given Activity
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Article> getArticlesByActivity(long activity) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from item WHERE id_activity = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, activity);
        ResultSet result = statement.executeQuery();

        List<Article> list = new ArrayList<>();
        while (result.next()) {
            list.add(new Article(result.getLong("id")));
        }
        statement.close();
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.market);
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.price) ^ (Double.doubleToLongBits(this.price) >>> 32));
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Article other = (Article) obj;
        if (Double.doubleToLongBits(this.price) != Double.doubleToLongBits(other.price)) {
            return false;
        }
        if (!Objects.equals(this.market, other.market)) {
            return false;
        }

        if (!Objects.equals(this.getName(), other.getName())) {
            return false;
        }

        if (!Objects.equals(this.getId(), other.getId())) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Article{" + "name=" + this.getName() + "market=" + market + ", price=" + price + ", id=" + this.getId() + '}';
    }

}
