package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a User
 */
public class User {

    private String name;                   // The User name
    private String surnames;               // The User surnames
    private String user;                   // The User username
    private long id;                       // The User ID
    private static Connection connection;  // The persistency connection used by the User

    /**
     * Creates a new User.
     *
     * <p>
     * The User will be retrieved from persistency if it already exists, if not,
     * it will be created and persisted.
     *
     * @param name The User name
     * @param surnames The User surnames
     * @param user The User username
     * @param password The user password
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public User(String name, String surnames, String user, String password) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {

        String query = "select USERSECUENCE.NEXTVAL from dual";
        PreparedStatement statement = User.getConnection().prepareStatement(query);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.id = result.getLong(1);
        }
        query = "INSERT INTO User_ (id, name, surname, username, password) values (?, ?, ?, ?, ?)";

        statement = User.getConnection().prepareStatement(query);
        statement.setLong(1, this.id);
        statement.setString(2, name);
        statement.setString(3, surnames);
        statement.setString(4, user);
        statement.setString(5, Utils.generateStorngPasswordHash(password));
        statement.execute();

        this.name = name;
        this.surnames = surnames;
        this.user = user;

        statement.close();
    }

    /**
     * Creates a new User. The User will be retrieved from persistency.
     *
     * @param id The ID of User in the persistency
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public User(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT * from User_ WHERE id = ?";
        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.id = result.getLong("id");
            this.name = result.getString("name");
            this.surnames = result.getString("surname");
            this.user = result.getString("username");
        }
        statement.close();
    }

    /**
     * Retrieves a User from persistency.
     *
     * @param user The user username
     * @param password The user password
     *
     * @return The User if it exists, {@code null} otherwise
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static User getUser(String user, String password) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        User userObject = null;

        String query = "SELECT id, password from User_ WHERE username = ?";
        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setString(1, user);
        ResultSet result = statement.executeQuery();

        if (result.next() && Utils.validatePassword(password, result.getString("password"))) {
            userObject = new User(result.getLong("id"));
        }
        statement.close();
        return userObject;
    }

    /**
     * Get the User name.
     *
     * @return The User name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the user name. The name is persisted when set.
     *
     * @param name The user name
     *
     * @throws SQLException
     */
    public void setName(String name) throws SQLException {

        String query = "UPDATE User_ SET name = ? WHERE id = ?";

        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setLong(2, this.getId());
        statement.execute();

        this.name = name;
        statement.close();
    }

    /**
     * Get the User surnames.
     *
     * @return The User surnames
     */
    public String getSurnames() {
        return surnames;
    }

    /**
     * Set the user surnames. The surname is persisted when set.
     *
     * @param surnames The user surnames
     *
     * @throws SQLException
     */
    public void setSurnames(String surnames) throws SQLException {

        String query = "UPDATE User_ SET surname = ? WHERE id = ?";

        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setString(1, surnames);
        statement.setLong(2, this.getId());
        statement.execute();

        this.surnames = surnames;
        statement.close();
    }

    /**
     * Get the User username.
     *
     * @return The User username
     */
    public String getUser() {
        return user;
    }

    /**
     * Set the user username. The username is persisted when set.
     *
     * @param user The user username
     *
     * @throws SQLException
     */
    public void setUser(String user) throws SQLException {
        String query = "UPDATE User_ SET username = ? WHERE id = ?";

        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setString(1, user);
        statement.setLong(2, this.getId());
        statement.execute();

        this.user = user;
        statement.close();
    }

    /**
     * Get the User ID.
     *
     * @return The User ID
     */
    public long getId() {
        return this.id;
    }

    /**
     * Set the user password. The password is persisted when set.
     *
     * @param newPassword The new user password
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public void setPassword(String newPassword) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "UPDATE User_ SET password = ? WHERE id = ?";

        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setString(1, Utils.generateStorngPasswordHash(newPassword));
        statement.setLong(2, this.getId());
        statement.execute();
        statement.close();

    }

    /**
     * Deletes the user.
     *
     * @param password The User password
     */
    public void deleteUser(String password) {

    }

    /**
     * Checks whether a password is this User's password.
     *
     * @param password The password to check
     *
     * @return {@code True} if passwords match, {@code False} otherwise
     */
    private boolean checkPassword(String password) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT password from User_ WHERE username = ? and password = ?";
        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setString(1, this.user);
        statement.setString(2, Utils.generateStorngPasswordHash(password));
        ResultSet result = statement.executeQuery();
        String passwordDDBB = null;
        if (result.next()) {
            passwordDDBB = result.getString("password");
        }
        statement.close();
        return passwordDDBB != null && passwordDDBB.equals(Utils.generateStorngPasswordHash(password));
    }

    public static User getUserbyNickName(String user) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from User_ WHERE username = ?";
        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setString(1, user);
        ResultSet result = statement.executeQuery();
        User userObject = null;
        if (result.next()) {
            userObject = new User(result.getLong("id"));
        }
        statement.close();
        return userObject;
    }

    /**
     * Sets this user connection to persistency.
     *
     * @param connection The connection to persistency
     */
    public static void setConnection(Connection connection) {
        User.connection = connection;
    }

    /**
     * Get the User connection to persistency
     *
     * @return The User connection to persistency
     */
    public static Connection getConnection() {
        return User.connection;
    }

    /**
     * Closes this User connection to persistency
     *
     * @throws SQLException
     */
    public static void closeConnection() throws SQLException {
        if (!User.connection.isClosed()) {
            User.connection.close();
            System.err.println("Connection closed");
        }

    }

    /**
     * Gets a lisf of Users part of a Group given the Group ID.
     *
     * @param group The Group retrieve the Users from
     *
     * @return A list of Users that are part of the group
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<User> getUsersByGroup(long group) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT * From User_Group WHERE id_group = ?";

        List<User> list = new ArrayList<>();

        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setLong(1, group);
        ResultSet result = statement.executeQuery();

        while (result.next()) {
            list.add(new User(result.getLong("id_user")));
        }
        statement.close();
        return list;
    }

    public static List<User> getUsersNotInGroup(long idGroup) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id From User_  where id not in (SELECT id_user From User_Group WHERE id_group = ?)";

        List<User> list = new ArrayList<>();

        PreparedStatement statement = User.getConnection().prepareStatement(query);
        statement.setLong(1, idGroup);
        ResultSet result = statement.executeQuery();

        while (result.next()) {
            list.add(new User(result.getLong("id")));
        }
        statement.close();
        return list;
    }

}
