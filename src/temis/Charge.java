package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Represents a charge that should be paid
 */
public class Charge extends Item {

    private boolean paid;   // A flag indicating if the charge has elready been paid
    private double amount;  // The amount(monetary costs) of the charge
    private User user;      // The user who paid the charge

    /**
     * Creates a new Charge.
     *
     * <p>
     * The Charge will be retrieved from persistency if it already exists, if
     * not, it will be created and persisted.
     *
     * @param paid The flag indicating if the charge has been paid
     * @param amount The amount paid
     * @param user The user who paid the charge
     * @param name The name of the Charge Item
     * @param activity The activity this Charge belongs to
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Charge(boolean paid, double amount, User user, String name, Activity activity) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(name, activity);

        long id = super.getId();

        String query = "INSERT INTO Charge (paid, amount, id_item, id_user) values (?, ?, ?, ?)";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);

        statement.setString(1, (paid ? "1" : "0"));
        statement.setDouble(2, amount);
        statement.setLong(3, id);
        statement.setLong(4, user.getId());
        statement.execute();

        this.paid = paid;
        this.amount = amount;
        this.user = user;

        statement.close();
    }

    /**
     * Creates a new Charge. The Charge will be retrieved from persistency.
     *
     * @param id The ID of the Charge
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Charge(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);

        String query = "SELECT * from Charge d WHERE d.id_item = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.amount = result.getDouble("amount");
            this.paid = result.getString("paid").equals("1");
            this.user = new User(result.getLong("id_user"));
        }
        statement.close();
    }

    /**
     * Checks if the charge has been paid.
     *
     * @return {@code True} if the charge has been already paid, {@code False}
     * otherwise
     */
    public boolean isPaid() {
        return paid;
    }

    /**
     * Set the if the charge has been paid.
     *
     * @param paid {@code True} if paid, {@code False} otherwise
     *
     * @throws SQLException
     */
    public void setPaid(boolean paid) throws SQLException {
        String query = "UPDATE Charge SET paid = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, (paid ? "1" : "0"));
        statement.setLong(2, this.getId());
        statement.execute();
        this.paid = paid;
        statement.close();
    }

    /**
     * Get the amount (costs) of the charge.
     *
     * @return The costs of the charge
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Set the amount (costs) of the charge.
     *
     * @param amount The costs of the charge
     *
     * @throws SQLException
     */
    public void setAmount(double amount) throws SQLException {
        String query = "UPDATE Charge SET amount = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setDouble(1, amount);
        statement.setLong(2, this.getId());
        statement.execute();
        this.amount = amount;
        statement.close();
    }

    /**
     * Get the User who paid the charge.
     *
     * @return The User who paid the charge
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the User who paid the charge.
     *
     * @param user The User who paid the charge
     *
     * @throws SQLException
     */
    public void setUser(User user) throws SQLException {
        String query = "UPDATE Charge SET id_user = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, user.getId());
        statement.setLong(2, this.getId());
        statement.execute();
        this.user = user;
        statement.close();
    }

    /**
     * Gets a lisf of Charges given an Activity ID.
     *
     * @param activity The Activity ID to retrieve the Charges
     *
     * @return A list of Charges from the given Activity
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Charge> getChargesByActivity(long activity) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from item WHERE id_activity = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, activity);
        ResultSet result = statement.executeQuery();

        List<Charge> list = new ArrayList<>();
        while (result.next()) {
            list.add(new Charge(result.getLong("id")));
        }
        statement.close();
        return list;
    }

    public void delete() throws SQLException {
        String query = "Delete from Charge where id_item = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, this.getId());
        statement.execute();
        super.delete();
        statement.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.paid ? 1 : 0);
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.amount) ^ (Double.doubleToLongBits(this.amount) >>> 32));
        hash = 43 * hash + Objects.hashCode(this.user);
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Charge other = (Charge) obj;
        if (this.paid != other.paid) {
            return false;
        }
        if (Double.doubleToLongBits(this.amount) != Double.doubleToLongBits(other.amount)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        return true;
    }

}
