package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Represents an Item from an Activity
 */
public class Item {

    private long id;                       // The ID of this Activity
    private String name;                   // The name of this Activity
    private static Connection connection;  // The database connection used to persist this Class
    private Activity activity;             // The Activity this Item belongs to 

    /**
     * Creates a new Activity.
     *
     * <p>
     * The Activity will be retrieved from persistency if it already exists, if
     * not, it will be created and persisted.
     *
     * @param name The name of the Item
     * @param activity The activity this Item belongs to
     *
     * @throws SQLException
     */
    public Item(String name, Activity activity) throws SQLException {
        String query = "select ITEMSECUENCE.NEXTVAL from dual";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        ResultSet result = statement.executeQuery();

        result = statement.executeQuery();
        if (result.next()) {
            this.id = result.getLong(1);
        }

        query = "INSERT INTO Item (name, id, id_activity) values (?,?,?)";

        statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setLong(2, this.id);
        statement.setLong(3, activity.getId());
        statement.execute();

        this.name = name;
        this.activity = activity;

        statement.close();
    }

    /**
     * Creates a new Item. The Item will be retrieved from persistency.
     *
     * @param id The ID of the Item
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Item(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT * from item WHERE id = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.id = result.getLong("id");
            this.name = result.getString("name");
            this.activity = new Activity(result.getLong("id_activity"));
        }
        statement.close();
    }

    /**
     * Get the name of this Item.
     *
     * @return The name of this Item
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this Item. The name is persisted when set.
     *
     * @param name The name of this Item
     *
     * @throws SQLException
     */
    public void setName(String name) throws SQLException {
        String query = "UPDATE Item SET name = ? WHERE id = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setLong(2, this.getId());
        statement.execute();

        this.name = name;
        statement.close();
    }

    /**
     * Get the ID of this Item.
     *
     * @return The ID of this Item
     */
    public long getId() {
        return this.id;
    }

    /**
     * Get the Activity this Item belongs to.
     *
     * @return The Activity this Item belongs to
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     * Set the Activity this Item belongs to. The Activity is persisted when
     * set.
     *
     * @param activity The Actibity
     *
     * @throws SQLException
     */
    public void setActivity(Activity activity) throws SQLException {
        String query = "UPDATE Item SET id_activity = ? WHERE id = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, activity.getId());
        statement.setLong(2, this.getId());
        statement.execute();

        this.activity = activity;
        statement.close();
    }

    /**
     * Deletes this Item
     */
    public void delete() throws SQLException {
        String query = "Delete from Item where id = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, this.getId());
        statement.execute();
        statement.close();
    }

    /**
     * Sets the connection to the persistency
     *
     * @param connection The connection to the persistency
     */
    public static void setConnection(Connection connection) {
        Item.connection = connection;
    }

    /**
     * Get the connection to the persistency.
     *
     * @return The connection to the persistency
     */
    public static Connection getConnection() {
        return Item.connection;
    }

    /**
     * Closes connection to the persistency.
     *
     * @throws SQLException
     */
    public static void closeConnection() throws SQLException {
        if (!Item.connection.isClosed()) {
            Item.connection.close();
            System.err.println("Connection closed");
        }
    }

}
