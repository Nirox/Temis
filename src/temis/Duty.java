package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

/**
 * Represents a duty that should be performed by an user
 */
public class Duty extends Item {

    private String description;  // The duty description
    private User user;           // The user responsible by the duty
    private boolean finished;    // A flag indicating if the duty has been finished
    private Calendar endDate;    // The date and time the duty has finished

    /**
     * Creates a new Duty.
     *
     * <p>
     * The duty will be retrieved from persistency if it already exists, if not,
     * it will be created and persisted.
     *
     * @param description The description of the duty
     * @param user The user responsible of the duty
     * @param finished The flag indicating if the duty has been finished
     * @param name The name of the duty
     * @param activity The activity this Article belongs to
     * @param endDate the date of the duty
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Duty(String description, User user, boolean finished, String name, Activity activity, Calendar endDate) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(name, activity);

        long id = super.getId();

        String query = "INSERT INTO Duty (description, finished, endDate, id_item, id_user) values (?, ?, ?, ?, ?)";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);

        statement.setString(1, description);
        statement.setString(2, (finished ? "1" : "0"));
        statement.setDate(3, new Date(endDate.getTime().getTime()));
        statement.setLong(4, id);
        statement.setLong(5, user.getId());
        statement.execute();

        this.description = description;
        this.endDate = endDate;
        this.finished = finished;
        this.user = user;

        statement.close();
    }

    /**
     * Creates a new duty. The duty will be retrieved from persistency.
     *
     * @param id The ID of the duty
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Duty(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);

        String query = "SELECT * from Duty d WHERE d.id_item = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, id);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.endDate = Calendar.getInstance();

            this.description = result.getString("description");
            this.endDate.setTime(result.getDate("endDate"));
            this.finished = result.getString("finished").equals("1");
            this.user = new User(result.getLong("id_user"));
        }
        statement.close();
    }

    /**
     * Get the description of the duty.
     *
     * @return The descrition of the duty
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of the duty. The description is persisted when set.
     *
     * @param description The description of the duty
     *
     * @throws SQLException
     */
    public void setDescription(String description) throws SQLException {
        String query = "UPDATE Duty SET description = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, description);
        statement.setLong(2, this.getId());
        statement.execute();

        this.description = description;
        statement.close();
    }

    /**
     * Get the User who is responsible for the duty
     *
     * @return The User who is responsible for the duty
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the User who is responsible for the duty.
     *
     * @param user The User who is responsible for the duty
     *
     * @throws SQLException
     */
    public void setUser(User user) throws SQLException {
        String query = "UPDATE Duty SET id_user = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, user.getId());
        statement.setLong(2, this.getId());
        statement.execute();
        this.user = user;
        statement.close();
    }

    /**
     * Checks whether the duty has been finished.
     *
     * @return {@code True} if the duty has been finished, False otherwise
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * Set if the duty has been finished. This flag is persisted when set.
     *
     * @param finished {@code True} if the duty has been finished, {@code False}
     * otherwise
     *
     * @throws SQLException
     */
    public void setFinished(boolean finished) throws SQLException {
        String query = "UPDATE Duty SET finished = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setString(1, (finished ? "1" : "0"));
        statement.setLong(2, this.getId());
        statement.execute();
        this.finished = finished;
        statement.close();
    }

    /**
     * Get the date the duty has been finished
     *
     * @return The date the duty has been finished
     */
    public Calendar getEndDate() {
        return endDate;
    }

    /**
     * Set the date the duty has been finished. This date is persisted when set.
     *
     * @param endDate The date the duty has been finishe
     *
     * @throws SQLException
     */
    public void setEndDate(Calendar endDate) throws SQLException {
        String query = "UPDATE Duty SET endDate = ? WHERE id_item = ?";

        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setDate(1, (Date) endDate.getTime());
        statement.setLong(2, this.getId());
        statement.execute();

        this.endDate = endDate;
        statement.close();
    }

    /**
     * Gets a lisf of Duties given an Activity ID.
     *
     * @param activity The Activity ID to retrieve the Duties
     *
     * @return A list of Duties from the given Activity ID
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Duty> getDutiesByActivity(long activity) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT id from item WHERE id_activity = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, activity);
        ResultSet result = statement.executeQuery();

        List<Duty> list = new ArrayList<>();
        while (result.next()) {
            list.add(new Duty(result.getLong("id")));
        }
        statement.close();
        return list;
    }

    public void delete() throws SQLException {
        String query = "Delete from Duty where id_item = ?";
        PreparedStatement statement = Item.getConnection().prepareStatement(query);
        statement.setLong(1, this.getId());
        statement.execute();
        super.delete();
        statement.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.description);
        hash = 43 * hash + Objects.hashCode(this.user);
        hash = 43 * hash + (this.finished ? 1 : 0);
        hash = 43 * hash + Objects.hashCode(this.endDate);
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Duty other = (Duty) obj;
        if (this.finished != other.finished) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Duty{" + "name=" + this.getName() + "description=" + description + ", user=" + user + ", finished=" + finished + ", endDate=" + endDate + '}';
    }

}
