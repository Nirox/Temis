package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents an Appartment shared by a group of Users
 */
public class Apartment extends Group {

    private String address;  // The address of the apartment

    /**
     * Creates an Apartment.
     *
     * <p>
     * The Apartment will be retrieved from persistency if it already exists, if
     * not, it will be created and persisted.
     *
     * @param name The Apartment name
     * @param description The Appartment description
     * @param address The Appartment address
     * @param pot The Appartment pot
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Apartment(String name, String description, String address, double pot) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(name, pot, description);

        Long idGroup = super.getId();
        String query = "INSERT INTO Apartment (address, id_group) values (?,?)";
        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, address);
        statement.setLong(2, idGroup);
        statement.execute();

        this.address = address;

        statement.close();
    }

    /**
     * Creates an Apartment. The activity will be retrieved from persistency.
     *
     * @param id The ID of the activity in the persistency
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException{
     */
    public Apartment(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);
        Long idGroup = super.getId();
        String query = "SELECT * from Apartment ap where ap.id_group = ?";
        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, idGroup);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.address = result.getString("address");
        }
        statement.close();

    }

    /**
     * Returns a string representation of the Apartment.
     *
     * @return A string representation of the Apartment
     */
    @Override
    public String toString() {
        return "Group{" + "name=" + this.getName() + ", pot=" + this.getPot() + ", description=" + this.getDescription() + ", users=" + this.getUsers() + ", activities=" + this.getActivities() + ", address=" + this.address + ", id=" + this.getId() + '}';
    }

    /**
     * Returns a hash identifier of the Apartment.
     *
     * @return A hash identifier of the Apartment
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.address);
        return hash;
    }

    /**
     * Get the Apartment address.
     *
     * @return The Apartment address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Set the Apartment address.
     *
     * @param address The Apartment Address
     *
     * @throws SQLException
     */
    public void setAddress(String address) throws SQLException {
        String query = "UPDATE Apartment SET address = ? WHERE id_group = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, address);
        statement.setLong(2, this.getId());
        statement.execute();

        this.address = address;
        statement.close();
    }

    /**
     * Checks if an object is equal to this.
     *
     * @param obj The object we want to compare to
     *
     * @return {@code True} if both objects are equal, {@code False} otherwise
     */
    @Override
    public boolean equals(Object obj) {
        boolean res = obj instanceof Apartment;
        Apartment apartment = res ? (Apartment) obj : null;
        return res && this.getName().equals(apartment.getName()) && this.getPot() == apartment.getPot() && this.getDescription().equals(apartment.getDescription()) && this.getUsers().containsAll(apartment.getUsers()) && this.getActivities().containsAll(apartment.getActivities()) && this.getId() == apartment.getId() && address.equals(apartment.address);
    }

    /**
     * Gets a list of Apartments a user is part of.
     *
     * @param user The User object we want to use to search for Apartments
     *
     * @return A list of Apartments the user is part of
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Apartment> getApartmentsByUser(User user) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT * from User_Group ug, Apartment a WHERE id_user = ? and a.id_group = ug.id_group";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, user.getId());
        ResultSet result = statement.executeQuery();

        List<Apartment> list = new ArrayList<>();

        while (result.next()) {
            list.add(new Apartment(result.getLong("id_group")));
        }
        statement.close();
        return list;
    }

    /**
     * Deletes this Group from persistency.
     */
    public void delete() {
        try {
            List<Purchase> activityListPurchase = Purchase.getPurchasesByGroup(this.getId());

            for (Purchase activity : activityListPurchase) {
                activity.delete();
            }

            List<Debt> activityListDebt = Debt.getDebtsByGroup(this.getId());

            for (Debt activity : activityListDebt) {
                activity.delete();
            }

            List<Task> activityListTask = Task.getTasksByGroup(this.getId());

            for (Task activity : activityListTask) {
                activity.delete();
            }

            List<JournalPlace> activityListJournalPlace = JournalPlace.getJournalPlacesByGroup(this.getId());

            for (JournalPlace activity : activityListJournalPlace) {

                activity.delete();

            }

            String query = "Delete from Apartment where id_group = ?";
            PreparedStatement statement = Item.getConnection().prepareStatement(query);
            statement.setLong(1, this.getId());
            statement.execute();
            super.delete();
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(Apartment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Apartment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Apartment.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
