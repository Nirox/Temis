package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a Trip that a Group would like to make.
 */
public class Trip extends Group {

    private String place;      // The place the group would like to travel to
    private Calendar date;     // The date and time of the trip
    private String lodging;    // The place to stay during the trip
    private String transport;  // The means of transportation used on the trip

    /**
     * Creates a new Trip.
     *
     * <p>
     * The Trip will be retrieved from persistency if it already exists, if not,
     * it will be created and persisted.
     *
     * @param place The place the group would like to travel to
     * @param lodging The place to stay during the trip
     * @param transport The means of transportation used on the trip
     * @param name The name of the Trip
     * @param date The date and time of the trip
     * @param pot The amount of money the Group share for the Trip
     * @param description The description of the Trip
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Trip(String place, String lodging, String transport, String name, Calendar date, double pot, String description) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(name, pot, description);

        Long idGroup = super.getId();
        this.date = Calendar.getInstance();

        String query = "INSERT INTO Trip(place, date_, lodging, transport, id_group) values (?, ?, ?, ?, ?)";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);

        statement.setString(1, place);
        statement.setDate(2, new Date(date.getTime().getTime()));
        statement.setString(3, lodging);
        statement.setString(4, transport);
        statement.setLong(5, idGroup);
        statement.execute();

        this.place = place;
        this.date = date;
        this.lodging = lodging;
        this.transport = transport;

        statement.close();
    }

    /**
     * Creates a new Trip. The Trip will be retrieved from persistency.
     *
     * @param id The ID of the Trip in the persistency
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Trip(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);

        Long idGroup = super.getId();
        this.date = Calendar.getInstance();

        String query = "SELECT * from Trip where id_group = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, idGroup);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.place = result.getString("place");
            this.date.setTime(result.getDate("date_"));
            this.lodging = result.getString("lodging");
            this.transport = result.getString("transport");
        }
        statement.close();
    }

    /**
     * Get the place the Group would like to visit.
     *
     * @return The place the Group would like to visit
     */
    public String getPlace() {
        return place;
    }

    /**
     * Set the place the Group would like to visit. The place is persisted when
     * set.
     *
     * @param place The place the Group would like to visit
     *
     * @throws SQLException
     */
    public void setPlace(String place) throws SQLException {
        String query = "UPDATE Trip SET place = ? WHERE id_group = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, place);
        statement.setLong(2, this.getId());
        statement.execute();

        this.place = place;
        statement.close();
    }

    /**
     * Get the lodging place to stay during the Trip.
     *
     * @return The lodging place to stay during the Trip
     */
    public String getLodging() {
        return lodging;
    }

    /**
     * Set the lodging place to stay during the Trip. The place is persisted
     * when set.
     *
     * @param lodging The lodging place to stay during the Trip
     *
     * @throws SQLException
     */
    public void setLodging(String lodging) throws SQLException {
        String query = "UPDATE trip SET lodging = ? WHERE id_group = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, lodging);
        statement.setLong(2, this.getId());
        statement.execute();

        this.lodging = lodging;
        statement.close();
    }

    /**
     * Get the means of transportation of the Trip.
     *
     * @return The means of transportation of the Trip
     */
    public String getTransport() {
        return transport;
    }

    /**
     * Set the means of transportation of the Trip. The means of transportation
     * is persisted when set.
     *
     * @param transport The means of transportation of the Trip
     *
     * @throws SQLException
     */
    public void setTransport(String transport) throws SQLException {
        String query = "UPDATE Trip SET transport = ? WHERE id_group = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, transport);
        statement.setLong(2, this.getId());
        statement.execute();

        this.transport = transport;
        statement.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.place);
        hash = 97 * hash + Objects.hashCode(this.date);
        hash = 97 * hash + Objects.hashCode(this.lodging);
        hash = 97 * hash + Objects.hashCode(this.transport);
        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Trip other = (Trip) obj;
        if (!Objects.equals(this.place, other.place)) {
            return false;
        }
        if (!Objects.equals(this.lodging, other.lodging)) {
            return false;
        }
        if (!Objects.equals(this.transport, other.transport)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    /**
     * Gets a lisf of Trips given a User.
     *
     * @param user The User to get his/her Trips
     *
     * @return A list of Trips made by the User
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Trip> getTripsByUser(User user) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeySpecException {
        String query = "SELECT * from User_Group ug, Trip t WHERE ug.id_user = ? and t.id_group = ug.id_group";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, user.getId());
        ResultSet result = statement.executeQuery();
        List<Trip> list = new ArrayList<>();

        while (result.next()) {
            list.add(new Trip(result.getLong("id_group")));
        }
        return list;
    }

    /**
     * Deletes the Group
     */
    public void delete() {
        try {
            List<Purchase> activityListPurchase = Purchase.getPurchasesByGroup(this.getId());

            for (Purchase activity : activityListPurchase) {
                activity.delete();
            }

            List<Debt> activityListDebt = Debt.getDebtsByGroup(this.getId());

            for (Debt activity : activityListDebt) {
                activity.delete();
            }

            List<Task> activityListTask = Task.getTasksByGroup(this.getId());

            for (Task activity : activityListTask) {
                activity.delete();
            }

            List<JournalPlace> activityListJournalPlace = JournalPlace.getJournalPlacesByGroup(this.getId());

            for (JournalPlace activity : activityListJournalPlace) {

                activity.delete();

            }

            String query = "Delete from Trip where id_group = ?";
            PreparedStatement statement = Item.getConnection().prepareStatement(query);
            statement.setLong(1, this.getId());
            statement.execute();
            super.delete();
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(Apartment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Apartment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Apartment.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
