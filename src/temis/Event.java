package temis;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents an event where a group of users will participate.
 */
public class Event extends Group {

    private String place;   // The place the event takes place
    private Calendar date;  // The date  the event takes place

    /**
     * Creates a new Event.
     *
     * <p>
     * The Event will be retrieved from persistency if it already exists, if
     * not, it will be created and persisted.
     *
     * @param name The name of the Event
     * @param place The place of the Event
     * @param date The date of the Event
     * @param pot The amount of money for the Event
     * @param description The description of the Event
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Event(String name, String place, Calendar date, double pot, String description) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(name, pot, description);

        long idGroup = super.getId();
        date = Calendar.getInstance();

        String query = "INSERT INTO Event(place, date_, id_group) values (?, ?, ?)";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);

        statement.setString(1, place);
        statement.setDate(2, new Date(date.getTime().getTime()));
        statement.setLong(3, idGroup);
        statement.execute();

        this.place = place;
        this.date = date;

        statement.close();
    }

    /**
     * Creates a new Event. The Event will be retrieved from persistency.
     *
     * @param id The ID of the Event
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public Event(long id) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        super(id);

        long idGroup = super.getId();
        this.date = Calendar.getInstance();

        String query = "SELECT * from Event where id_group = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, idGroup);
        ResultSet result = statement.executeQuery();

        if (result.next()) {
            this.place = result.getString("place");
            this.date.setTime(result.getDate("date_"));
        }
        statement.close();
    }

    /**
     * Get the place of the Event.
     *
     * @return The place of the Event
     */
    public String getPlace() {
        return place;
    }

    /**
     * Set the place of the Event. The place is persisted when set.
     *
     * @param place The place of the Event
     *
     * @throws SQLException
     */
    public void setPlace(String place) throws SQLException {
        String query = "UPDATE Event SET place = ? WHERE id_group = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setString(1, place);
        statement.setLong(2, this.getId());
        statement.execute();

        this.place = place;
        statement.close();
    }

    /**
     * Get the date of the Event.
     *
     * @return The date of the Event
     */
    public Calendar getDate() {
        return date;
    }

    /**
     * Set the date of the Event. The date is persisted when set.
     *
     * @param date The date of the Event
     *
     * @throws SQLException
     */
    public void setDate(Calendar date) throws SQLException {
        String query = "UPDATE Event SET date_ = ? WHERE id_group = ?";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setDate(1, new Date(date.getTime().getTime()));
        statement.setLong(2, this.getId());
        statement.execute();
        this.date = date;
        statement.close();
    }

    /**
     * Returns a hash identifier of the Article.
     *
     * @return A hash identifier of the Article
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.place);
        hash = 67 * hash + Objects.hashCode(this.date);
        return hash;
    }

    /**
     * Checks if an object is equal to this.
     *
     * @param obj The object we want to compare to
     *
     * @return {@code True} if both objects are equal, {@code False} otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Event other = (Event) obj;
        if (!Objects.equals(this.place, other.place)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the Article.
     *
     * @return A string representation of the Article
     */
    @Override
    public String toString() {
        return "Event{" + "name=" + this.getName() + ", place=" + place + ", date=" + date + '}';
    }

    /**
     * Gets a lisf of all Events a User is part of.
     *
     * @param user The User to use as search
     *
     * @return A list of all Events the User is part of
     *
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static List<Event> getEventsByUser(User user) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
        String query = "SELECT * from User_Group ug, Event e WHERE id_user = ? and e.id_group = ug.id_group";

        PreparedStatement statement = Group.getConnection().prepareStatement(query);
        statement.setLong(1, user.getId());
        ResultSet result = statement.executeQuery();
        List<Event> list = new ArrayList<>();

        while (result.next()) {
            list.add(new Event(result.getLong("id_group")));
        }
        statement.close();
        return list;
    }

    /**
     * Deletes a group
     */
    public void delete() {
        try {
            List<Purchase> activityListPurchase = Purchase.getPurchasesByGroup(this.getId());

            for (Purchase activity : activityListPurchase) {
                activity.delete();
            }

            List<Debt> activityListDebt = Debt.getDebtsByGroup(this.getId());

            for (Debt activity : activityListDebt) {
                activity.delete();
            }

            List<Task> activityListTask = Task.getTasksByGroup(this.getId());

            for (Task activity : activityListTask) {
                activity.delete();
            }

            List<JournalPlace> activityListJournalPlace = JournalPlace.getJournalPlacesByGroup(this.getId());

            for (JournalPlace activity : activityListJournalPlace) {

                activity.delete();

            }

            String query = "Delete from Event where id_group = ?";
            PreparedStatement statement = Item.getConnection().prepareStatement(query);
            statement.setLong(1, this.getId());
            statement.execute();
            super.delete();
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(Apartment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Apartment.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Apartment.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
