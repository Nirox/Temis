package temis;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class UtilsTest {
    
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of generateStorngPasswordHash method, of class Utils.
     * @throws java.lang.Exception
     */
    @Test
    public void testGenerateStorngPasswordHash() throws Exception {
        String password = "myveryownpassword1234";
        String result = Utils.generateStorngPasswordHash(password);
        assertEquals(174, result.length());
    }

    /**
     * Test of validatePassword method, of class Utils.
     * @throws java.lang.Exception
     */
    @Test
    public void testValidatePassword() throws Exception {
        String originalPassword = "myveryownpassword1234";
        String storedPassword = "1000:262928aed13ebd6ba56fb18587c2d2157eeb7313:4a37ebe8bd2f1fe4f7c6e4fe29dc9b142a114d2dbfbaf08ecb14496b3f91701578e5dffdab9ef7817becb572906ea16395c1beb4fd055d6a98dcd06e7b66a467";
        boolean result = Utils.validatePassword(originalPassword, storedPassword);
        assertEquals(true, result);
    }

    /**
     * Test of stringToDate method, of class Utils.
     * @throws java.lang.Exception
     */
    @Test
    public void testStringToDate() throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String date = "10/12/2017";
        Date result = Utils.stringToDate(date);
        assertEquals(dateFormat.parse("10/12/2017"), result);
    }

    /**
     * Test of dateToString method, of class Utils.
     * @throws java.text.ParseException
     */
    @Test
    public void testDateToString() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = dateFormat.parse("10/12/2017");
        String result = Utils.dateToString(date);
        assertEquals("10/12/2017", result);
    }
    
}
